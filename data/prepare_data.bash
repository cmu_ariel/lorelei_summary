# 03 Nov 2018
# By: Aldrian Obaja Muis
# To copy all LoReLEI data into single folder with consistent format

script_dir=$(dirname $(readlink -f $0))
outdir="langpacks"
mkdir -p ${outdir}
outdir_abs=$(readlink -f ${outdir})
echo "script_dir: ${script_dir}"
echo "outdir: ${outdir}"
echo "outdir_abs: ${outdir_abs}"

echo "Creating directories..."
# In the following, IL{3,5,6,9,10} are used since some file names use ilN format, where other languages uses the
# 3-letter language code.
for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL" "IL3" "IL5" "IL6" "IL9" "IL10" "ENG"; do
    if [ -e "${outdir}/${lang}" ]; then
        echo "${outdir}/${lang} exists! Aborting"
        continue
    fi
    # Directory structure for each language:
    # ${lang}
    # - set0/
    #   - IL/
    #     - ltf/
    # - set[123]/
    #   - IL/
    #     - ltf/
    # - setE/
    #   - IL/
    #     - ltf/
    #   - ENG/
    #     - ltf/
    #   - speech/
    #     - ltf/
    #   - ENG_from_IL/
    #     - ltf/
    #       - <doc_id>.eng_A.ltf.xml <-- always start with _A for the first reference translation
    #       - (<doc_id>.eng_B.ltf.xml) <-- _B for the second translation, if available
    #   - sf_anno/
    #     - issues/
    #     - needs/
    #     - mentions/
    #     - annotated_filelist_SF.tab
    #   - ${lang}_edl.tab
    # - setS/
    #   - ENG/
    #     - ltf/
    # - core_data/
    # - dictionaries/
    # - morph/
    # - word_embeddings/
    # - IL_keywords/
    # - ner_output/
    # - edl_output/
    mkdir -p ${outdir}/${lang}
    mkdir -p ${outdir}/${lang}/{set{0,1,2,3,E,S},dictionaries,morph,word_embeddings,IL_keywords,ner_output,edl_output}
    mkdir -p ${outdir}/${lang}/set{0,1,2,3}/IL/{ltf,rsd}
    mkdir -p ${outdir}/${lang}/setS/ENG/{ltf,rsd}
    mkdir -p ${outdir}/${lang}/core_data/IL/{ltf,rsd,sf_anno}
    mkdir -p ${outdir}/${lang}/setE/{IL,ENG,speech,IL_from_ENG,IL_into_ENG,ENG_from_IL,ENG_into_IL}/ltf
    mkdir -p ${outdir}/${lang}/setE/{IL,ENG,speech}/sf_anno
done

ln -s ${outdir_abs}/IL3 ${outdir}/UIG
ln -s ${outdir_abs}/IL5 ${outdir}/TIR
ln -s ${outdir_abs}/IL6 ${outdir}/ORM
ln -s ${outdir_abs}/IL9 ${outdir}/KIN
ln -s ${outdir_abs}/IL10 ${outdir}/SIN
echo "Directories created."

echo "Copying documents and annotations..."
outdir=${outdir} bash copy_documents_and_annotations.bash
echo "Done copying documents and annotations."

echo "Copying dictionaries and morphology analyzers..."
outdir=${outdir} bash copy_dictionaries_and_morph.bash
echo "Done copying dictionaries and morphology analyzers."

echo "Processing SF keywords..."
outdir=${outdir} bash process_SF_keywords.bash
echo "Done processing SF keywords."

#echo "Copying NER, EDL, and MT outputs..."
#bash copy_ner_edl_mt_output.bash
#echo "Done copying NER, EDL, and MT outputs."
