# -*- coding: utf-8 -*-
"""
05 May 2018
To read SF files and convert to JSON
Usage history:
    - To convert SF annotation exercise LDC2017E07 for Efsun (5 May 2018 - Aldrian)
      python3 /usr2/home/amuis/codebase/sf/utils/sf_to_json.py \
              --ltf_dir /home/data/LDC2017E07_LORELEI_Situation_Frame_Exercise_Annotation/data/monolingual_text/ltf \
              --sf_dir /home/data/LDC2017E07_LORELEI_Situation_Frame_Exercise_Annotation/data/situation_frame \
              --json_out /usr2/home/amuis/work/internal_dry_run2018/public/files/LDC2017E07_gold_sf.json \
              --copy_segment_text
    - To convert SF IL5 and IL6 (23 Oct 2018 - Aldrian)
      python3 /usr2/home/amuis/codebase/sf/utils/sf_to_json.py \
              --ltf_dir /usr2/home/amuis/data/by_dataset/2017_tir_setE/tir/ltf/ \
              --sf_dir /usr2/home/amuis/data/by_dataset/2017_tir_setE/sf_anno/ \
              --json_out /usr2/home/amuis/data/by_dataset/2017_tir_setE/il5_gold.json \
              --copy_segment_text
      python3 /usr2/home/amuis/codebase/sf/utils/sf_to_json.py \
              --ltf_dir /usr2/home/amuis/data/by_dataset/2017_orm_setE/orm/ltf/ \
              --sf_dir /usr2/home/amuis/data/by_dataset/2017_orm_setE/sf_anno/ \
              --json_out /usr2/home/amuis/data/by_dataset/2017_orm_setE/il6_gold.json \
              --copy_segment_text
      python3 /usr2/home/amuis/codebase/sf/utils/sf_to_json.py \
              --ltf_dir /usr2/home/amuis/data/by_dataset/2017_orm_setE/orm/ltf/ \
              --sf_dir /usr2/home/amuis/data/by_dataset/2017_orm_setE/sf_anno_without_outlier/ \
              --json_out /usr2/home/amuis/data/by_dataset/2017_orm_setE/il6_gold_without_outlier.json \
              --copy_segment_text
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
from lor_utils import read_ltf_files, read_sf_annos, read_sf_mentions_dir, get_kb_from_manager, read_ner_file
import json

def main():
    parser = ArgumentParser(description='Read SF files and convert to JSON')
    parser.add_argument('--ltf_dir', help='The directory to LTF files.')
    parser.add_argument('--sf_dir', help='The directory to SF annotations.')
    parser.add_argument('--json_out', help='The path to output the JSON.')
    parser.add_argument('--copy_segment_text', action='store_true',
                        help='Whether to copy segment text into "TextSegment" field')
    parser.add_argument('--kb_manager_port', default=50000, help='Port to KB Manager to fill KB info')
    args = parser.parse_args()
    kb = get_kb_from_manager(args.kb_manager_port)
    ltf_dir = args.ltf_dir
    sf_dir = args.sf_dir
    json_out = args.json_out
    copy_segment_text = args.copy_segment_text
    docs = read_ltf_files(ltf_dir, outtype='dict')
    sfs = read_sf_annos(sf_dir)
    try:
        entities = read_sf_mentions_dir('{}/mentions'.format(sf_dir), outtype='dict')
    except:
        entities = {}
    sf_dicts = []
    count = 0
    for sf in sfs:
        doc_id = sf.doc_id
        doc = docs.get(doc_id, None)
        if not doc:
            dot_index = doc_id.find('.')
            doc_id = doc_id[:dot_index]
            doc = docs.get(doc_id, None)
        if not doc:
            count += 1
            continue
        if sf.text is not None and sf.text != 'none':
            for segment in doc.segments:
                if sf.text in segment.text:
                    sf.seg_id = segment.seg_id
                    break
        sf.place = kb.get(sf.kb_id, None)
        if sf.place_mention is not None:
            kb_names = []
            if sf.kb_id:
                for kb_id in sf.kb_id.split('|'):
                    kb_names.append(kb.get(kb_id).name)
            sf.place_mention.men_text_kb = '|'.join(kb_names)
        sf_dict = sf.to_dict(entities=entities) # Convert to dict, assign place
        if copy_segment_text and sf.seg_id is not None:
            sf_dict['TextSegment'] = doc.get_segment(sf.seg_id).text
        sf_dicts.append(sf_dict)
    print('Docs in SF annotations not found in LTF: {}'.format(count))
    with open(json_out, 'w') as outfile:
        json.dump(sf_dicts, outfile, indent=4, ensure_ascii=False, sort_keys=True)

if __name__ == '__main__':
    main()

