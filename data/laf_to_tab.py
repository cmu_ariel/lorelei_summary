# -*- coding: utf-8 -*-
"""
07 Nov 2018
Convert LAF file into TAB file
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
from bs4 import BeautifulSoup as BS
import os

def main(args=None):
    indir = ('/home/data/LDC2016E75_LORELEI_IL3_Dual_Annotation_for_Simple_Named_Entity_and_Situation_Frame_'
             'Unsequestered/data/simple_named_entity')
    parser = ArgumentParser(description='Convert LAF files into TAB file')
    parser.add_argument('--indir', default=indir,
                        help='The directory to find LAF files. All LAF files found recursively will be used.')
    parser.add_argument('--outfile', required=True,
                        help='The output file')
    args = parser.parse_args(args)
    indir = args.indir
    outfile = args.outfile
    text_to_kb_id = {}
    sys_run_dirs = set()
    entities = []
    for basedir, dirnames, filenames in os.walk(indir):
        for filename in filenames:
            if not filename.endswith('laf.xml'):
                continue
            if basedir not in sys_run_dirs:
                sys_run_dirs.add(basedir)
                sys_run_id = 'LDC-{}'.format(len(sys_run_dirs))
            with open('{}/{}'.format(basedir, filename), 'r') as infile:
                text = infile.read()
            soup = BS(text, 'lxml')
            doc = soup.find('doc')
            doc_id = doc['id']
            for ann in doc.find_all('annotation'):
                mention_id = ann['id']
                mention_text = ann.extent.get_text()
                start = int(ann.extent['start_char'])
                end = int(ann.extent['end_char'])
                entity_type = ann.tag.get_text()
                mention_type = 'NAM'
                confidence = 1.0
                if mention_text not in text_to_kb_id:
                    text_to_kb_id[mention_text] = len(text_to_kb_id)+30001
                kb_id = 'NIL{}'.format(text_to_kb_id[mention_text])
                extents = '{}:{}-{}'.format(doc_id, start, end)
                entities.append((sys_run_id, mention_id, mention_text, extents, kb_id, entity_type, mention_type, confidence))
    headers = ['system_run_id', 'mention_id', 'mention_text', 'extents', 'kb_id', 'entity_type', 'mention_type', 'confidence']

if __name__ == '__main__':
    main()

