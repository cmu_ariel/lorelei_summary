# -*- coding: utf-8 -*-
"""
07 Nov 2018
To combine IL3 dual annotations into single files
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import os

def main(args=None):
    basedir = ('/home/data/LDC2016E75_LORELEI_IL3_Dual_Annotation_for_Simple_Named_Entity_and_Situation_Frame_'
               'Unsequestered/data/situation_frame/')
    parser = ArgumentParser(description='To combine IL3 dual annotations into single files')
    parser.add_argument('--basedir', default=basedir,
                        help='The base directory, containing "version1" and "version2" directories')
    parser.add_argument('--outdir', required=True,
                        help='The output directory.')
    args = parser.parse_args(args)
    basedir = args.basedir
    outdir = args.outdir
    try:
        os.mkdir(outdir)
    except:
        pass
    for sf_cat in ['issues', 'mentions', 'needs']:
        try:
            os.mkdir('{}/{}'.format(outdir, sf_cat))
        except:
            pass
        filenames = set()
        for version in ['version1', 'version2']:
            ann_dir = '{}/{}/{}'.format(basedir, version, sf_cat)
            for filename in os.listdir(ann_dir):
                filenames.add(filename)
        for filename in filenames:
            outpath = '{}/{}/{}'.format(outdir, sf_cat, filename)
            header = None
            lines = []
            for version in ['version1', 'version2']:
                user_id = 30000 + int(version[-1])
                inpath = '{}/{}/{}/{}'.format(basedir, version, sf_cat, filename)
                if os.path.exists(inpath):
                    with open(inpath, 'r') as infile:
                        for line in infile:
                            if line.startswith('doc_id'):
                                if header is None:
                                    header = 'user_id\t{}'.format(line.rstrip('\n'))
                            else:
                                lines.append('{}\t{}'.format(user_id, line.rstrip('\n')))
            with open(outpath, 'w') as outfile:
                outfile.write('{}\n'.format(header))
                for line in lines:
                    outfile.write('{}\n'.format(line))

if __name__ == '__main__':
    main()

