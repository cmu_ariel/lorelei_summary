# 10 Apr 2019
# By: Aldrian Obaja Muis
# Copy dictionaries and morphology analyzer (if available)

script_dir=$(dirname $(readlink -f $0))
if [ -z "${outdir+x}" ]; then
    outdir="langpacks"
fi
mkdir -p ${outdir}
outdir_abs=$(readlink -f ${outdir})
echo "script_dir: ${script_dir}"
echo "outdir: ${outdir}"
echo "outdir_abs: ${outdir_abs}"
uw_path="/home/data/misc_resources/UW_Lexicons-020317"
ldc_path="/home/data/ldc_packs/"
echo "Using dictionaries from ${uw_path} and ${ldc_path}"

dict_paths=()
dict_paths+=("AKA:/home/data/ldc_packs/aka/annotations/data/lexicon/aka_lexicon.llf.xml")
dict_paths+=("AMH:/home/data/ldc_packs/amh/annotations/data/lexicon/amh_lexicon.v2.0.llf.xml")
dict_paths+=("ARA:/home/data/ldc_packs/ara/annotations/data/lexicon/ara_lexicon.v1.0.llf.xml")
dict_paths+=("BEN:/home/data/ldc_packs/ben/annotations/data/lexicon/ben_lexicon.v1.0.llf.xml")
dict_paths+=("CMN:/home/data/ldc_packs/cmn/annotations/data/lexicon/cmn_lexicon.v1.0.llf.xml")
dict_paths+=("FAS:/home/data/ldc_packs/fas/annotations/data/lexicon/fas_lexicon.v2.0.llf.xml")
dict_paths+=("HAU:/home/data/ldc_packs/hau/data/lexicon/hau_lexicon.llf.v1_2.xml")
dict_paths+=("HIN:/home/data/ldc_packs/hin/annotations/data/lexicon/hin_lexicon.v1.0.llf.xml")
dict_paths+=("HUN:/home/data/ldc_packs/hun/annotations/data/lexicon/hun_lexicon.v2.0.llf.xml")
dict_paths+=("IND:/home/data/ldc_packs/ind/annotations/data/lexicon/ind_lexicon.v1.0.llf.xml")
dict_paths+=("RUS:/home/data/ldc_packs/rus/annotations/data/lexicon/rus_lexicon.v2.0.llf.xml")
dict_paths+=("SOM:/home/data/ldc_packs/som/annotations/data/lexicon/som_lexicon.v2.0.llf.xml")
dict_paths+=("SPA:/home/data/ldc_packs/spa/annotations/data/lexicon/spa_lexicon.v2.0.llf.xml")
dict_paths+=("SWA:/home/data/ldc_packs/swa/annotations/data/lexicon/swa_lexicon.v1.0.llf.xml")
dict_paths+=("TAM:/home/data/ldc_packs/tam/annotations/data/lexicon/tam_lexicon.v1.0.llf.xml")
dict_paths+=("TGL:/home/data/ldc_packs/tgl/annotations/data/lexicon/tgl_lexicon.v1.0.llf.xml")
dict_paths+=("THA:/home/data/ldc_packs/tha/annotations/data/lexicon/tha_lexicon.v1.0.llf.xml")
dict_paths+=("TUR:/home/data/ldc_packs/tur/data/lexicon/lexicon.llf.xml")
dict_paths+=("UKR:/home/data/ldc_packs/ukr/set0/docs/categoryI_dictionary/eng_il4_dictionary.txt")
dict_paths+=("URD:/home/data/ldc_packs/urd/data/lexicon/Urdu_v0.6.llf.xml")
dict_paths+=("UZB:/home/data/ldc_packs/uzb/data/lexicon/lexicon.v6.llf.xml")
dict_paths+=("VIE:/home/data/ldc_packs/vie/annotations/data/lexicon/vie_lexicon.v1.0.llf.xml")
dict_paths+=("WOL:/home/data/ldc_packs/wol/annotations/data/lexicon/wol_lexicon.llf.xml")
dict_paths+=("YOR:/home/data/ldc_packs/yor/annotations/data/lexicon/yor_lexicon.v2.0.llf.xml")
dict_paths+=("ZUL:/home/data/ldc_packs/zul/annotations/data/lexicon/zul_lexicon.v1.0.llf.xml")

echo "Copying UW dictionaries..."
# In the following, IL{3,5,6,9,10} are used since some file names use ilN format, where other languages uses the
# 3-letter language code.
for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL" "UIG" "TIR" "ORM" "KIN" "SIN"; do
    lang_code=$(echo ${lang} | tr [A-Z] [a-z]);
    if [ "${lang}" == "FAS" ]; then
        lang_code="pes"
    fi
    dict_path="${uw_path}/${lang_code}-eng.masterlex.txt"
    if [ "${lang}" == "ARA" ]; then
        outpath="${outdir}/${lang}/dictionaries/lexicon_uw_2017_withdiacritics.tab"
    else
        outpath="${outdir}/${lang}/dictionaries/lexicon_uw_2017.tab"
    fi
    python3 masterlex_to_tab.py \
        --inpath ${dict_path} \
        --outpath ${outpath} \
        --outformat "en_il" \
        --lang ${lang}

done
echo "Copying LDC dictionaries..."
for lang_path in ${dict_paths[@]}; do
    lang=${lang_path//:*}
    path=${lang_path##*:}
    if [ "${lang}" == "ARA" ]; then
        outpath="${outdir}/${lang}/dictionaries/lexicon_ldc_withdiacritics.tab"
    else
        outpath="${outdir}/${lang}/dictionaries/lexicon_ldc.tab"
    fi
    if [ "${path}" ~= "*.xml" ]; then
        python3 llf_to_tab.py \
            --inpaths ${path} \
            --outpath ${outpath} \
            --outformat "en_il" \
            --lang ${lang}
    else
        cp ${path} ${outpath}
    fi
done
echo "Cleaning LDC dictionaries..."
for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL"; do
    if [ "${lang}" == "ARA" ]; then
        path="${outdir}/${lang}/dictionaries/lexicon_ldc_withdiacritics.tab"
        outpath="${outdir}/${lang}/dictionaries/lexicon_ldc_clean_withdiacritics.tab"
    else
        path="${outdir}/${lang}/dictionaries/lexicon_ldc.tab"
        outpath="${outdir}/${lang}/dictionaries/lexicon_ldc_clean.tab"
    fi
    python3 clean_lexicon.py \
        --inpath ${path} \
        --outpath ${outpath} \
        --lang ${lang}
done
echo "Done copying dictionaries"

echo "Combining LDC (clean) lexicon with UW (2017) lexicon..."
for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL"; do
    if [ "${lang}" == "ARA" ]; then
        lexicon_ldc_path="${outdir}/${lang}/dictionaries/lexicon_ldc_clean_withdiacritics.tab";
        lexicon_uw_path="${outdir}/${lang}/dictionaries/lexicon_uw_2017_withdiacritics.tab";
        out_path="${outdir}/${lang}/dictionaries/lexicon_ldc_clean+uw_2017_withdiacritics.tab";
    else
        lexicon_ldc_path="${outdir}/${lang}/dictionaries/lexicon_ldc_clean.tab";
        lexicon_uw_path="${outdir}/${lang}/dictionaries/lexicon_uw_2017.tab";
        out_path="${outdir}/${lang}/dictionaries/lexicon_ldc_clean+uw_2017.tab";
    fi
    if [ -f ${lexicon_ldc_path} ]; then
        echo "${lexicon_ldc_path}"
        cat ${lexicon_ldc_path} ${lexicon_uw_path} > ${out_path}
    fi
done
echo "Done combining lexicons"

outdir="langpacks"
echo "Removing diacritics from Arabic dictionaries..."
for dict_name in "lexicon_ldc" "lexicon_ldc_clean" "lexicon_uw_2017" "lexicon_ldc_clean+uw_2017"; do
    dict_inpath="${outdir}/ARA/dictionaries/${dict_name}_withdiacritics.tab"
    dict_outpath="${outdir}/ARA/dictionaries/${dict_name}.tab"
    python3 remove_diacritics.py \
        --inpath "${dict_inpath}" \
        --outpath "${dict_outpath}" \
        --remove_char_class "combining"
done
echo "Done removing diacritics from Arabic dictionaries"
