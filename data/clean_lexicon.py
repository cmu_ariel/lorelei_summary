# -*- coding: utf-8 -*-
"""
10 Apr 2019
Clean LDC lexicon by splitting and removing multi-definitions
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import pandas as pd
import re

def clean_gloss(en_word, lang):
    en_word = re.sub(r'(\([^)]*\)|\[[^\]]+\]|{[^}]+})', '', en_word)
    en_word = re.sub('\t', ' ', en_word)
    en_word = re.sub(' +', ' ', en_word)
    if lang.lower() in ['ara', 'wol', 'zul']:
        en_word = en_word.strip(' ,;/')
        en_words = re.split('[,;/]', en_word)
    else:
        en_word = en_word.strip(' ,;')
        en_words = re.split('[,;]', en_word)
    en_words = [en_word.strip() for en_word in en_words]
    en_words = [en_word for en_word in en_words if en_word]
    return en_words

def main(args=None):
    parser = ArgumentParser(description='Clean a lexicon in en_il TAB format')
    parser.add_argument('--inpath', required=True,
                        help='The file path to be cleaned')
    parser.add_argument('--outpath', default='en_il_lexicon.tab',
                        help='The out path of the dictionary')
    parser.add_argument('--lang', default='',
                        help='The language name of this dictionary, for printing')
    args = parser.parse_args(args)
    lexicon = pd.read_csv(args.inpath, sep='\t', header=None, dtype='object')
    lexicon.columns = ['en_word', 'il_word']
    entries = list(zip(*(lexicon['en_word'], lexicon['il_word'], lexicon.index)))
    entries = [(str(a), str(b), c) for a,b,c in entries]
    clean_entries = []
    removed = 0
    for en_word, il_word, index in entries:
        if en_word.startswith('NO_GLOSS'):
            removed += 1
            continue
        for clean_en_word in clean_gloss(en_word, args.lang):
            clean_entries.append((clean_en_word, il_word, index))
    print('{}: old lexicon size: {}, cleaned size: {}, removed: {}'.format(args.lang,
                                                                           len(entries),
                                                                           len(clean_entries),
                                                                           removed
                                                                           ))
    with open(args.outpath, 'w') as outfile:
        for entry in clean_entries:
            outfile.write('{}\t{}\n'.format(*entry))

if __name__ == '__main__':
    main()

