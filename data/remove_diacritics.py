# -*- coding: utf-8 -*-
"""
14 May 2019
Remove diacritics from text
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
from sf_kw import remove_accents

def main(args=None):
    parser = ArgumentParser(description='Remove diacritics from text')
    parser.add_argument('--inpath', help='The input path')
    parser.add_argument('--outpath', help='The output path')
    parser.add_argument('--remove_char_class', choices=['none', 'combining', 'tones'], default='none',
                        help='The character class to be removed (none, all combining, or only tones)')
    args = parser.parse_args(args)
    with open(args.inpath, 'r') as infile:
        with open(args.outpath, 'w') as outfile:
            for line in infile:
                text = remove_accents(line.strip(), args.remove_char_class)
                outfile.write('{}\n'.format(text))

if __name__ == '__main__':
    main()

