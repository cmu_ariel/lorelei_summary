#!/usr/bin/env python3

import sys
import lxml.etree as ET

def main(fin):
    tree = ET.parse(fin)
    root = tree.getroot()
    for entry in root.findall('ENTRY'):
        lemma = entry.find('LEMMA')
        for gloss in entry.findall('GLOSS'):
            print('{}\t{}'.format(gloss.text, lemma.text))

if __name__ == '__main__':
    main(sys.argv[1])
