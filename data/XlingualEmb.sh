src_corp=$1;
tgt_corp=$2;
bi_dict=$3;
src_lang=$4;
tar_lang=$5;
exp_path=$6;
XlingualEmb_ROOT=/usr2/home/amuis/work/lorelei_summary/data/XlingualEmb/; # change this to where your repo is

mkdir -p "$exp_path";

echo "Mixing up $src_corp and $tgt_corp ..."
# mix monolingual corpus from src and tgt
python "$XlingualEmb_ROOT"/mix_corpus.py \
    -src "$src_corp" \
    -tgt "$tgt_corp" \
    -src_name "$src_lang" \
    -tgt_name "$tar_lang" \
    -out "$exp_path"/mono_"$src_lang"_"$tar_lang".txt \
    | tee "$exp_path"/run.log

echo "Adding prefix to $bi_dict ... "
# add prefix to bi-dict, we need to do this to let the model know which langauge each word is from
python "$XlingualEmb_ROOT"/prefix_dict.py \
    -bi_dict "$bi_dict" \
    -src_name "$src_lang" \
    -tgt_name "$tar_lang" \
    -out "$exp_path"/prefix_"$src_lang"_"$tar_lang".txt \
    | tee -a "$exp_path"/run.log

echo "Running XlingualEmb ..."
# run XlingualEmb executable
"$XlingualEmb_ROOT"/xlingemb \
    -train "$exp_path"/mono_"$src_lang"_"$tar_lang".txt \
    -output "$exp_path"/"$src_lang"_"$tar_lang".emb.txt \
    -size 100 \
    -window 10 \
    -iter 15 \
    -negative 25 \
    -sample 0.0001 \
    -alpha 0.025 \
    -cbow 1 \
    -threads 16 \
    -dict "$exp_path"/prefix_"$src_lang"_"$tar_lang".txt \
    -outputn "$exp_path"/"$src_lang"_"$tar_lang".context.txt \
    -reg 0.01 \
    | tee -a "$exp_path"/run.log

echo "Spliting the resulting embeddings"
# split the results by language
python3 "$XlingualEmb_ROOT"/split_dict.py \
    -raw_file "$exp_path"/"$src_lang"_"$tar_lang".emb.txt \
    -src_name "$src_lang" \
    -tgt_name "$tar_lang" \
    -src_file_out "$exp_path"/xlingual."$src_lang".emb.txt \
    -tgt_file_out "$exp_path"/xlingual."$tar_lang".emb.txt \
    | tee -a "$exp_path"/run.log
