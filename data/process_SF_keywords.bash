# 10 Apr 2019
# By: Aldrian Obaja Muis
# Process SF keywords for each language

script_dir=$(dirname $(readlink -f $0))
if [ -z "${outdir+x}" ]; then
    outdir="langpacks"
fi
mkdir -p ${outdir}
outdir_abs=$(readlink -f ${outdir})
echo "script_dir: ${script_dir}"
echo "outdir: ${outdir}"
echo "outdir_abs: ${outdir_abs}"

for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL"; do
    dict_path1="${outdir}/${lang}/dictionaries/lexicon_ldc_clean.tab"
    #dict_path2="${outdir}/${lang}/dictionaries/lexicon_uw_2017.tab"
    dict_path2=""
    #keyword_dir_path="${outdir}/${lang}/IL_keywords_ldc_clean+uw_2017"
    keyword_dir_path="${outdir}/${lang}/IL_keywords"
    mkdir -p "${keyword_dir_path}"
    python3 ../code/process_SF_keywords.py \
        --dict_paths ${dict_path1} ${dict_path2} \
        --eng_kw_dir_in "eng_extended_cleaned" \
        --il_kw_dir_out "${keyword_dir_path}" \
        --lang ${lang}
done
