# -*- coding: utf-8 -*-
"""
10 Apr 2019
Convert LLF format into TAB format
This is used for converting Dictionary into Tab format
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
from bs4 import BeautifulSoup as BS

def main(args=None):
    parser = ArgumentParser(description='Convert LLF into TAB')
    parser.add_argument('--inpaths', nargs='+', default=[], required=True,
                        help='The list of paths to be converted')
    parser.add_argument('--outpath', default='en_il_lexicon.tab',
                        help='The out path of the dictionary')
    parser.add_argument('--outformat', default='en_il', choices=['en_il', 'il_en'],
                        help='Whether to output in English-IL format or IL-English')
    parser.add_argument('--lang', default='',
                        help='The language name of this dictionary, for printing')
    args = parser.parse_args(args)
    entries = []
    for inpath in args.inpaths:
        with open(inpath, 'r') as infile:
            soup = BS(infile, 'lxml')
        for entry in soup.find_all('entry'):
            try:
                il_word = entry.word.text
            except:
                il_word = entry.lemma.text
            try:
                en_word = entry.definition.text
            except:
                en_word = entry.gloss.text
            en_word = en_word.replace('\t', ' ')
            il_word = il_word.replace('\t', ' ')
            if args.outformat == 'en_il':
                entries.append((en_word, il_word))
            else:
                entries.append((il_word, en_word))
    print('{}: lexicon size: {}'.format(args.lang, len(entries)))
    with open(args.outpath, 'w') as outfile:
        for entry in entries:
            outfile.write('{}\t{}\n'.format(*entry))

if __name__ == '__main__':
    main()

