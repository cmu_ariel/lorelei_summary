# -*- coding: utf-8 -*-
"""
16 May 2019
To convert gold SF JSON file into NI annotation format
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import json
import random

def main(args=None):
    parser = ArgumentParser(description='To convert gold SF JSON file into NI annotation format')
    parser.add_argument('--json_in',
                        help='The path to gold SF JSON file')
    parser.add_argument('--json_out',
                        help='The path to the output JSON file')
    parser.add_argument('--limit', type=int,
                        help='The maximum number of examples')
    parser.add_argument('--seed', type=int,
                        help='The seed for the random number generator')
    args = parser.parse_args(args)
    random.seed(args.seed)
    with open(args.json_in, 'r') as infile:
        sfs = json.load(infile)
    ni_annotations = []
    for sf in sfs:
        if 'Text' in sf:
            annotation = {}
            annotation['TypeList'] = [sf['Type']]
            annotation['NegativeTypeList'] = []
            annotation['DocumentID'] = sf['DocumentID']
            annotation['SegmentID'] = sf['SegmentID']
            annotation['Text'] = sf['Text']
            ni_annotations.append(annotation)
    print('{}: {} annotations'.format(args.json_in, len(ni_annotations)))
    random.shuffle(ni_annotations)
    with open(args.json_out, 'w') as outfile:
        json.dump(ni_annotations[:args.limit], outfile, ensure_ascii=False, indent=4)

if __name__ == '__main__':
    main()

