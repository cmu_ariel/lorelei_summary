# -*- coding: utf-8 -*-
"""
08 May 2019
Check annotation filelist, take the minimum
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import os
from lor_utils import read_sf_annos

lang_code_map = {'Akan':        ('AKA',  None),
                 'Amharic':     ('AMH',  None),
                 'Arabic':      ('ARA',  None),
                 'Bengali':     ('BEN',  None),
                 'Farsi':       ('FAS',  None),
                 'Hausa':       ('HAU',  None),
                 'Hindi':       ('HIN',  None),
                 'Hungarian':   ('HUN',  None),
                 'Indonesian':  ('IND',  None),
                 'Mandarin':    ('CMN',  None),
                 'Russian':     ('RUS',  None),
                 'Somali':      ('SOM',  None),
                 'Spanish':     ('SPA',  None),
                 'Swahili':     ('SWA',  None),
                 'Tamil':       ('TAM',  None),
                 'Tagalog':     ('TGL',  None),
                 'Thai':        ('THA',  None),
                 'Turkish':     ('TUR',  None),
                 'Urdu':        ('URD',  None),
                 'Uzbek':       ('UZB',  None),
                 'Vietnamese':  ('VIE',  None),
                 'Wolof':       ('WOL',  None),
                 'Yoruba':      ('YOR',  None),
                 'Zulu':        ('ZUL',  None),
                 'Uyghur':      ('UIG', 'IL3'),
                 'Tigrinya':    ('TIR', 'IL5'),
                 'Oromo':       ('ORM', 'IL6'),
                 'Kinyarwanda': ('KIN', 'IL9'),
                 'Sinhala':     ('SIN', 'IL10'),
                 'IL3':         ('UIG', 'IL3'),
                 'IL5':         ('TIR', 'IL5'),
                 'IL6':         ('ORM', 'IL6'),
                 'IL6_orig':    ('ORM', 'IL6'),
                 'IL9':         ('KIN', 'IL9'),
                 'IL10':        ('SIN', 'IL10'),
                 'English':     ('ENG', None)
                }

def main(args=None):
    parser = ArgumentParser(description='')
    args = parser.parse_args(args)
    filelists = {}
    for language, (lang_code, il_code) in lang_code_map.items():
        if il_code is not None:
            # Ignore ILs
            continue
        path = '/home/data/English_Core_Data_translations/{}.list'.format(lang_code.lower())
        with open(path, 'r') as infile:
            filelist = []
            for line in infile:
                filelist.append(line.strip())
            filelists[lang_code] = set(filelist)
    intersection = None
    union = None
    for lang_code, filelist in filelists.items():
        if intersection is None:
            intersection = set(filelist)
            union = set(filelist)
        else:
            intersection = set(filelist).intersection(intersection)
            union = set(filelist).union(union)
    os.makedirs('missing', exist_ok=True)
    for lang_code, filelist in sorted(filelists.items()):
        print('{}: {} documents'.format(lang_code, len(filelist)))
        with open('missing/{}'.format(lang_code), 'w') as outfile:
            for doc_id in union.difference(set(filelist)):
                outfile.write('{}\n'.format(doc_id))
    print('Intersection: {} documents'.format(len(intersection)))
    with open('engcore_intersection_20190508.list', 'w') as outfile:
        for doc_id in sorted(intersection):
            outfile.write('{}\n'.format(doc_id))
    sf_anno_dir = '/usr0/home/data/English_SF_Annotation/2017_English_Core_Data/sf_anno_LoReHLT17_3/'
    sfs = read_sf_annos(sf_anno_dir, outtype='dict')
    docs_with_sfs = set(sfs.keys())
    docs_in_intersection_not_annotated = intersection.difference(docs_with_sfs)
    docs_annotated_not_in_intersection = docs_with_sfs.difference(intersection)
    print('Annotated docs not translated in some languages: {}'.format(len(docs_annotated_not_in_intersection)))
    print('Translated docs that have no annotations: {}'.format(len(docs_in_intersection_not_annotated)))
    for doc_id in sorted(docs_annotated_not_in_intersection):
        missing_from = []
        for lang_code, filelist in filelists.items():
            if doc_id not in filelist:
                missing_from.append(lang_code)
        missing_from = sorted(missing_from, key=lambda x: (x not in ['THA', 'TAM', 'TGL', 'URD', 'BEN'], x))
        print('{} missing from: {}'.format(doc_id, missing_from))

if __name__ == '__main__':
    main()

