# -*- coding: utf-8 -*-
"""
10 Apr 2019
Convert masterlex format into TAB format
This is used for converting Dictionary from UW into Tab format
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import pandas as pd

def main(args=None):
    parser = ArgumentParser(description='Convert LLF into TAB')
    parser.add_argument('--inpath', required=True,
                        help='The file path to be converted')
    parser.add_argument('--outpath', default='en_il_lexicon.tab',
                        help='The out path of the dictionary')
    parser.add_argument('--outformat', default='en_il', choices=['en_il', 'il_en'],
                        help='Whether to output in English-IL format or IL-English')
    parser.add_argument('--lang', default='',
                        help='The language name of this dictionary, for printing')
    args = parser.parse_args(args)
    lexicon = pd.read_csv(args.inpath, sep='\t', header=None, dtype='object', error_bad_lines=False)
    lexicon.columns = ['Ortho', 'Lemma', 'PoS', 'Transliteration', 'Pronunciation', 'Translation', 'Score',
                       'Dialect', 'Domain', 'Source', 'Variants']
    if args.outformat == 'en_il':
        entries = list(zip(*(lexicon['Translation'], lexicon['Ortho'], lexicon.index)))
    elif args.outformat == 'il_en':
        entries = list(zip(*(lexicon['Ortho'], lexicon['Translation'], lexicon.index)))
    else:
        raise ValueError('Outformat {} not recognized'.format(args.outformat))
    # entries = [(str(a), str(b), str(c)) for a, b, c in entries]
    print('{}: lexicon size: {}'.format(args.lang, len(entries)))
    with open(args.outpath, 'w') as outfile:
        for entry in entries:
            outfile.write('{}\t{}\n'.format(*entry))

if __name__ == '__main__':
    main()

