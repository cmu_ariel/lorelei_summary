# 08 Jun 2018
# By: Aldrian Obaja Muis
# Get Xlingual word embedding

script_dir=$(dirname $(readlink -f $0))
outdir="langpacks"
mkdir -p ${outdir}
outdir_abs=$(readlink -f ${outdir})
echo "script_dir: ${script_dir}"
echo "outdir: ${outdir}"
echo "outdir_abs: ${outdir_abs}"
# Convert LTF into simple text form
# In the following, IL{3,5,6,9,10} are used since some file names use ilN format, where other languages uses the
# 3-letter language code.
#for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
#            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL" "ENG"; do
for lang in "UKR" "URD"; do
    echo "Creating corpora for ${lang}..."
    for dataset in "set1" "set2" "set3" "setE" "core_data"; do
        echo "Doing ${dataset}..."
        datadir="${outdir}/${lang}/${dataset}/IL/ltf"
        marker_path="${outdir}/${lang}/${dataset}/IL/.rsd_marker"
        if [ ! -e ${datadir} ]; then
            echo "${datadir} does not exist!"
            continue
        fi
        if [ -f ${marker_path} ]; then
            echo "Already done. Skipping."
            continue
        fi
        python3 /usr2/home/amuis/codebase/sf/utils/ltf_converter.py \
            --in-dir ${datadir} \
            --out-dir ${outdir}/${lang}/${dataset}/IL/rsd \
            --out-format text \
            && touch ${marker_path}
    done
done

echo "Linking the ReliefWeb dataset"
reliefweb_path="${outdir}/ENG/reliefweb.txt"
if [ ! -e ${reliefweb_path} ]; then
    ln -s /usr2/data/ruochenx/SF/bi_emb/bilingual_emb_LORELEI/data/rsd.eng.txt ${reliefweb_path}
fi

date +"[%c] Done getting txt files"

# To ensure the random source is fixed and reproducible
get_seeded_random()
{
  seed="$1"
  openssl enc -aes-256-ctr -pass pass:"$seed" -nosalt \
    </dev/zero 2>/dev/null
}

#for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
#            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL"; do
for lang in "UKR" "URD"; do
    # Simulate CP1
    prefix="${outdir}/${lang}/word_embeddings/"
    subset="core"
    if [ ! -e ${prefix}/${subset} ]; then
        mkdir -p "${prefix}/${subset}"
        echo "Doing ${subset}"
        find -L ${outdir}/${lang}/core_data/IL/rsd -type f | xargs cat > "${prefix}/${subset}/text.raw.txt"
        cat "${prefix}/${subset}/text.raw.txt" | sort | uniq | shuf --random-source <(get_seeded_random 31) \
            > "${prefix}/${subset}/text.txt"
        date +"[%c] Done ${subset}"
    fi
    subset="set1_core"
    if [ ! -e ${prefix}/${subset} ]; then
        mkdir -p "${prefix}/${subset}"
        echo "Doing ${subset}"
        find -L ${outdir}/${lang}/core_data/IL/rsd ${outdir}/${lang}/set1/IL/rsd -type f | xargs cat \
            > "${prefix}/${subset}/text.raw.txt"
        cat "${prefix}/${subset}/text.raw.txt" | sort | uniq | shuf --random-source <(get_seeded_random 31) \
            > "${prefix}/${subset}/text.txt"
        date +"[%c] Done ${subset}"
    fi
    subset="set12_core"
    if [ ! -e ${prefix}/${subset} ]; then
        mkdir -p "${prefix}/${subset}"
        echo "Doing ${subset}"
        find -L ${outdir}/${lang}/core_data/IL/rsd ${outdir}/${lang}/set{1,2}/IL/rsd -type f | xargs cat \
            > "${prefix}/${subset}/text.raw.txt"
        cat "${prefix}/${subset}/text.raw.txt" | sort | uniq | shuf --random-source <(get_seeded_random 31) \
            > "${prefix}/${subset}/text.txt"
        date +"[%c] Done ${subset}"
    fi
    subset="set123_core"
    if [ ! -e ${prefix}/${subset} ]; then
        mkdir -p "${prefix}/${subset}"
        echo "Doing ${subset}"
        find -L ${outdir}/${lang}/core_data/IL/rsd ${outdir}/${lang}/set{1,2,3}/IL/rsd -type f | xargs cat \
            > "${prefix}/${subset}/text.raw.txt"
        cat "${prefix}/${subset}/text.raw.txt" | sort | uniq | shuf --random-source <(get_seeded_random 31) \
            > "${prefix}/${subset}/text.txt"
        date +"[%c] Done ${subset}"
    fi
done
# English
prefix="${outdir}/ENG"
if [ ! -e ${prefix}/reliefweb_core.txt ]; then
    echo "Doing English (${prefix}/reliefweb_core.txt)"
    # The eng_ortho_reliefweb.txt is a symbolic link to Ruochen's symbolic link to ReliefWeb dataset
    # The origin seems to be /home/data/misc_resources/reliefweb/reliefweb_corpus_raw_v1.1_20170717_lines.json.gz
    find -L ${reliefweb_path} ${outdir}/ENG/core_data/IL/rsd -type f | xargs cat > ${prefix}/reliefweb_core.raw.txt
    cat ${prefix}/reliefweb_core.raw.txt | sort | uniq | shuf --random-source <(get_seeded_random 31) \
        > ${prefix}/reliefweb_core.txt
    date +"[%c] Done English"
fi
date +"[%c] Done corpus creation"

# Run XlingualEmb
lexicon_type="ldc"
if [ "${lexicon_type}" == "ldc" ]; then
    output_suffix=""
    lexicon_name="lexicon_ldc_clean.tab"
elif [ "${lexicon_type}" == "ldc+uw" ]; then
    output_suffix=".ldc+uw"
    lexicon_name="lexicon_ldc_clean+uw_2017.tab"
else
    echo "Unrecognized lexicon_type: ${lexicon_type}"
    exit 1
fi
for subset in "set1_core" "set12_core"; do
    #for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
    #            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL"; do
    for lang in "UKR" "URD"; do
        echo "Doing Xlingual in ${lang} ${subset}"
        mkdir -p "${outdir}/${lang}/word_embeddings${output_suffix}"
        if [ -e ${outdir}/${lang}/word_embeddings${output_suffix}/${subset}/model ]; then
            echo "Already done. Skipping"
            continue
        fi
        lang_small=$(echo ${lang} | tr [A-Z] [a-z])
        time bash XlingualEmb.sh \
            ${outdir}/ENG/reliefweb.txt \
            ${outdir}/${lang}/word_embeddings${output_suffix}/${subset}/text.txt \
            ${outdir}/${lang}/dictionaries/${lexicon_name} \
            eng ${lang_small} \
            ${outdir}/${lang}/word_embeddings${output_suffix}/${subset}
        date +"[%c] Done ${lang} ${subset}"
    done
done
