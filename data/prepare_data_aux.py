# -*- coding: utf-8 -*-
"""
06 Nov 2018
To prepare the data for languages other than IL5, IL6, IL9, and IL10
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from lor_utils import read_sf_annos, read_ltf_files
from argparse import ArgumentParser
import os
import re
import shutil
import json
import math
import numpy as np
np.random.seed(31)

ann_file_map = {'IL5': ('/home/data/LDC2017E57_LORELEI_IL5_Incident_Language_References_for_Year_2_Eval_Unsequestered_'
                        'V1.0/setE/docs/annotated_filelist_SF.tab'),
                'IL6': ('/usr0/home/zsheikh/LoReHLT17/orm/CP3/IL6_unsequestered_without_outlier/'
                        'annotated_filelist_SF.tab'),
                'IL6_orig': ('/home/data/LDC2017E58_LORELEI_IL6_Incident_Language_References_for_Year_2_Eval_'
                             'Unsequestered_V1.0/setE/docs/annotated_filelist_SF.tab')}

lang_code_map = {'Akan':        ('AKA',  None),
                 'Amharic':     ('AMH',  None),
                 'Arabic':      ('ARA',  None),
                 'Bengali':     ('BEN',  None),
                 'Farsi':       ('FAS',  None),
                 'Hausa':       ('HAU',  None),
                 'Hindi':       ('HIN',  None),
                 'Hungarian':   ('HUN',  None),
                 'Indonesian':  ('IND',  None),
                 'Mandarin':    ('CMN',  None),
                 'Russian':     ('RUS',  None),
                 'Somali':      ('SOM',  None),
                 'Spanish':     ('SPA',  None),
                 'Swahili':     ('SWA',  None),
                 'Tamil':       ('TAM',  None),
                 'Tagalog':     ('TGL',  None),
                 'Thai':        ('THA',  None),
                 'Turkish':     ('TUR',  None),
                 'Ukrainian':   ('UKR',  None),
                 'Urdu':        ('URD',  None),
                 'Uzbek':       ('UZB',  None),
                 'Vietnamese':  ('VIE',  None),
                 'Wolof':       ('WOL',  None),
                 'Yoruba':      ('YOR',  None),
                 'Zulu':        ('ZUL',  None),
                 'Uyghur':      ('UIG', 'IL3'),
                 'Tigrinya':    ('TIR', 'IL5'),
                 'Oromo':       ('ORM', 'IL6'),
                 'Kinyarwanda': ('KIN', 'IL9'),
                 'Sinhala':     ('SIN', 'IL10'),
                 'IL3':         ('UIG', 'IL3'),
                 'IL5':         ('TIR', 'IL5'),
                 'IL6':         ('ORM', 'IL6'),
                 'IL6_orig':    ('ORM', 'IL6'),
                 'IL9':         ('KIN', 'IL9'),
                 'IL10':        ('SIN', 'IL10'),
                }

GENRES = {
          'DF':'', # discussion forum data
          'NW':'', # news and general text harvested from news sites
          'RF':'', # reference text (wikipedia, religious texts, technical documents, etc.)
          'SN':'',  # "social network" data (i.e., Twitter)
          'WL':'', # weblog and newsgroup data
          'NA':'', # Others: transcribed speech data (only in Wolof)
          'NG':'', # Others: News Group (only in Mandarin: Google Groups and mitbbn.com)
          }

GENRES_PATTERN = '({})'.format('|'.join(sorted(GENRES)))

def parse_doc_id(doc_id):
    """Parse the information in the document ID into its components.

    Standard pattern: LANGCODE_GENRE_SOURCE_DATE_UIN
    Translated pattern: GENRE_SOURCE_LANGSOURCE_UIN_DATE
    """
    if doc_id.endswith('.ltf.xml'):
        doc_id = doc_id[:-len('.ltf.xml')]
    underscore_count = doc_id.count('_')
    pattern_translated = GENRES_PATTERN+r'_([A-Za-z0-9]{3})_([A-Za-z]{3}|IL[0-9]+)_([A-Za-z0-9]+)_([A-Za-z0-9]+)(?:\.([A-Za-z]{3}))?.*'
    pattern_standard = '([A-Za-z]{3}|IL[0-9]+)_'+GENRES_PATTERN+'_([A-Za-z0-9]+)_([A-Za-z0-9]+)_([A-Za-z0-9]+).*'
    lang, genre, source, yearmonth, uin = [None]*5
    match = re.match(pattern_standard, doc_id)
    if match:
        lang = match.group(1)
        genre = match.group(2)
        source = match.group(3)
        yearmonth = match.group(4)
        uin = match.group(5)
    else:
        match = re.match(pattern_translated, doc_id)
        if match:
            lang = match.group(6)
            if lang is None:
                lang = match.group(3)
            lang = lang.upper()
            genre = match.group(1)
            source = match.group(2)
            yearmonth = match.group(5)
            uin = match.group(4)
    if lang is None:
        print('doc_id format unknown: {}'.format(doc_id))
    return lang, genre, source, yearmonth, uin

def sample_documents(docs, n_words=100000, sn_word_count=20000, shuffle=True):
    """
    
    n_words: the number of words desired in the resulting dataset
    sn_word_count: the number of words from tweets
    shuffle: whether to shuffle or ordered as is
    """
    total_docs = len(docs)
    sn_docs = []
    nonsn_docs = []
    for doc in docs:
        if doc.doc_id is not None:
            # Document
            genre = parse_doc_id(doc.doc_id)[1]
        else:
            # LazyDocument
            doc_id = os.path.split(doc.path)[1]
            if doc_id.endswith('.ltf.xml'):
                doc_id = doc_id[:-len('.ltf.xml')]
            genre = parse_doc_id(doc_id)[1]
            doc.doc_id = doc_id
        if genre == 'SN':
            sn_docs.append(doc)
        else:
            nonsn_docs.append(doc)
    if shuffle:
        np.random.shuffle(sn_docs)
        np.random.shuffle(nonsn_docs)

    samples = []
    other_docs = []

    sn_cur_word_count = 0
    sn_doc_idx = 0
    for sn_doc_idx, doc in enumerate(sn_docs):
        for segment in doc:
            sn_cur_word_count += len(segment)
        samples.append(doc)
        if sn_cur_word_count >= sn_word_count:
            break
    cur_word_count = sn_cur_word_count
    for doc_idx, doc in enumerate(nonsn_docs):
        for segment in doc:
            cur_word_count += len(segment)
        samples.append(doc)
        if cur_word_count >= n_words:
            break
    if cur_word_count < n_words:
        # If the non-SN documents cannot cover the n_words
        for sn_doc_idx in range(sn_doc_idx, len(sn_docs)):
            doc = sn_docs[sn_doc_idx]
            for segment in doc:
                sn_cur_word_count += len(segment)
                cur_word_count += len(segment)
            samples.append(doc)
            if cur_word_count >= n_words:
                break
    other_docs.extend(sn_docs[sn_doc_idx:])
    other_docs.extend(nonsn_docs[doc_idx:])

    return samples, other_docs, cur_word_count, sn_doc_idx, sn_cur_word_count

def main(args=None):
    basedir = '/home/data'
    outdir = '.'
    parser = ArgumentParser(description='')
    parser.add_argument('--basedir', default=basedir,
                        help='The base directory containing all languages')
    parser.add_argument('--outdir', default=outdir,
                        help='The base output directory to collect the language packs.')
    parser.add_argument('--dryrun', action='store_true',
                        help='Whether to just calculate statistics, without copying data')
    parser.add_argument('--update_cache', action='store_true',
                        help='Whether to update cache of statistics information')
    parser.add_argument('--lang_codes', nargs='+', default=[],
                        help='Process only these language codes. If empty, process all')
    args = parser.parse_args(args)
    basedir = args.basedir
    outdir = args.outdir
    dryrun = args.dryrun
    update_cache = args.update_cache
    lang_codes = args.lang_codes
    lang_to_ann_dir = {}
    lang_to_ltf_dir = {}
    lang_to_base_dir = {}
    for dirname in sorted(os.listdir(basedir)):
        try:
            lang = dirname.split('_')[2]
            if lang == 'IL6':
                lang = 'IL6_orig'
        except:
            pass
        if dirname.startswith('REFLEX_'):
            lang = dirname.split('_')[1]
            if lang not in lang_to_base_dir:
                lang_to_base_dir[lang] = '{}/{}'.format(basedir, dirname)
                lang_to_ann_dir[lang] = '{}/{}/data/annotation/situation_frame'.format(basedir, dirname)
                lang_to_ltf_dir[lang] = '{}/{}/data/monolingual_text/ltf'.format(basedir, dirname)
        if 'Representative_Language_Pack_Translation_Annotation_Grammar_Lexicon_and_Tools_V' in dirname:
            lang_to_base_dir[lang] = '{}/{}'.format(basedir, dirname)
            lang_to_ann_dir[lang] = '{}/{}/data/annotation/situation_frame'.format(basedir, dirname)
        if 'Monolingual_Text' in dirname:
            lang_to_ltf_dir[lang] = '{}/{}/data/monolingual_text/ltf'.format(basedir, dirname)
        if 'BOLT_LRL' in dirname:
            lang = dirname.split('_')[3]
            lang_to_ltf_dir[lang] = '{}/{}/data/monolingual_text/ltf'.format(basedir, dirname)
        if dirname == 'LDC2016E30_LORELEI_Mandarin_Incident_Language_Pack_V2.0':
            # This is superseded by the Representative Language Pack above
            # lang_to_ann_dir[lang] = '{}/{}/setE/data/annotation/situation_frame'.format(basedir, dirname)
            # lang_to_ltf_dir[lang] = '{}/{}/setE/data/monolingual_text/ltf'.format(basedir, dirname)
            continue
        if dirname == ('LDC2016E75_LORELEI_IL3_Dual_Annotation_for_Simple_Named_Entity_'
                       'and_Situation_Frame_Unsequestered'):
            lang_to_ann_dir[lang] = '{}/{}/data/situation_frame/version2'.format(basedir, dirname)
        if 'Incident_Language_References_for_Year' in dirname:
            lang_to_base_dir[lang] = '{}/{}/setE'.format(basedir, dirname)
            if 'IL5' in dirname or 'IL6' in dirname:
                lang_to_ann_dir[lang] = '{}/{}/setE/data/annotation/situation_frame'.format(basedir, dirname)
            if 'IL9' in dirname:
                lang_to_ann_dir[lang] = '{}/{}/setE/data/annotation/il9/situation_frame'.format(basedir, dirname)
            if 'IL10' in dirname:
                lang_to_ann_dir[lang] = '{}/{}/setE/data/annotation/il10/situation_frame'.format(basedir, dirname)
        if 'Incident_Language_Pack_for_Year' in dirname:
            if 'IL3' in dirname or 'IL5' in dirname or 'IL6' in dirname:
                lang_to_ltf_dir[lang] = '{}/{}/setE/data/monolingual_text/ltf'.format(basedir, dirname)
            if 'IL9' in dirname:
                lang_to_ltf_dir[lang] = '{}/{}/setE/data/monolingual_text/il9/ltf'.format(basedir, dirname)
            if 'IL10' in dirname:
                lang_to_ltf_dir[lang] = '{}/{}/setE/data/monolingual_text/il10/ltf'.format(basedir, dirname)
    lang_to_ann_dir['IL6'] = '/usr0/home/zsheikh/LoReHLT17/orm/CP3/IL6_unsequestered_without_outlier'
    lang_to_ltf_dir['IL6'] = lang_to_ltf_dir['IL6_orig']
    # lang_to_ann_dir = {'IL3': 'IL3/setE/IL/sf_anno'}
    # lang_to_ltf_dir = {'IL3': 'IL3/setE/IL/ltf'}
    for lang in list(lang_to_ltf_dir):
        if lang not in lang_code_map:
            del lang_to_ltf_dir[lang]
            if lang in lang_to_ann_dir:
                del lang_to_ann_dir[lang]
            if lang in lang_to_base_dir:
                del lang_to_base_dir[lang]
    for lang, ltf_dir in sorted(lang_to_ltf_dir.items(), key=lambda x:lang_code_map[x[0]][0]):
        lang_code, lang_code_alias = lang_code_map.get(lang, [None, None])
        if lang_codes and lang_code not in lang_codes:
            continue
        sf_anno_dir = lang_to_ann_dir.get(lang, '')
        if not os.path.exists(ltf_dir):
            print('Language {} has no LTF'.format(lang))
            continue
        if not os.path.exists(sf_anno_dir):
            print('Language {} has no SF annotations'.format(lang))
            # continue
        print('{}: {}'.format(lang, sf_anno_dir))

        if lang == 'IL6_orig':
            cache_path = '{}/{}/stats_orig.log'.format(outdir, lang_code)
        else:
            cache_path = '{}/{}/stats.log'.format(outdir, lang_code)
        cache_exists = os.path.exists(cache_path)
        if cache_exists and dryrun and not update_cache:
            # Special case: if only getting statistics, no need to read the data, simply print
            with open(cache_path, 'r') as infile:
                stats = json.load(infile)
        else:
            stats = {}
            # Read the data
            docs = read_ltf_files(ltf_dir, lazy=True)
            ltf_doc_ids = []
            for filename in os.listdir(ltf_dir):
                ltf_doc_ids.append(filename.split('.')[0])
            ltf_doc_ids = set(ltf_doc_ids)
            if not os.path.exists(sf_anno_dir):
                sfs = []
            else:
                sfs = read_sf_annos(sf_anno_dir)
            if lang in ['IL5', 'IL6']:
                ann_info_file = ann_file_map[lang]
                ann_doc_ids = set()
                with open(ann_info_file, 'r') as infile:
                    for line in infile:
                        if line.startswith('doc_id'):
                            continue
                        ann_doc_ids.add(line.rstrip('\n').split('\t')[0])
                ann_doc_ids = list(sorted(ann_doc_ids))
            else:
                ann_doc_ids = list(sorted(set(sf.doc_id for sf in sfs)))
            ann_doc_paths = []
            for doc_id in ann_doc_ids:
                if doc_id not in ltf_doc_ids:
                    # Corresponding LTF file not found in the Monolingual Pack, search in translation pack
                    trans_path = '{}/translation'.format('/'.join(sf_anno_dir.split('/')[:-2]))
                    for cur_dir, dirnames, filenames in os.walk(trans_path):
                        filename = '{}.ltf.xml'.format(doc_id)
                        if filename in filenames:
                            ann_doc_paths.append('{}/{}'.format(cur_dir, filename))
                            break
                    else:
                        print('doc_id not found: {}'.format(doc_id))
                else:
                    path = '{}/{}.ltf.xml'.format(ltf_dir, doc_id)
                    ann_doc_paths.append(path)
            ann_docs = read_ltf_files(ann_doc_paths, outtype='dict', progress_bar=False)
            user_ids = list(sorted(set(sf.user_id for sf in sfs)))
            num_segments = sum(len(doc) for doc in ann_docs.values())
            stats['#doc'] = len(docs)
            stats['#docann'] = len(ann_doc_paths)
            stats['#SFs'] = len(sfs)
            stats['#annotators'] = len(user_ids)
            stats['#sents'] = num_segments
            stats['ltf_path'] = ltf_dir
            stats['ann_path'] = sf_anno_dir
            genre_counts = {}
            for doc in docs:
                path = doc.path
                _, docname = os.path.split(path)
                doc_lang, genre, source, yearmonth, uin = parse_doc_id(docname)
                if genre not in genre_counts:
                    genre_counts[genre] = 0
                genre_counts[genre] += 1
            stats['genre_counts'] = genre_counts
            if update_cache or not cache_exists:
                with open(cache_path, 'w') as outfile:
                    json.dump(stats, outfile)
        genre_count_str = ' '.join('#{}={}'.format(genre,
                                                   stats['genre_counts'].get(genre, 0))
                                   for genre in sorted(GENRES))
        print('{}: #doc={} #docann={} #SFs={} #annotators={} #sents={} {}'.format(
              lang,
              stats['#doc'],
              stats['#docann'],
              stats['#SFs'],
              stats['#annotators'],
              stats['#sents'],
              genre_count_str,
              ))

        #######################
        # Do the data copying #
        #######################
        if lang_code_alias is not None or lang_code is None:
            print('Skipping copying IL3, IL5, IL6, IL9, IL10, since it has been copied elsewhere')
            continue
        if dryrun:
            for setnum in [1, 2, 3]:
                print('{0} set{1}_docs={2} set{1}_words={3} set{1}_sn_docs={4} set{1}_sn_words={5}'.format(
                            lang, setnum, stats['#set{}_docs'.format(setnum)],
                                          stats['#set{}_word_count'.format(setnum)],
                                          stats['#set{}_sn_docs'.format(setnum)],
                                          stats['#set{}_sn_word_count'.format(setnum)],
                                          ))
            continue
        ann_basedir = lang_to_base_dir.get(lang, None)
        if ann_basedir is not None:
            # Copy the EDL file
            edl_inpath = os.path.join(ann_basedir, 'data', 'annotation', 'entity', '{}_edl.tab'.format(lang_code.lower()))
            if not os.path.exists(edl_inpath):
                print('EDL path does not exist: {}'.format(edl_inpath))
            edl_outpath = os.path.join(outdir, lang_code, 'setE', 'gold_edl.tab')
            os.makedirs(os.path.join(outdir, lang_code, 'setE'), exist_ok=True)
            try:
                os.symlink(edl_inpath, edl_outpath)
            except FileExistsError:
                pass

            # Copy the annotated filelist
            annfile_inpath = os.path.join(ann_basedir, 'docs', 'annotated_filelist_SF.tab')
            if not os.path.exists(annfile_inpath):
                print('Annotated filelist path does not exist: {}'.format(annfile_inpath))
            annfile_outpath = os.path.join(outdir, lang_code, 'setE', 'IL', 'sf_anno', 'annotated_filelist_SF.tab')
            try:
                os.symlink(annfile_inpath, annfile_outpath)
            except FileExistsError:
                pass

            # Copy the situation frames annotations
            sf_anno_outdir = os.path.join(outdir, lang_code, 'setE', 'IL', 'sf_anno')
            for subdir in ['issues', 'needs', 'mentions']:
                os.makedirs(os.path.join(sf_anno_outdir, subdir), exist_ok=True)
                sf_anno_indir = os.path.join(sf_anno_dir, subdir)
                if not os.path.exists(sf_anno_indir):
                    print('SF anno path does not exist: {}'.format(sf_anno_indir))
                    break
                for filename in os.listdir(sf_anno_indir):
                    sf_anno_outpath = os.path.join(sf_anno_outdir, subdir, filename)
                    sf_anno_inpath = os.path.join(sf_anno_dir, subdir, filename)
                    try:
                        os.symlink(sf_anno_inpath, sf_anno_outpath)
                    except FileExistsError:
                        pass

            # Copy the translated files into English
            has_translation = True
            trans_indir = os.path.join(ann_basedir, 'data', 'translation', 'from_{}'.format(lang_code.lower()), 'eng',
                                       'ltf')
            if not os.path.exists(trans_indir):
                trans_indir = os.path.join(ann_basedir, 'data', 'translation', 'eng', 'ltf')
                if not os.path.exists(trans_indir):
                    print('Translation directory does not exist: {}'.format(trans_indir))
                    has_translation = False
            trans_outdir = os.path.join(outdir, lang_code, 'setE', 'ENG_from_IL', 'ltf')
            os.makedirs(trans_outdir, exist_ok=True)
            if has_translation:
                for filename in os.listdir(trans_indir):
                    trans_outpath = os.path.join(trans_outdir, filename)
                    trans_inpath = os.path.join(trans_indir, filename)
                    try:
                        os.symlink(trans_inpath, trans_outpath)
                    except FileExistsError:
                        pass

            # Copy the LTF files for setE (those which are annotated)
            ltf_outdir = os.path.join(outdir, lang_code, 'setE', 'IL', 'ltf')
            os.makedirs(ltf_outdir, exist_ok=True)
            for path in ann_doc_paths:
                filename = os.path.split(path)[1]
                out_path = os.path.join(ltf_outdir, filename)
                try:
                    os.symlink(path, out_path)
                except FileExistsError:
                    pass

        # Copy the LTF files for set1 (100k words) and set2 (100k words)
        other_docs = [doc for doc in docs if doc.path not in ann_doc_paths]
        for setnum in [1, 2, 3]:
            if setnum < 3:
                n_words = 1e5
                sn_word_count = 2e4
            else:
                n_words = 2e5
                sn_word_count = 4e4
            set_docs, other_docs, set_word_count, sn_doc_count, sn_word_count = sample_documents(other_docs,
                                                                                         n_words=n_words,
                                                                                         sn_word_count=sn_word_count)
            stats['#set{}_docs'.format(setnum)] = len(set_docs)
            stats['#set{}_word_count'.format(setnum)] = set_word_count
            stats['#set{}_sn_docs'.format(setnum)] = sn_doc_count
            stats['#set{}_sn_word_count'.format(setnum)] = sn_word_count
            ltf_outdir = os.path.join(outdir, lang_code, 'set{}'.format(setnum), 'IL', 'ltf')
            os.makedirs(ltf_outdir, exist_ok=True)
            for doc in set_docs:
                path = doc.path
                filename = os.path.split(path)[1]
                out_path = os.path.join(ltf_outdir, filename)
                try:
                    os.symlink(path, out_path)
                except FileExistsError:
                    pass
            print('{0} set{1}_docs={2} set{1}_words={3} set{1}_sn_docs={4} set{1}_sn_words={5}'.format(
                        lang, setnum, stats['#set{}_docs'.format(setnum)],
                                      stats['#set{}_word_count'.format(setnum)],
                                      stats['#set{}_sn_docs'.format(setnum)],
                                      stats['#set{}_sn_word_count'.format(setnum)],
                                      ))
        if update_cache or not cache_exists:
            with open(cache_path, 'w') as outfile:
                json.dump(stats, outfile, indent=4, sort_keys=True)


if __name__ == '__main__':
    main()

