import sys
from os import listdir 
from os.path import join
import argparse
from random import shuffle

def add_prefix(lines, prefix):
    list_of_toks = []
    for l in lines:
        toks = l.strip().split()
        new_toks = [prefix+t for t in toks]
        list_of_toks.append(new_toks)
    return list_of_toks

def main():
    parser = argparse.ArgumentParser(description='mix two monolingual dataset')
    parser.add_argument('-src', help='path to src data')
    parser.add_argument('-tgt', help='path to tgt data')
    parser.add_argument('-src_name', help='prefix to write src data')
    parser.add_argument('-tgt_name', help='prefix to write tgt data')
    parser.add_argument('-out', help='output path')
    args = parser.parse_args()

    src_toks = add_prefix(open(args.src).readlines(), args.src_name + '_')
    tgt_toks = add_prefix(open(args.tgt).readlines(), args.tgt_name + '_')
    # print('src_toks[:10]:', src_toks[:10])
    # print('tgt_toks[:10]:', tgt_toks[:10])
    all_toks = src_toks + tgt_toks
    shuffle(all_toks)

    f_out = open(args.out, 'w')
    for toks in all_toks:
        f_out.write(' '.join(toks).lower() + '\n')

if __name__ == '__main__':
    main()