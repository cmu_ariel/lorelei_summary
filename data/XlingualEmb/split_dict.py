import argparse
import codecs
import pandas as pd
import sys
from os import listdir 
from os.path import join
from random import shuffle
from tqdm import tqdm


CHUNKSIZE = 1e4

def process_chunk(df, src_name, tgt_name):
    def extract_word(s):
        try:
            return s.split('_')[1]
        except IndexError:
            return s

    df['lang'] = df[0].map(lambda s: s.split('_')[0])
    df['word'] = df[0].map(extract_word)
    df.ix[(df['lang'] != src_name)&(df['lang'] != tgt_name), 'lang'] = 'both'
    return df

def main():
    parser = argparse.ArgumentParser(description='mix to monolingual dataset')
    parser.add_argument('-raw_file', help='path to merged xlingual embedding')
    parser.add_argument('-src_name', help='prefix to write src data')
    parser.add_argument('-tgt_name', help='prefix to write tgt data')
    parser.add_argument('-src_file_out', help='src output path')
    parser.add_argument('-tgt_file_out', help='tgt output path')
    args = parser.parse_args()

    f_tgt_out = open(args.tgt_file_out, 'w')
    f_src_buffer = ''
    f_src_counter = 0
    f_tgt_buffer = ''
    f_tgt_counter = 0
    i = 0
    # for i, l in enumerate(open(args.raw_file).readlines()):

    with codecs.open(args.raw_file, "r", encoding='utf-8', errors='ignore') as fdata:
        l = fdata.readline()
        vocabsize, dim = map(int, l.split())
        N = int(vocabsize // CHUNKSIZE)
        reader = pd.read_csv(fdata, delimiter=' ', header=None, chunksize=CHUNKSIZE,
                             keep_default_na=False, na_values=['<N/A><N/A><N/A>'])
        df = pd.concat([process_chunk(chunk, args.src_name, args.tgt_name) for chunk in tqdm(reader, total=N)])

        # for l in tqdm(fdata, total=vocab):
        #     if l[:len(args.src_name)+1] == args.src_name + '_':
        #         f_src_buffer += l[len(args.src_name)+1:]
        #         f_src_counter += 1
        #     elif l[:len(args.tgt_name)+1] == args.tgt_name + '_':
        #         f_tgt_buffer += l[len(args.tgt_name)+1:]
        #         f_tgt_counter += 1
        #     i += 1

    cols = ['word'] + [i for i in range(1, dim + 1)]
    with open(args.src_file_out, 'w') as f_src_out:
        subset = df[(df['lang'] == args.src_name)|(df['lang'] == 'both')]
        src_vocabsize = len(subset)
        f_src_out.write(str(src_vocabsize) + ' ' + str(dim) + '\n')
        
        subset[cols].to_csv(f_src_out, sep=' ', header=None, index=None, encoding='utf-8')

    with open(args.tgt_file_out, 'w') as f_tgt_out:
        subset = df[(df['lang'] == args.tgt_name)|(df['lang'] == 'both')]
        tgt_vocabsize = len(subset)
        f_tgt_out.write(str(tgt_vocabsize) + ' ' + str(dim) + '\n')
        subset[cols].to_csv(f_tgt_out, sep=' ', header=None, index=None, encoding='utf-8')

if __name__ == '__main__':
    main()
