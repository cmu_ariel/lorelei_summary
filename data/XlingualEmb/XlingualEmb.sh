src_corp=$1;
tgt_corp=$2;
bi_dict=$3;
src_lang=$4;
tar_lang=$5;
exp_path=$6;
window=$7;
XlingualEmb_ROOT=/usr2/home/notani/work/2018_official_dry_run/bilingual_emb_LORELEI/XlingualEmb/; # change this to where your repo is

if [ -z ${window} ]; then
    window=10
fi

mkdir -p "$exp_path";

echo "Mixing up $src_corp and $tgt_corp ..."
# mix monolingual corpus from src and tgt
cmd="python $XlingualEmb_ROOT/mix_corpus.py -src $src_corp -tgt $tgt_corp -src_name $src_lang -tgt_name $tar_lang -out $exp_path/mono_$src_lang_$tar_lang.txt;"
echo $cmd > $exp_path/log
echo $cmd
eval $cmd

echo "Adding prefix to $bi_dict ... "
# add prefix to bi-dict, we need to do this to let the model know which langauge each word is from
cmd="python $XlingualEmb_ROOT/prefix_dict.py -bi_dict $bi_dict -src_name $src_lang -tgt_name $tar_lang -out $exp_path/prefix_$src_lang_$tar_lang.txt;"
echo $cmd >> $exp_path/log
echo $cmd
eval $cmd

echo "Running XlingualEmb ..."
# run XlingualEmb executable
cmd="$XlingualEmb_ROOT/xlingemb -train $exp_path/mono_$src_lang_$tar_lang.txt -output $exp_path/$src_lang_$tar_lang.emb.txt  -size 100 -window ${window} -iter 15 -negative 25 -sample 0.0001 -alpha 0.025 -cbow 1 -threads 16 -dict $exp_path/prefix_$src_lang_$tar_lang.txt -outputn $exp_path/$src_lang_$tar_lang.context.txt -reg 0.01;"
echo $cmd >> $exp_path/log
echo $cmd
eval $cmd

echo "Spliting the resulting embeddings"
# split the results by language
cmd="python $XlingualEmb_ROOT/split_dict.py -raw_file $exp_path/$src_lang_$tar_lang.emb.txt -src_name $src_lang -tgt_name $tar_lang -src_file_out $exp_path/xlingual.$src_lang.emb.txt -tgt_file_out $exp_path/xlingual.$tar_lang.emb.txt;"
echo $cmd >> $exp_path/log
echo $cmd
eval $cmd

