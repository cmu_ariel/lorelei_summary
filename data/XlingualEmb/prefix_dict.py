import sys
from os import listdir 
from os.path import join
import argparse
from random import shuffle

def main():
    parser = argparse.ArgumentParser(description='mix to monolingual dataset')
    parser.add_argument('-bi_dict', help='path to bi-dict')
    parser.add_argument('-src_name', help='prefix to write src data')
    parser.add_argument('-tgt_name', help='prefix to write tgt data')
    parser.add_argument('-out', help='output path')
    args = parser.parse_args()

    f_out = open(args.out, 'w')
    for l in open(args.bi_dict):
        ss = l.strip().split('\t')
        if len(ss) < 2:
            continue
        f_out.write(args.src_name + '_' + ss[0] + '\t' + args.tgt_name + '_' + ss[1] + '\n' )

if __name__ == '__main__':
    main()
