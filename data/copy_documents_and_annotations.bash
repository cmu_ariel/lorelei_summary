# 09 Apr 2019
# By: Aldrian Obaja Muis
# Copy the documents (LTF) and annotations (EDL, SF) into a common directory structure

if [ -z "${script_dir+x}" ]; then
    script_dir=$(dirname $(readlink -f $0))
fi
if [ -z "${outdir+x}" ]; then
    outdir="langpacks"
fi

### IL3
il3_ann_langpath="/home/data/LDC2016E75_LORELEI_IL3_Dual_Annotation_for_Simple_Named_Entity_and_Situation_Frame_Unsequestered"
python3 combine_il3_dual_annotations.py \
    --outdir ${il3_ann_langpath}/data/situation_frame/ \
    --outdir ${outdir}/IL3/setE/IL/sf_anno/.
ln -s ${il3_ann_langpath}/docs/docs_dually_annotated_sf.txt \
    ${outdir}/IL3/setE/IL/sf_anno/annotated_filelist_SF.tab
python3 laf_to_tab.py \
    --indir ${il3_ann_langpath}/data/simple_named_entity/version2/ \
    --outfile ${outdir}/IL3/setE/gold_edl.tab
il3_tra_langpath="/home/data/LDC2016E70_LoReHLT_IL3_Incident_Language_References_for_Year_1_Eval_Unsequestered_V1.1"
for file in ${il3_tra_langpath}/setE/data/translation/eng/ltf/*; do
    ln -s ${file} ${outdir}/IL3/setE/ENG_from_IL/ltf/.
done
il3_ltf_langpath="/home/data/LDC2016E57_LORELEI_IL3_Incident_Language_Pack_for_Year_1_Eval"
for file in ${il3_ltf_langpath}/setE/data/monolingual_text/ltf_unsequestered_subset/*; do
    ln -s ${file} ${outdir}/IL3/setE/IL/ltf/.
done

### IL5
il5_ann_langpath="/home/data/LDC2017E57_LORELEI_IL5_Incident_Language_References_for_Year_2_Eval_Unsequestered_V1.0"
ln -s ${il5_ann_langpath}/setE/data/annotation/il5_edl.tab \
    ${outdir}/IL5/setE/gold_edl.tab
ln -s ${il5_ann_langpath}/setE/docs/annotated_filelist_SF.tab \
    ${outdir}/IL5/setE/IL/sf_anno/annotated_filelist_SF.tab
for subdir in "issues" "needs" "mentions"; do
    mkdir -p ${outdir}/IL5/setE/IL/sf_anno/${subdir}
    path=${il5_ann_langpath}/setE/data/annotation/situation_frame/${subdir}
    for file in ${path}/*; do
        ln -s ${file} ${outdir}/IL5/setE/IL/sf_anno/${subdir}/.
    done
done
for file in ${il5_ann_langpath}/setE/data/translation/eng/ltf/*; do
    ln -s ${file} ${outdir}/IL5/setE/ENG_from_IL/ltf/.
done
il5_ltf_langpath="/home/data/LDC2017E27_LORELEI_IL5_Incident_Language_Pack_for_Year_2_Eval_V1.1"
for file in ${il5_ltf_langpath}/setE/data/monolingual_text/ltf/*; do
    ln -s ${file} ${outdir}/IL5/setE/IL/ltf/.
done

### IL6
il6_ann_langpath="/home/data/LDC2017E58_LORELEI_IL6_Incident_Language_References_for_Year_2_Eval_Unsequestered_V1.0"
ln -s ${il6_ann_langpath}/setE/data/annotation/il6_edl.tab \
    ${outdir}/IL6/setE/IL/gold_edl.tab
# IL6 has outlier annotator, marking everything as regimechange. This is the original files with that outlier annotator
mkdir -p ${outdir}/IL6/setE/IL/sf_anno_original/
ln -s ${il6_ann_langpath}/setE/docs/annotated_filelist_SF.tab \
    ${outdir}/IL6/setE/IL/sf_anno_original/annotated_filelist_SF.tab
for subdir in "issues" "needs" "mentions"; do
    mkdir -p ${outdir}/IL6/setE/IL/sf_anno_original/${subdir}
    path=${il6_ann_langpath}/setE/data/annotation/situation_frame/${subdir}
    for file in ${path}/*; do
        ln -s ${file} ${outdir}/IL6/setE/IL/sf_anno_original/${subdir}/.
    done
done
for file in ${il6_ann_langpath}/setE/data/translation/eng/ltf/*; do
    ln -s ${file} ${outdir}/IL6/setE/ENG_from_IL/ltf/.
done
# This is the files without the outlier annotator
il6_ann2_langpath="/usr0/home/zsheikh/LoReHLT17/orm/CP3/IL6_unsequestered_without_outlier"
ln -s ${il6_ann2_langpath}/annotated_filelist_SF.tab \
    ${outdir}/IL6/setE/IL/sf_anno/annotated_filelist_SF.tab
for subdir in "issues" "needs" "mentions"; do
    mkdir -p ${outdir}/IL6/setE/IL/sf_anno/${subdir}
    path=${il6_ann2_langpath}/${subdir}
    for file in ${path}/*; do
        ln -s ${file} ${outdir}/IL6/setE/IL/sf_anno/${subdir}/.
    done
done
il6_ltf_langpath="/home/data/LDC2017E29_LORELEI_IL6_Incident_Language_Pack_for_Year_2_Eval_V1.1"
for file in ${il6_ltf_langpath}/setE/data/monolingual_text/ltf/*; do
    ln -s ${file} ${outdir}/IL6/setE/IL/ltf/.
done

### IL9
il9_ann_langpath="/usr3/data/shared/LoReHLT18/scripts/LoReHLT18_SF_Scorer_v1.0.4_07092018/datasets/IL9_Reference"
ln -s ${il9_ann_langpath}/setE/data/annotation/il9/il9_edl.tab \
    ${outdir}/IL9/setE/gold_edl.tab
# This is the annotated_filelist_SF.tab obtained by looking at the annotations. This unfortunately results in not
# knowing which files the annotators looked at but not give any judgment.
il9_ann2_langpath="/usr0/home/data/LoReHLT17/scripts/LoReHLT_Frame_Scorer_v2.1.0-prerelease_20170920"
ln -s ${il9_ann2_langpath}/references/kin/il9/annotated_filelist_SF.tab \
    ${outdir}/IL9/setE/IL/sf_anno/annotated_filelist_SF.tab
for subdir in "issues" "needs" "mentions"; do
    mkdir -p ${outdir}/IL9/setE/IL/sf_anno/${subdir}
    path=${il9_ann_langpath}/setE/data/annotation/il9/situation_frame/${subdir}
    for file in ${path}/*; do
        ln -s ${file} ${outdir}/IL9/setE/IL/sf_anno/${subdir}/.
    done
done
path="${il9_ann_langpath}/setE/data/translation/eng/ltf"
for file in $(ls ${path}); do
    newfile=$(echo ${file} | sed s/eng.ltf/eng_A.ltf/g)
    ln -s ${path}/${file} ${outdir}/IL9/setE/ENG_from_IL/ltf/${newfile}
done
for file in /home/data/LoReHLT18/ldc_packs/il9/setE/data/monolingual_text/il9/ltf/*; do
    ln -s ${file} ${outdir}/IL9/setE/IL/ltf/.
done

### IL10
il10_ann_langpath="/usr3/data/shared/LoReHLT18/scripts/LoReHLT18_SF_Scorer_v1.0.4_07092018/datasets/IL10_Reference"
ln -s ${il10_ann_langpath}/setE/data/annotation/il10/il10_edl.tab \
    ${outdir}/IL10/setE/gold_edl.tab
# This is the annotated_filelist_SF.tab obtained by looking at the annotations. This unfortunately results in not
# knowing which files the annotators looked at but not give any judgment.
il10_ann2_langpath="/usr0/home/data/LoReHLT17/scripts/LoReHLT_Frame_Scorer_v2.1.0-prerelease_20170920"
ln -s ${il10_ann2_langpath}/references/sin/il10/annotated_filelist_SF.tab \
    ${outdir}/IL10/setE/IL/sf_anno/annotated_filelist_SF.tab
for subdir in "issues" "needs" "mentions"; do
    mkdir -p ${outdir}/IL10/setE/IL/sf_anno/${subdir}
    path=${il10_ann_langpath}/setE/data/annotation/il10/situation_frame/${subdir}
    for file in ${path}/*; do
        ln -s ${file} ${outdir}/IL10/setE/IL/sf_anno/${subdir}/.
    done
done
path="${il10_ann_langpath}/setE/data/translation/eng/ltf"
for file in $(ls ${path}); do
    newfile=$(echo ${file} | sed s/eng.ltf/eng_A.ltf/g)
    ln -s ${path}/${file} ${outdir}/IL10/setE/ENG_from_IL/ltf/${newfile}
done
for file in /home/data/LoReHLT18/ldc_packs/il10/setE/data/monolingual_text/il10/ltf/*; do
    ln -s ${file} ${outdir}/IL10/setE/IL/ltf/.
done

echo "Processing other languages..."
python3 prepare_data_aux.py \
    --outdir ${outdir} \
    --update_cache
echo "Done processing other languages."

echo "Converting SF annotations into JSON..."
for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL" "IL5" "IL6" "IL9" "IL10"; do
    python3 ${script_dir}/sf_to_json.py \
        --ltf_dir ${outdir}/${lang}/setE/IL/ltf \
        --sf_dir ${outdir}/${lang}/setE/IL/sf_anno \
        --json_out ${outdir}/${lang}/setE/IL/sf_anno/gold_sf_IL.json
done
echo "Done converting SF annotations into JSON."

echo "Converting gold SF annotations into NI simulation"
for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL" "IL5" "IL6" "IL9" "IL10"; do
    python3 ${script_dir}/gold_to_ni.py \
        --json_in ${outdir}/${lang}/setE/IL/sf_anno/gold_sf_IL.json \
        --json_out ${outdir}/${lang}/setE/IL/sf_anno/gold_sf_IL_as_NI.json \
        --limit 150
done
echo "Done converting gold SF annotations into NI simulation"

echo "Linking English Core Data..."
for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL"; do
    lang_small=$(echo ${lang} | tr A-Z a-z)
    for file in /home/data/English_Core_Data_translations/${lang_small}/*; do
        ln -s ${file} ${outdir}/${lang}/core_data/IL/ltf/.
    done
done
if [ -z "${script_dir+x}" ]; then
    script_dir=$(dirname $(readlink -f $0))
fi
outdir="langpacks"
for file in /home/data/English_Core_Data/*; do
    ln -s ${file} ${outdir}/ENG/core_data/IL/ltf/.
done
core_data_sf_anno="/usr0/home/data/English_SF_Annotation/2017_English_Core_Data/sf_anno_LoReHLT17_3/"
for subdir in "issues" "needs" "mentions"; do
    mkdir -p ${outdir}/ENG/core_data/IL/sf_anno/${subdir}
    path=${core_data_sf_anno}/${subdir}
    for file in ${path}/*; do
        ln -s ${file} ${outdir}/ENG/core_data/IL/sf_anno/${subdir}/.
    done
done
python3 ${script_dir}/sf_to_json.py \
    --ltf_dir ${outdir}/ENG/core_data/IL/ltf \
    --sf_dir ${outdir}/ENG/core_data/IL/sf_anno \
    --json_out ${outdir}/ENG/core_data/IL/sf_anno/gold_sf_IL.json
echo "Done linking English Core Data."
