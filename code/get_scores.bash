# 11 Apr 2019
# By: Aldrian Obaja Muis
# To score SFs on English Core Data using Zaid's scripts

use_ldc_and_uw="false"
#for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
#            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL" "ENG"; do
#    lang_code=$(echo ${lang} | tr [A-Z] [a-z])
#    #lang_filelist="/home/data/English_Core_Data_translations/${lang_code}.list"
#    lang_filelist="/usr2/home/amuis/work/lorelei_summary/data/core_data_cleanup/engcore_intersection_20190508.list"
#    if [ "${use_ldc_and_uw}" == "true" ]; then
#        dict_identifier="_ldc_clean+uw_2017"
#    else
#        dict_identifier=""
#    fi
#    python3 ~/work/real_run2018/scripts/strip_fields.py \
#        --sf_file "sf/${lang}_kw${dict_identifier}.json" \
#        --out_file "sf/${lang}_kw${dict_identifier}_v2017.json" \
#        --version "v2017"
#    json_path="sf/${lang}_kw${dict_identifier}_v2017.json"
#    file_name=${json_path##*/}
#    score_path="scores${dict_identifier}_intersection_20190508/${file_name/json/scores}"
#    echo "Scoring ${json_path} into ${score_path}"
#    bash /home/data/LoReHLT17/scripts/LoReHLT_Frame_Scorer_v2.1.0-prerelease_20170920/score_engcore_translations_3.sh \
#        ${lang_filelist} \
#        "${json_path}" \
#        "${score_path}"
#done

# NN classifier
for setting in "set12_core"; do
    for with_ni in "0"; do
        if [ "${with_ni}" == "0" ]; then
            with_ni_label="_no_ni"
        else
            with_ni_label="_with_ni"
        fi
        score_dir="scores_nn_${setting}${with_ni_label}_intersection_20190508"
        mkdir -p ${score_dir}
        #for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
        #            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL" "ENG"; do
        for lang in "UKR" "URD"; do
            lang_code=$(echo ${lang} | tr [A-Z] [a-z])
            #lang_filelist="/home/data/English_Core_Data_translations/${lang_code}.list"
            lang_filelist="/usr2/home/amuis/work/lorelei_summary/data/core_data_cleanup/engcore_intersection_20190508.list"
            for run_id in "1" "2" "3"; do
                orig_path="sf_nn/core_data/${lang}/${setting}/bwe_XlingualEmb_dom_simple_tgt_train_${with_ni}.run_${run_id}/out.thres.-1.5.json"
                clean_path="sf_nn/core_data/${lang}/${setting}/bwe_XlingualEmb_dom_simple_tgt_train_${with_ni}.run_${run_id}/out.thres.-1.5.clean.json"
                python3 ~/work/real_run2018/scripts/strip_fields.py \
                    --sf_file "${orig_path}" \
                    --out_file "${clean_path}" \
                    --version "v2017" \
                    --clean_doc_id
                file_name="${setting}_nn_run_${run_id}_${clean_path##*/}"
                score_path="${score_dir}/${lang}_${file_name/json/scores}"
                echo "Scoring ${clean_path} into ${score_path}"
                bash /home/data/LoReHLT17/scripts/LoReHLT_Frame_Scorer_v2.1.0-prerelease_20170920/score_engcore_translations_3.sh \
                    ${lang_filelist} \
                    "${clean_path}" \
                    "${score_path}"
            done
        done
    done
done
