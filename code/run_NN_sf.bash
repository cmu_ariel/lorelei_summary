CUDA_ROOT=/usr0/home/ruochenx/local/cuda-7.5/;
CPATH="/usr0/home/ruochenx/local/cuda-7.5/:$CPATH"
export CPATH
export CUDA_ROOT;
export PATH=/usr0/home/ruochenx/local/cuda-7.5/bin:$PATH;
export LD_LIBRARY_PATH=/usr0/home/ruochenx/local/cuda-7.5/lib64:/usr0/home/ruochenx/local/cuda-7.5/extras/CUPTI/lib64:$LD_LIBRARY_PATH;
export KERAS_BACKEND=theano;

# use "SF_Classify" conda env
SCRIPT_ROOT=$(pwd);
SF_ROOT=${SCRIPT_ROOT}/SF-classifier;
DATA_ROOT="/usr2/home/amuis/work/lorelei_summary/data/langpacks"
OUTPUT_ROOT="/usr2/home/amuis/work/lorelei_summary/code/sf_nn"
MODEL=bwe
# CHKPT=CP2_relabel_partial_fit;

approx_mem=3000;
ps_count=0;

#LANGS=( "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
#        "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL" )
LANGS=("UKR" "URD")
#LANGS+=("ENG")

mkdir -p "${OUTPUT_ROOT}"

for setting in "set12_core";
do
    for SPLIT in "core_data"; # "setE"
    do
        for run_id in "1" "2" "3"; # for multiple runs, but initial experiments show not much difference
        do
            for tgt_train in 1
            do
                for lang in ${LANGS[@]};
                do
                    for run_name in bwe_XlingualEmb
                    do
                        for dom in simple
                        do
                            # CUDA_VISIBLE_DEVICES=$((($ps_count+0)%4));
                            CUDA_VISIBLE_DEVICES=$(python /usr2/data/ruochenx/tools/test_cuda/test_cuda.py ${approx_mem} 2>&1);

                            tgt_path="${DATA_ROOT}/${lang}/${SPLIT}/IL/ltf"
                            save_path="${OUTPUT_ROOT}/${SPLIT}/${lang}/${setting}/${run_name}_dom_${dom}_tgt_train_${tgt_train}.run_${run_id}";
                            mkdir -p $save_path;
                            config_path="$SCRIPT_ROOT/nn_config/${setting}/${lang}_nn_config.json";

                            echo "run_id: ${run_id} run_name: $run_name dom: $dom use_tgt_train: $tgt_train"
                            echo "GPU: $CUDA_VISIBLE_DEVICES lang: $lang SPLIT: $SPLIT";
                            echo "tgt_path: ${tgt_path}";
                            echo "save_path: ${save_path}";
                            echo "config_path: ${config_path}";

                            THEANO_FLAGS=mode=FAST_RUN,device=cuda"$CUDA_VISIBLE_DEVICES",floatX=float32,optimizer=fast_compile \
                                python "$SF_ROOT"/pipeline.py --tgt_test_path "$tgt_path" \
                                                              --model $MODEL \
                                                              --save_path "$save_path" \
                                                              --config_file "${config_path}" \
                                                              --domain_match "$dom" \
                                                              --use_tgt_train "$tgt_train" \
                                                              2>&1 | tee "$save_path"/run_log.txt;

                            ps_count=$((ps_count+1));

                        done
                    done
                done
            done
        done
    done
done
