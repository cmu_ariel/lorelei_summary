CUDA_ROOT=/usr0/home/ruochenx/local/cuda-7.5/;
CPATH="/usr0/home/ruochenx/local/cuda-7.5/:$CPATH"
export CPATH
export CUDA_ROOT;
export PATH=/usr0/home/ruochenx/local/cuda-7.5/bin:$PATH;
export LD_LIBRARY_PATH=/usr0/home/ruochenx/local/cuda-7.5/lib64:/usr0/home/ruochenx/local/cuda-7.5/extras/CUPTI/lib64:$LD_LIBRARY_PATH;
export KERAS_BACKEND=theano;

approx_mem=1000;

# run with the bilingual embedding model
# rm -rf save/il5/bwe_xlingual;
THEANO_FLAGS=mode=FAST_RUN,device=cuda1,floatX=float32,optimizer=fast_compile python pipeline.py --model bwe --save_path save/il5/bwe_xlingual --config_file ./config/il5_config_bwe.json --use_tgt_train 0 --domain_match simple

# run with the cross-lingual distillation model
# rm -rf save/il5/cross_distill;
# THEANO_FLAGS=mode=FAST_RUN,device=cuda0,floatX=float32,optimizer=fast_compile python src/pipeline.py --model distill --save_path save/il5/cross_distill --config_file ./src/config/il5_config_distill.json
