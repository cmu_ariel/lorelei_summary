from os.path import join
import os
import argparse
import errno
import json
import logging
import shutil

import random
import numpy as np
random.seed(42)
np.random.seed(42)

from config import config_helper

# -----------------------------
# Train a SF classifier using:
# 1) crosslingual distillation(https://arxiv.org/pdf/1705.02073) or
# 2) crosslingual word embedding
# -----------------------------

def get_logger(p):
    logger = logging.getLogger(__name__)
    hdlr = logging.FileHandler(p)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    return logger

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def set_config_default(config):
    # some default config
    if 'maxlen' not in config:
        config['maxlen'] = 400
    if 'emb_dim' not in config:
        config['emb_dim'] = 100
    if 'seg' not in config:
        config['seg'] = True
    if 'include_eng' not in config:
        config['include_eng'] = False
    if 'eng_SN_only' not in config:
        config['eng_SN_only'] = False

    return config

def import_config_from_args(config, args):
    # overwrite some config
    if args.save_path is not None:
        config['save_path'] = args.save_path
    if args.tgt_test_path is not None:
        config['tgt_test_path'] = args.tgt_test_path
    if args.domain_match is not None:
        config['domain_match'] = args.domain_match
    if args.attention:
        config['pool'] = 'attn'
    else:
        config['pool'] = 'max'
    if args.cnn_config_files is not None:
        config_files = args.cnn_config_files
        if len(config_files) == 1:
            config_files.append(args.cnn_config_files[0])
        config['hyper_params'] = config_files

    if args.bpe:
        config["bpe_model"] = args.bpe_model

    return config

def main():
    parser = argparse.ArgumentParser(description='Train SF classifier and test on test data')
    parser.add_argument('--tgt_test_path', help='path to read the test data', default=None)
    parser.add_argument('--save_path', help='path to save the logger and model')
    parser.add_argument('--use_tgt_train', help='whether to use target training data', default=0, type=int)
    parser.add_argument('--domain_match', help='domain adaptation option', default='simple')
    parser.add_argument('--attention', help="use attention to replace pooling", action="store_true")
    parser.add_argument('--config_file', help='path to the configuration JSON file', type=str, required=True)
    parser.add_argument('--cnn_config_files', help='path to the CNN configuration JSON file', nargs='*')
    parser.add_argument('--model', default='bwe')
    parser.add_argument('--bpe', default=False, action="store_true")
    parser.add_argument('--bpe_model', default='', help='path to the bpe model file')
    args = parser.parse_args()

    # load the configuration file
    config = config_helper.get_train_config(args.config_file)
    config = set_config_default(config)
    config = import_config_from_args(config, args)

    # save the configuration file
    mkdir_p(config['save_path'])
    logger = get_logger(join(config['save_path'], 'logger.txt'))
    # shutil.copyfile(args.config_file, join(config['save_path'], 'config.json'))
    json.dump(config, open(join(config['save_path'], 'config.json'), 'w'), indent=4, ensure_ascii=False)
    for _i, _f in enumerate(config['hyper_params']):
        shutil.copyfile(_f, join(config['save_path'], 'hyper_params.'+str(_i)+'.json'))

    # initialize a model object
    if args.model == 'distill':
        from model.CLDistill import CLD # Using cross-lingual distillation(need parallel data)
        model = CLD(config, logger)
    elif args.model == 'bwe':
        from model.CMD_BWE import CMD_BWE # Using bilingual word embedding (BWE)
        model = CMD_BWE(config, logger)
    elif args.model == 'bwe-swem':  # Using a SWEM with bilingual word embedding
        from model.SWEM import SWEM_BWE
        model = SWEM_BWE(config, logger)
    elif args.model == 'bwe-grl': # Using a gradient reversal layer for domain matching
        from model.GRL_BWE import GRL_BWE
        model = GRL_BWE(config, logger)
    elif args.model == 'simple':
        from model.SimpleTrain import Simple_WE # Only train and test in a single language (English)
        model = Simple_WE(config, logger)
    else:
        raise NotImplementedError

    for name in config['eng_train_path']: # iterate different classification tasks
        # read src training data
        model.read_src(config['eng_train_path'][name])
        # read tgt testing data
        model.read_tgt()
        # read tgt annotation if exists
        if 'tgt_train_path' in config and name in config['tgt_train_path'] and args.use_tgt_train:
            model.read_tgt_json(train_paths=config['tgt_train_path'][name])
        if 'parl_path' in config:
            model.read_parl()
        # let's start the cross-lingual training
        model.train(name)

    # let's make SF predictions on test data
    model.write_json(thres=config['thres'])

if __name__ == '__main__':
    main()
