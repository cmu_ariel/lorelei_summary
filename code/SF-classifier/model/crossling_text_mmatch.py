# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 2016

@author: zellinger
"""

import numpy as np
import datetime
import sys

from keras import backend as K
from keras.constraints import maxnorm
from keras.layers import Conv1D, MaxPooling1D, AveragePooling1D
from keras.layers import GlobalAveragePooling1D
from keras.layers import GlobalMaxPooling1D
# from keras.layers import Dense, Dropout, Activation, Flatten, Reshape, Concatenate
from keras.layers import Dense, Dropout, Activation, Flatten, Reshape
from keras.layers import merge
from keras.layers import Embedding, Lambda
from keras.layers import Input, TimeDistributed
from keras.layers.merge import concatenate

from keras.models import Model
from keras.models import Sequential
from keras.models import load_model
from keras.optimizers import Adam
from keras.optimizers import SGD
from keras.preprocessing import sequence

from model.keras_helper import masked_accuracy
from utils.metric import f1

# root folder of experiments
# model parameters, evaluated accuracies, etc. get dumped here
EXP_FOLDER = 'experiments/amazon_review'
MAX_N_EPOCH = 1500
BATCH_SIZE = 300

def max_1d(X):
    return K.max(X, axis=1)

class kimCNN:
    def __init__(self, params):
        self.max_features = params['max_features']
        self.batch_size = params['batch_size']
        self.nb_filter = params['nb_filter']
        self.filter_length_list = params['filter_length_list']
        self.nb_epoch = params['nb_epoch']
        self.dropout_rate = params['dropout_rate']
        self.use_pretrained_embedding = params['use_pretrained_embedding']
        self.l2_constraint = params['l2_constraint']

        # parameters with defaults
        self.pool = params.get('pool', 'max')
        self.fix_embedding = params.get('fix_embedding', True)
        self.hidden_dim = params.get('hidden_dim', None)
        self.domain_regularizer = params.get('domain_regularizer', False)

        self.label_type = params['label_type']
        self.embedding_dims = params['embedding_dims']
        self.label_dims = params['label_dims']
        self.maxlen = params['maxlen']
        self.lr = params['lr']

        self.feature_extractor = None
        self.top_logistic_regression = None
        self.end2end_model = None
        self.embedding_lookup = None
        self.embedding_weight = None
        # self.n_features = n_features
        # self.nn = None
        # self.encoder = None
        # self.n_epoch = epoch
        # self.batch_size = bsize
        # self.visualize_model = None
        # self.n_hidden = n_hidden
        # self.domain_regularizer = domain_regularizer
        # self.exp_folder = folder
        # self.save_weights = save_weights

    def define_layers(self):
        """Define network architrecture."""
        # embedding layer
        if self.embedding_weight is not None:
            if self.fix_embedding:
                self.embedding_lookup = Embedding(
                    output_dim=self.embedding_dims, input_dim=self.max_features,
                    input_length=self.maxlen, weights=[self.embedding_weight], trainable=False)
            else:
                self.embedding_lookup = Embedding(
                    output_dim=self.embedding_dims, input_dim=self.max_features,
                    input_length=self.maxlen, weights=[self.embedding_weight], trainable=True)
        else:
            if self.fix_embedding:
                print("# WARNING: Using random embedding as fix!")
                # sys.exit(-1)
            self.embedding_lookup = Embedding(
                output_dim=self.embedding_dims, input_dim=self.max_features,
                input_length=self.maxlen, trainable=True)

        # Reshape embedding
        self.reshape_emb_layer = Reshape((self.maxlen, self.embedding_dims), name='reshape(emb)')

        # Convoluional layers
        self.conv_layer_list = []
        self.pool_layer_list = []
        self.flat_layer_list = []
        for i, filter_length in enumerate(self.filter_length_list):
            # conv_layer = Conv1D(nb_filter=self.nb_filter,
            #                     filter_length=filter_length,
            #                     activation='relu',
            #                     W_constraint=maxnorm(self.l2_constraint),
            #                     name='conv({})'.format(filter_length)
            #                     )
            conv_layer = Conv1D(filters=self.nb_filter,
                                kernel_size=filter_length,
                                padding='valid',
                                activation='relu',
                                kernel_constraint=maxnorm(self.l2_constraint),
                                name='conv({})'.format(filter_length)
                                )
            self.conv_layer_list.append(conv_layer)

            if self.pool == 'avg':
                pool_layer = AveragePooling1D(pool_length=self.maxlen+1-filter_length,
                                              name='avg-pool({})'.format(filter_length))
            if self.pool == 'attn':
                from model.attention import AttLayer
                print('Using AttLayer()')
                sys.stderr.write("'Using AttLayer()'\n")
                pool_layer = AttLayer()
            else:
                # pool_layer = MaxPooling1D(pool_length=self.maxlen+1-filter_length,
                #                           name='max-pool({})'.format(filter_length))
                pool_layer = MaxPooling1D(pool_size=self.maxlen+1-filter_length,
                                          name='max-pool({})'.format(filter_length))

            self.pool_layer_list.append(pool_layer)

            if self.pool != 'attn':
                self.flat_layer_list.append(Flatten(name='flatten({})'.format(filter_length)))

        # Fully connected layer
        if self.hidden_dim is not None:
            self.dropout_feat_layer = Dropout(self.dropout_rate, name='dropout(feat)')
            if self.domain_regularizer:
                self.fcl = Dense(self.hidden_dim, activation='tanh', name='flc',
                                 activity_regularizer=self.domain_regularizer)
            else:
                self.fcl = Dense(self.hidden_dim, activation='tanh')

        self.dropout_hid_layer = Dropout(self.dropout_rate, name='dropout(hid)')
        self.output_layer = Dense(self.label_dims, name='output')

        if self.label_type == 'multi-class':
            self.output_activation = Activation('softmax', name='softmax')
        elif self.label_type == 'multi-label':
            self.output_activation = Activation('sigmoid', name='sigmoid')
        else:
            print('undefined label type {0}'.format(self.label_type))
            sys.exit()

    def define_computational_graph(self, word_input, tgt=False):
        """Define a computational graph starting from a given word_input and return an output"""
        label = 'tgt' if tgt else 'src'

        # Embedding
        embedding_layer = self.embedding_lookup(word_input)

        # we add a Convolution1D, which will learn nb_filter
        # word group filters of size filter_length, note here
        # we have more than one filter_length:
        reshaped_embedding_layer = self.reshape_emb_layer(embedding_layer)

        # Convolution
        conv_layer_output_list = []
        pool_layer_output_list = []
        flat_layer_output_list = []
        for i, _ in enumerate(self.filter_length_list):
            conv_layer_output_list.append(self.conv_layer_list[i](reshaped_embedding_layer))
            pool_layer_output_list.append(self.pool_layer_list[i](conv_layer_output_list[-1]))

            # We flatten the output of the conv layer,
            # so that we can add a vanilla dense layer:
            if self.pool == 'attn':
                flat_layer_output_list.append(pool_layer_output_list[-1])
            else:
                flat_layer_output_list.append(self.flat_layer_list[i](pool_layer_output_list[-1]))

        # merged_layer_output = merge(flat_layer_output_list, mode='concat',
        #     concat_axis=1,name='Merge({})'.format(label))
        merged_layer_output = concatenate(flat_layer_output_list, axis=1,
            name='Merge({})'.format(label))


        # Fully connected layer
        self.feature_output = merged_layer_output
        # define feature_extractor part from above
        self.feature_extractor = Model(inputs=word_input, outputs=merged_layer_output)
        # self.feature_extractor = Model(inputs=[word_input], outputs=[merged_layer_output])

        if self.hidden_dim is not None:
            merged_layer_output = self.dropout_feat_layer(merged_layer_output)
            layer_output = self.fcl(merged_layer_output)
        else:
            layer_output = merged_layer_output

        hid = self.dropout_hid_layer(layer_output)
        logits = self.output_layer(hid)
        output = Activation('softmax')(logits)

        return output


    def build_model(self, pretrained_embedding=None, fix_embedding=None, domain_regularizer=False):
        # Variables
        if fix_embedding is not None:
            print('Warning: fix_embedding for build_model is deprecated')
        if domain_regularizer:
            self.domain_regularizer = domain_regularizer
        self.embedding_weight = pretrained_embedding

        # Define layers
        self.define_layers()

        # Define computational graph
        input_s = Input(shape=(self.maxlen,), name='source_input')
        input_t = Input(shape=(self.maxlen,), name='target_input')
        pred_s = self.define_computational_graph(input_s, tgt=False)
        pred_t = self.define_computational_graph(input_t, tgt=True)
        self.end2end_model = Model(inputs=[input_s, input_t],
                                   outputs=[pred_s, pred_t])

        if self.label_type == 'multi-class':
            loss_type = 'categorical_crossentropy'
        elif self.label_type == 'multi-label':
            loss_type = 'binary_crossentropy'

        self.end2end_opt = Adam(lr=self.lr)
        self.end2end_model.compile(
            loss=loss_type,
            optimizer=self.end2end_opt,
            metrics=['accuracy', 'categorical_accuracy', f1],
            loss_weights=[1., 0.])

    def setup_partial_loss(self):
        def masked_loss_function(y_true, y_pred):
            mask = K.not_equal(y_true, mask_value)
            return K.binary_crossentropy(y_pred[mask], y_true[mask])
        mask_value = -1
        self.end2end_model.compile(optimizer=self.end2end_opt,
                                   loss=masked_loss_function, loss_weights=[1., 0.],
                                   metrics=[masked_accuracy])  # tgt loss

class charCNN:
    def __init__(self, params):
        self.filter_length_list = params['filter_length_list']
        self.nb_filter_list = params['nb_filter_list']
        self.pool_size = params['pool_size']
        self.dropout_rate = params['dropout_rate']
        self.hidden_dim = params['hidden_dim']
        self.lstm_h = params['lstm_h']

        self.maxlen = params['maxlen']
        self.nb_char = params['max_features']
        self.max_sentences = params['max_sentences']
        self.label_type = params['label_type']
        self.label_dims = params['label_dims']
        self.lr = params['lr']
        self.optimizer = params['optimizer']

        self.feature_extractor = None
        self.end2end_model = None

        if 'domain_regularizer' in params:
            self.domain_regularizer = params['domain_regularizer']
        else:
            self.domain_regularizer = False

    def build_model(self, pretrained_embedding=None, temperature=None, domain_regularizer=False):
        def char_block(in_layer, nb_filter=[64, 100], filter_length=[3, 3], subsample=[2, 1], pool_size=[2, 2], hidden_dim=128, dropout_rate=0.0):
            block = in_layer
            for i in range(len(nb_filter)):

                block = Convolution1D(nb_filter=nb_filter[i],
                                      filter_length=filter_length[i],
                                      border_mode='valid',
                                      activation='relu',
                                      subsample_length=subsample[i])(block)
                # block = BatchNormalization()(block)
                block = Dropout(dropout_rate)(block)
                if pool_size[i]:
                    block = MaxPooling1D(pool_size=pool_size[i])(block)

            block = Lambda(max_1d, output_shape=(nb_filter[-1],))(block)
            block = Dense(hidden_dim, activation='relu')(block)
            return block

        if domain_regularizer:
            self.domain_regularizer = domain_regularizer

        # document = Input(shape=(self.max_sentences, self.maxlen), dtype='int64')
        in_sentence = Input(shape=(self.maxlen, ), dtype='int64')
        char_embedding = np.identity(self.nb_char)
        char_embedding[0,:] = 0 # zero for the padding
        embedded = Embedding(input_dim=self.nb_char, input_length=self.maxlen, output_dim=self.nb_char, weights=[char_embedding], trainable=False)(in_sentence)
        # embedded = Lambda(binarize, output_shape=binarize_outshape)(in_sentence)

        block2 = char_block(embedded, self.nb_filter_list[0], filter_length=self.filter_length_list[0], subsample=[1, 1, 1], pool_size=self.pool_size[0])
        block3 = char_block(embedded, self.nb_filter_list[1], filter_length=self.filter_length_list[1], subsample=[1, 1, 1], pool_size=self.pool_size[1])

        merged = merge([block2, block3], mode='concat', concat_axis=-1)
        # sent_encode = Dropout(self.dropout_rate)(sent_encode)

        # encoder = Model(input=in_sentence, output=sent_encode)
        # encoded = TimeDistributed(encoder)(document)

        # forwards = LSTM(params['lstm_h'], return_sequences=False, dropout_W=self.dropout_rate, dropout_U=self.dropout_rate,
        #                 consume_less='gpu')(encoded)
        # backwards = LSTM(params['lstm_h'], return_sequences=False, dropout_W=self.dropout_rate, dropout_U=self.dropout_rate,
        #                  consume_less='gpu', go_backwards=True)(encoded)

        # merged = merge([forwards, backwards], mode='concat', concat_axis=-1)
        self.feature_output = merged
        self.feature_extractor = Model(input=in_sentence, output=merged)

        if self.hidden_dim is not None:
            merged = Dropout(self.dropout_rate)(merged)
            if self.domain_regularizer:
                final_layer = Dense(self.hidden_dim, activation='tanh', activity_regularizer=self.domain_regularizer)
                merged = final_layer(merged)
            else:
                merged = Dense(self.hidden_dim, activation='tanh')(merged)

        # x = Dropout(self.dropout_rate)(merged)
        x = merged

        # We project onto a single unit output layer, and squash it with a sigmoid:
        logits = Dense(self.label_dims)(x)

        if self.label_type == 'multi-class':
            end2end_output = Activation('softmax')(logits)
        elif self.label_type == 'multi-label':
            end2end_output = Activation('sigmoid')(logits)
        else:
            print('undefined label type {0}'.format(self.label_type))
            sys.exit()

        # define the end-to-end model
        # self.end2end_model = Model(input=in_sentence, output=end2end_output)
        end2end_model = Model(input=in_sentence, output=end2end_output)
        input_s = Input(shape=(self.maxlen, ), name='souce_input')
        input_t = Input(shape=(self.maxlen, ), name='target_input')
        pred_s = end2end_model(input_s)
        pred_t = end2end_model(input_t)
        self.end2end_model = Model(input=[input_s,input_t],
                                   output=[pred_s,pred_t])

        if self.label_type == 'multi-class':
            loss_type = 'categorical_crossentropy'
        elif self.label_type == 'multi-label':
            loss_type = 'binary_crossentropy'

        if self.optimizer == 'SGD':
            end2end_opt = SGD(lr=self.lr, decay=1e-6)
        else:
            end2end_opt = Adam(lr=self.lr)

        self.end2end_model.compile(loss=loss_type,
                      optimizer=end2end_opt,
                      metrics=['accuracy', 'categorical_accuracy'],
                      loss_weights=[1., 0.])  # src loss

class SWEM:
    def __init__(self, params):
        self.max_features = params['max_features']
        self.batch_size = params['batch_size']
        self.nb_epoch = params['nb_epoch']
        self.dropout_rate = params['dropout_rate']
        self.use_pretrained_embedding = params['use_pretrained_embedding']

        self.fix_embedding = params.get('fix_embedding', True)
        self.hidden_dim = params.get('hidden_dim', 100)
        self.nb_hidden_layers = params.get('nb_hidden_layers', 2)

        self.label_type = params['label_type']
        self.embedding_dims = params['embedding_dims']
        self.label_dims = params.get('label_dims', 12)
        self.maxlen = params['maxlen']
        self.lr = params['lr']

        self.end2end_model = None
        self.embedding_lookup = None
        self.embedding_weight = None
        # self.n_features = n_features
        # self.nn = None
        # self.encoder = None
        # self.n_epoch = epoch
        # self.batch_size = bsize
        # self.visualize_model = None
        # self.n_hidden = n_hidden
        # self.domain_regularizer = domain_regularizer
        # self.exp_folder = folder
        # self.save_weights = save_weights

    def build_model(self, pretrained_embedding=None, fix_embedding=None):
        if fix_embedding is not None:
            print('Warning: fix_embedding for build_model is deprecated')

        # we start off with an efficient embedding layer which maps
        # our vocab indices into embedding_dims dimensions
        word_input = Input(shape=(self.maxlen, ), dtype='int32', name='word_input')
        self.word_input = word_input

        # Setting up word embedding
        fix_embedding = self.fix_embedding
        self.embedding_weight = pretrained_embedding
        self.embedding_lookup = Embedding(
            output_dim=self.embedding_dims, input_dim=self.max_features,
            input_length=self.maxlen, weights=[self.embedding_weight],
            trainable=(not fix_embedding))

        embedding_layer = self.embedding_lookup(word_input)
        reshaped_embedding_layer = Reshape((self.maxlen, self.embedding_dims))(embedding_layer)
        embedding_layer_dropout = Dropout(self.dropout_rate)(reshaped_embedding_layer)

        pooling_max = GlobalMaxPooling1D()(embedding_layer_dropout)
        pooling_avg = GlobalAveragePooling1D()(embedding_layer_dropout)

        merged_layer_output = Concatenate(axis=1)([pooling_max, pooling_avg])
        merged_layer_output_dropout = Dropout(self.dropout_rate)(merged_layer_output)
        hid_layer_output = [None for _ in range(self.nb_hidden_layers)]
        hid_layer_output_dropout = [None for _ in range(self.nb_hidden_layers)]
        for i in range(self.nb_hidden_layers):
            if i == 0:
                hid_layer_output[i] = Dense(self.hidden_dim, activation='tanh')(
                    merged_layer_output_dropout)
            else:
                hid_layer_output[i] = Dense(self.hidden_dim, activation='tanh')(
                    hid_layer_output[i-1])
            hid_layer_output_dropout[i] = Dropout(self.dropout_rate)(hid_layer_output[i])

        # standard logistic regression part
        # We project onto a single unit output layer, and squash it with a sigmoid:
        logits = Dense(self.label_dims)(hid_layer_output_dropout[-1])

        if self.label_type == 'multi-class':
            end2end_output = Activation('softmax')(logits)
        elif self.label_type == 'multi-label':
            end2end_output = Activation('sigmoid')(logits)
        else:
            print('undefined label type {0}'.format(self.label_type))
            sys.exit()

        # define the end-to-end model
        end2end_model = Model(inputs=[word_input], outputs=[end2end_output])
        input_s = Input(shape=(self.maxlen, ), name='souce_input')
        input_t = Input(shape=(self.maxlen, ), name='target_input')
        pred_s = end2end_model(input_s)
        pred_t = end2end_model(input_t)
        self.end2end_model = Model(inputs=[input_s,input_t],
                                   outputs=[pred_s,pred_t])

        if self.label_type == 'multi-class':
            loss_type = 'categorical_crossentropy'
        elif self.label_type == 'multi-label':
            loss_type = 'binary_crossentropy'

        self.end2end_opt = Adam(lr=self.lr)
        self.end2end_model.compile(loss=loss_type,
                                   optimizer=self.end2end_opt,
                                   metrics=['accuracy', 'categorical_accuracy'],
                                   loss_weights=[1.,0.])

    def setup_partial_loss(self):
        def masked_loss_function(y_true, y_pred):
            mask = K.cast(K.not_equal(y_true, mask_value), K.floatx())
            return K.categorical_crossentropy(y_true * mask, y_pred * mask) \
            * y_true.shape[0] * y_true.shape[1] / mask.sum()
        mask_value = -1
        self.end2end_opt = Adam(lr=self.lr)  # reset an optimizer
        self.end2end_model.compile(loss=masked_loss_function,
                                   optimizer=self.end2end_opt,
                                   metrics=['accuracy', 'categorical_accuracy'],
                                   loss_weights=[1., 0.])  # tgt loss
