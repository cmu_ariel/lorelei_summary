import math
import numpy as np

from keras import backend as K
from keras.metrics import binary_accuracy
from keras.callbacks import EarlyStopping

mask_value = -1

def partial_label_fit(model, Xs, ys, catgy_ind, batch_size, nb_epoch, X_tst=None):
    X, y = [], []
    sample_weight = []
    for cat, ind in sorted(catgy_ind.items(), key=lambda t: t[1]):
        if Xs[cat].shape[0] == 0:  # No instance
            continue
        X.append(Xs[cat])
        y_ = np.empty((len(ys[cat]), len(catgy_ind)))
        y_[:] = mask_value
        y_[:, ind] = ys[cat]
        y.append(y_)
        n = len(ys[cat])
        n_positives = float(ys[cat].sum())
        weight_positive = (n - n_positives + 0.1) / n
        sample_weight += [weight_positive if l == 1 else 1. - weight_positive
                          for l in ys[cat]]

        # print("||| cat", cat)
        # print("||| weight_positive", weight_positive)

    X = np.concatenate(X)
    y = np.concatenate(y).astype(int)

    if X_tst is not None:
        # add X_tst for domain matching
        X_tst = np.tile(X_tst, (int(math.ceil(float(X.shape[0])/X_tst.shape[0])), 1))
        X_tst = X_tst[:X.shape[0],:]
    else:
        X_tst = X

    early_stopping = EarlyStopping(monitor='val_loss', patience=3, verbose=0, mode='auto')
    sample_weight = np.array(sample_weight)

    model.fit([X, X_tst], [y, y],
              batch_size=batch_size,
              nb_epoch=nb_epoch,
              validation_split=0.1,
              callbacks=[early_stopping],
              sample_weight=[sample_weight, sample_weight])
    return model


def masked_accuracy(y_true, y_pred):
    mask = K.not_equal(y_true, mask_value)
    return binary_accuracy(y_true=y_true[mask], y_pred=y_pred[mask])
