from keras.optimizers import *
from keras.layers import Input
from keras.models import Model
from keras.layers import Dense, Dropout, Activation, Flatten, Reshape, Merge, TimeDistributed
from keras.layers import Embedding
from keras.layers import Input
from .GradientReversalLayer import GradientReversalLayer
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from keras.models import load_model
import random
from tqdm import tqdm
from keras.models import model_from_json
import datetime
import time
import os
import sys

def restore_weights(src_model, tgt_model, verbose=0):
    src_ws = src_model.get_weights()
    tgt_ws = tgt_model.get_weights()
    if verbose > 0:
      print('source model size:')
      for w in src_ws:
        print(w.shape)
        if w.shape[0] < 10 and len(w.shape) == 1:
          print(w)
        print('------')

    tgt_model.set_weights(src_ws[:len(tgt_ws)])
    tgt_ws = tgt_model.get_weights()
    if verbose > 0:
      print('target model size:')
      for w in tgt_ws:
        print(w.shape)
        if w.shape[0] < 10 and len(w.shape) == 1:
          print(w)
        print('------')
    return 0

def get_time_stamp():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')

# def train_for_adversarial_alternate(model, language_discriminator, src_embedding, tgt_embedding, X_src_train, y_src_train, X_tgt_test, nb_epoch=5000, batch_size=32, lr=1e-5, k=1):
#     # use the alternating way of training adversarial network

#     opt = Adam(lr=lr)

#     lang_discriminator_input = Input(shape=(model.maxlen, ), dtype='float32')
#     x = model.feature_extractor(lang_discriminator_input)
#     x = language_discriminator(x)
#     lang_discriminator = Model(input=lang_discriminator_input, output=x)
#     lang_discriminator.compile(loss='categorical_crossentropy',
#                       optimizer=opt,
#                       metrics=['accuracy'])

#     for e in tqdm(range(nb_epoch)):

#         src_batch_idx = np.random.choice(X_src_train.shape[0],size=batch_size, replace=False)
#         X_src_train_batch = X_src_train[src_batch_idx,:]
#         y_src_train_batch = y_src_train[src_batch_idx]

#         # train source language topic classifier
#         # changeWordEmbedding(model, src_embedding)
#         end2end_loss = model.end2end_model.train_on_batch(X_src_train_batch, y_src_train_batch)

#         # train language discriminator
#         tgt_batch_idx = np.random.choice(X_tgt_test.shape[0],size=batch_size, replace=False)
#         X_tgt_test_batch = X_tgt_test[tgt_batch_idx,:]

#         # update discriminator
#         make_trainable(language_discriminator,True)

#         for _ in range(k):
#             X_src_feature = model.feature_extractor.predict(X_src_train_batch)
#             y = np.zeros([batch_size,1])
#             language_discriminator.train_on_batch(X_src_feature, y)

#             # changeWordEmbedding(model, tgt_embedding)
#             X_tgt_feature = model.feature_extractor.predict(X_tgt_test_batch)
#             y = np.ones([batch_size,1])
#             language_discriminator.train_on_batch(X_tgt_feature, y)

#         # update feature extractor, freeze language_discriminator
#         make_trainable(language_discriminator,False)
#         y = np.zeros([batch_size,1])
#         lang_discriminator.train_on_batch(X_tgt_test_batch, y)

#         # changeWordEmbedding(model, src_embedding)
#         y = np.ones([batch_size,1])
#         lang_discriminator.train_on_batch(X_src_train_batch, y)

def get_discriminator(input_dim, hidden_dim=100, depth=2, dropout_rate=0.2):
    feature_input = Input(shape=(input_dim, ), dtype='float32')
    x = Dense(hidden_dim)(feature_input)
    x = Activation('relu')(x)
    for _ in range(depth-1):
        x = Dense(hidden_dim)(x)
        x = Activation('relu')(x)
    x = Dropout(dropout_rate)(x)
    x = Dense(2)(x)
    x = Activation('softmax')(x)
    discriminator = Model(input=feature_input, output=x)

    return discriminator

def make_trainable(net, val):
    net.trainable = val
    for l in net.layers:
        l.trainable = val

def changeWordEmbedding(model, pretrained_embedding): ## change word embedding to target language
    weights = model.feature_extractor.get_weights()
    weights[0] = pretrained_embedding
    model.feature_extractor.set_weights(weights)
    # return model

def plot_loss(losses, fig_file):
#        display.clear_output(wait=True)
#        display.display(plt.gcf())
    plt.figure(figsize=(10,8))
    plt.plot(losses["topic"], label='topic classification loss')
    if "lang" in losses:
        plt.plot(losses["lang"], label='lang classification loss')
    if "word" in losses:
        plt.plot(losses["word"], label='word classification loss')
    plt.legend()
    plt.savefig(fig_file)

def get_mini_batch_idx(size, mini_batch_size):
    idx = np.arange(size)
    random.shuffle(idx)
    mini_batches = [
        idx[k:min(k+mini_batch_size,size)]
        for k in range(0, size, mini_batch_size)
        ]
    return mini_batches

def dict_neg_sample(d):
    neg = np.zeros([d.shape[0],2], dtype=np.int32)
    neg[:,0:0] = d[:,1]
    neg[:,1:1] = d[:,0]
    return neg

def train_for_adversarial_GRL(model, language_discriminator, X_src_train, y_src_train, X_tgt_val, y_tgt_val, X_tgt_unlabeled, X_tgt_test=None, y_tgt_test=None, nb_epoch=5000, batch_size=32, lr=1e-5, k=1, l=1, hp_lambda=1e-7, plt_frq=25, mini_batch=True, change_word_embedding=False, src_embedding=None, tgt_embedding=None, plot=['BWE_src_loss.png', 'BWE_tgt_loss.png'], use_soft_model=False, val_patience_count = 1, val_metric='loss'):
    # use the Gradient Reversal Layer of training adversarial network
    # follow paper "Adversarial Deep Averaging Networks for Cross-Lingual Sentiment Classification"

    if use_soft_model:
        end2end_model = model.end2end_soft_model
        end2end_model_opt = model.end2end_soft_opt
    else:
        end2end_model = model.end2end_model
        end2end_model_opt = model.end2end_opt

    # set up loss storage vector
    src_losses = {"topic":[], "lang":[]}
    tgt_losses = {"topic":[]}
    val_no_imprv_count = 0
    minium_count = 0
    lang_discriminator_opt = Adam(lr=lr)

    time_stamp = get_time_stamp()
    weights_file_name = '/tmp/tmp_model.' + time_stamp + str(os.getpid()) + '.h5'
    json_file_name = '/tmp/tmp_model.' + time_stamp + str(os.getpid()) + '.json'

    lang_discriminator_input = Input(shape=(model.maxlen, ), dtype='float32')
    x = model.feature_extractor(lang_discriminator_input)
    x = GradientReversalLayer(hp_lambda=hp_lambda)(x)
    x = language_discriminator(x)
    lang_discriminator = Model(input=lang_discriminator_input, output=x)
    lang_discriminator.compile(loss='categorical_crossentropy',
                      optimizer=lang_discriminator_opt,
                      metrics=['categorical_accuracy'])
    
    val_loss = float("inf")
    val_acc = 0.0
    prev_obj = None

    for e in range(nb_epoch):
        print(('Iteration: ' + str(e)))
        cur_obj = 0.0
        if mini_batch:
            mini_batches = get_mini_batch_idx(X_src_train.shape[0], batch_size)
            for batch_idx, src_batch_idx in enumerate(tqdm(mini_batches)): 
                # src_batch_idx = np.random.choice(X_src_train.shape[0], size=batch_size, replace=False)
                # print(src_batch_idx.shape)
                X_src_train_batch = X_src_train[src_batch_idx,:]
                y_src_train_batch = y_src_train[src_batch_idx]

                # train source language topic classifier
                if change_word_embedding:
                    changeWordEmbedding(model, src_embedding)
                for _ in range(l):
                    end2end_topic_loss = end2end_model.train_on_batch(X_src_train_batch, y_src_train_batch)

                # end2end_topic_loss = end2end_model.evaluate(X_src_train_batch, y_src_train_batch, batch_size=batch_size)
                # print("topic loss BEFORE adversarial: " + str(end2end_topic_loss[0]))

                src_losses['topic'].append(end2end_topic_loss[0])

                # train language discriminator
                tgt_batch_idx = np.random.choice(X_tgt_unlabeled.shape[0], size=X_src_train_batch.shape[0], replace=False)
                X_tgt_unlabeled_batch = X_tgt_unlabeled[tgt_batch_idx,:]
                end2end_lang_loss = 0.0
                if change_word_embedding:
                    # update discriminator and feature extractor at same time
                    for inner in range(k):
                        if inner < k-1:
                            make_trainable(model.feature_extractor, False)
                        else:
                            make_trainable(model.feature_extractor, True)
                        y = np.zeros([X_src_train_batch.shape[0],2])
                        y[:,1] = 1
                        changeWordEmbedding(model, src_embedding)
                        loss = lang_discriminator.train_on_batch(X_src_train_batch, y)
                        if inner >= k-1:
                            end2end_lang_loss += loss[0]

                        y = np.zeros([X_src_train_batch.shape[0],2])
                        y[:,0] = 1
                        changeWordEmbedding(model, tgt_embedding)
                        loss = lang_discriminator.train_on_batch(X_tgt_unlabeled_batch, y)
                        if inner >= k-1:
                            end2end_lang_loss += loss[0]
                    src_losses['lang'].append(end2end_lang_loss/float(2))
                else:
                    
                    # update discriminator and feature extractor at same time
                    y = np.zeros([2*X_src_train_batch.shape[0],2])
                    y[:X_src_train_batch.shape[0],1] = 1
                    y[X_src_train_batch.shape[0]:,0] = 1
                    X_comb = np.concatenate((X_src_train_batch, X_tgt_unlabeled_batch))
                    for inner in range(k):
                        if inner < k-1:
                            make_trainable(model.feature_extractor, False)
                        else:
                            make_trainable(model.feature_extractor, True)
                        # update feature extractor, freeze language_discriminator
                        loss = lang_discriminator.train_on_batch(X_comb, y)
                        if inner >= k-1:
                            end2end_lang_loss += loss[0]

                    # print("lang loss: " + str(end2end_lang_loss/float(k)))
                    src_losses['lang'].append(end2end_lang_loss)

                # end2end_topic_loss = end2end_model.evaluate(X_src_train_batch, y_src_train_batch, batch_size=batch_size)
                # print("topic loss AFTER adversarial: " + str(end2end_topic_loss[0]))
                # sys.exit()

                if plot is not None and (batch_idx+1)%plt_frq == 0:
                    plot_loss(src_losses, plot[0])

        else:
            src_batch_idx = np.random.choice(X_src_train.shape[0],size=X_src_train.shape[0], replace=False)
            X_src_train_batch = X_src_train[src_batch_idx,:]
            y_src_train_batch = y_src_train[src_batch_idx,:]
            if change_word_embedding:
                changeWordEmbedding(model, src_embedding)
            end2end_model.fit(X_src_train_batch, y_src_train_batch,
                                    batch_size=batch_size, nb_epoch=l)
            # train language discriminator
            tgt_batch_idx = np.random.choice(X_tgt_unlabeled.shape[0], size=X_src_train.shape[0], replace=False)
            X_tgt_unlabeled_batch = X_tgt_unlabeled[tgt_batch_idx,:]
            for inner in range(k):
                if inner < k-1:
                    make_trainable(model.feature_extractor, False)
                else:
                    make_trainable(model.feature_extractor, True)

                if change_word_embedding:
                    y = np.zeros([X_src_train_batch.shape[0],2])
                    y[:,1] = 1
                    changeWordEmbedding(model, src_embedding)
                    lang_discriminator.fit(X_src_train_batch, y,
                                            batch_size=batch_size, nb_epoch=1)
                    y = np.zeros([X_src_train_batch.shape[0],2])
                    y[:,0] = 1
                    changeWordEmbedding(model, tgt_embedding)
                    lang_discriminator.fit(X_tgt_unlabeled_batch, y,
                                            batch_size=batch_size, nb_epoch=1)

                else:
                    ## src
                    y = np.zeros([2*X_src_train.shape[0],2])
                    y[:X_src_train.shape[0],1] = 1
                    y[X_src_train.shape[0]:,0] = 1
                    
                    X_comb = np.concatenate((X_src_train_batch, X_tgt_unlabeled_batch))
                    lang_discriminator.fit(X_comb, y,
                                            batch_size=batch_size, nb_epoch=1)

        # at the end of each epoch, should fix feature extractor and update task classifier?
        make_trainable(model.feature_extractor, False)
        if change_word_embedding:
            changeWordEmbedding(model, src_embedding)
        end2end_history = end2end_model.fit(X_src_train, y_src_train, batch_size=batch_size, nb_epoch=l)

        # record objective
        hist = end2end_history.history
        cur_obj = hist['loss']

        # validation
        if X_tgt_val is not None and e >= minium_count:
            if change_word_embedding:
                changeWordEmbedding(model, tgt_embedding)
            # validation
            tgt_loss = end2end_model.evaluate(X_tgt_val, y_tgt_val, batch_size=batch_size)
            tgt_losses["topic"].append(tgt_loss[0])
            print(("\n\ntopic val loss: " + str(tgt_loss[0]) + ", accuracy:" + str(tgt_loss[1])))
            
            #probe test
            if X_tgt_test is not None and y_tgt_test is not None:
                tgt_loss = end2end_model.evaluate(X_tgt_test, y_tgt_test, batch_size=batch_size)
                print(("\n\ntopic test loss: " + str(tgt_loss[0]) + ", accuracy:" + str(tgt_loss[1])))

            if plot:
                plot_loss(tgt_losses, plot[1])

            if (val_metric == 'loss' and tgt_loss[0] < val_loss) or (val_metric == 'acc' and tgt_loss[1] > val_acc):
                val_no_imprv_count = 0
                val_loss = min(tgt_loss[0], val_loss)
                val_acc = max(tgt_loss[1], val_acc)
                end2end_model.save_weights(weights_file_name)
                model_json = end2end_model.to_json()
                with open(json_file_name, "w") as json_file:
                    json_file.write(model_json)
            else:
                if end2end_model_opt.lr.get_value() > 1e-8:
                    end2end_model_opt.lr.set_value(np.float32(end2end_model_opt.lr.get_value()*0.8))
                    print(('end2end learning rate changed to ' + str(end2end_model_opt.lr.get_value())))
                # if lang_discriminator_opt.lr.get_value() > 1e-8:
                #     lang_discriminator_opt.lr.set_value(np.float32(lang_discriminator_opt.lr.get_value()*0.8))
                #     print('domain discriminator learning rate changed to ' + str(lang_discriminator_opt.lr.get_value()))
                val_no_imprv_count += 1
                if val_no_imprv_count >= val_patience_count:
                    break # early stop based on validation data
        elif e >= minium_count:
            if prev_obj is None:
                prev_obj = cur_obj
            else:
                if abs(prev_obj-cur_obj)/abs(prev_obj) < 0.001:
                    break
                else:
                    prev_obj = cur_obj
            
        plt.close('all')
    # get the best model
    # end2end_model = load_model('model/tmp_model.h5')
    if X_tgt_val is not None:
        # load json and create model
        json_file = open(json_file_name, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        end2end_model = model_from_json(loaded_model_json)
        # load weights into new model
        end2end_model.load_weights(weights_file_name)
        os.remove(weights_file_name)
        os.remove(json_file_name)

    # if use_soft_model:
    restore_weights(end2end_model, model.end2end_soft_model)
    restore_weights(end2end_model, model.feature_extractor)
    restore_weights(end2end_model, model.end2end_model)

def joint_train_for_adversarial_GRL(model, language_discriminator, X_src_train, y_src_train, X_tgt_val, y_tgt_val, X_tgt_unlabeled, bi_dict, nb_epoch=50, batch_size=32, lr=1e-5, k=1, l=1, hp_lambda=1e-7, plt_frq=25, plot=['BWE_src_loss.png', 'BWE_tgt_loss.png']):

    def get_lang_discriminator(model, feature_extractor, language_discriminator, hp_lambda):
        # define language discriminator
        lang_discriminator_input = Input(shape=(model.maxlen, ), dtype='int32')
        x = feature_extractor(lang_discriminator_input)
        x = GradientReversalLayer(hp_lambda=hp_lambda)(x)
        x = language_discriminator(x)
        discriminator = Model(input=lang_discriminator_input, output=x)
        discriminator.compile(loss='categorical_crossentropy',
                          optimizer=opt,
                          metrics=['categorical_accuracy'])
        return discriminator

    # set up loss storage vector
    # src_losses = {"topic":[], "lang":[]}
    src_losses = {"topic":[], "lang":[], "word":[]}
    tgt_losses = {"topic":[]}
    val_no_imprv_count = 0
    val_patience_count = 5
    opt = Adam(lr=lr)

    # define language discriminator
    src_lang_discriminator = get_lang_discriminator(model, model.trans_feature_extractor, language_discriminator, hp_lambda)
    tgt_lang_discriminator = get_lang_discriminator(model, model.feature_extractor, language_discriminator, hp_lambda)

    # define word discriminator
    # word_discriminator_input = Input(shape=(2, ), dtype='int32')
    # embedding_layer = Embedding(output_dim=model.embedding_dims, input_dim=model.max_features, input_length=2, weights=[model.embedding_weight], trainable=False)(word_discriminator_input)
    # reshaped_embedding_layer = Reshape((2, model.embedding_dims))(embedding_layer)
    # reshaped_embedding_layer = TimeDistributed(model.embedding_transform)(reshaped_embedding_layer)
    # x = Flatten()(reshaped_embedding_layer)
    # x = GradientReversalLayer(hp_lambda=hp_lambda)(x)
    # x = word_discriminator(x)
    # word_discriminator = Model(input=word_discriminator_input, output=x)
    # word_discriminator.compile(loss='categorical_crossentropy',
    #               optimizer=opt,
    #               metrics=['categorical_accuracy'])
    src_word_input = Input(shape=(1, ), dtype='int32')
    embedding_layer_output = Embedding(output_dim=model.embedding_dims, input_dim=model.max_features, input_length=2, weights=[model.embedding_weight], trainable=False)(src_word_input)
    flat_layer_output = Flatten()(embedding_layer_output)
    transfrom_embedding = model.embedding_transform(flat_layer_output)
    word_discriminator = Model(input=src_word_input, output=transfrom_embedding)
    word_discriminator.compile(loss='mean_squared_error',
                  optimizer=opt,
                  metrics=['mean_squared_error'])

    # save best model on validation data(if we have validation data)
    val_loss = float("inf")
    time_stamp = get_time_stamp()
    weights_file_name = 'model/tmp_model.' + time_stamp + '.h5'
    json_file_name = 'model/tmp_model.' + time_stamp + '.json'

    # let's begin epoch
    for e in range(nb_epoch):
        print(('Iteration: ' + str(e)))
        mini_batches = get_mini_batch_idx(X_src_train.shape[0], batch_size)
        dict_mini_batches = get_mini_batch_idx(bi_dict.shape[0], bi_dict.shape[0]/len(mini_batches))
        for i, src_batch_idx in enumerate(tqdm(mini_batches)): 
            # src_batch_idx = np.random.choice(X_src_train.shape[0], size=batch_size, replace=False)
            # print(src_batch_idx.shape)
            X_src_train_batch = X_src_train[src_batch_idx,:]
            y_src_train_batch = y_src_train[src_batch_idx]

            # train source language topic classifier
            make_trainable(model.feature_extractor, True)
            make_trainable(model.trans_feature_extractor, True)
            make_trainable(model.embedding_transform, False)
            for _ in range(l):
                end2end_topic_loss = model.trans_end2end_model.train_on_batch(X_src_train_batch, y_src_train_batch)
            # print("topic loss: " + str(end2end_topic_loss[0]))
            src_losses['topic'].append(end2end_topic_loss[0])

            # train language discriminator
            tgt_batch_idx = np.random.choice(X_tgt_unlabeled.shape[0], size=X_src_train_batch.shape[0], replace=False)
            X_tgt_unlabeled_batch = X_tgt_unlabeled[tgt_batch_idx,:]
                
            # update discriminator and feature extractor at same time
            y_src = np.zeros([X_src_train_batch.shape[0],2])
            y_tgt = np.zeros([X_src_train_batch.shape[0],2])
            y_src[:,1] = 1
            y_tgt[:,0] = 1
            # X_comb = np.concatenate((X_src_train_batch, X_tgt_unlabeled_batch))
            for inner in range(k):
                if inner < k-1:
                    # update feature extractor, freeze language_discriminator
                    make_trainable(model.feature_extractor, False)
                    make_trainable(model.trans_feature_extractor, False)
                else:
                    # update feature extractor, together with language_discriminator
                    make_trainable(model.feature_extractor, True)
                    make_trainable(model.trans_feature_extractor, True)
                # freeze embedding transform
                make_trainable(model.embedding_transform, False)
                src_loss = src_lang_discriminator.train_on_batch(X_src_train_batch, y_src)
                tgt_loss = tgt_lang_discriminator.train_on_batch(X_tgt_unlabeled_batch, y_tgt)
                if inner >= k-1:
                    end2end_lang_loss = (src_loss[0] + tgt_loss[0])/2

            # print("lang loss: " + str(end2end_lang_loss/float(k)))
            src_losses['lang'].append(end2end_lang_loss)

            # train word discriminator
            # bi_dict_idx = np.random.choice(bi_dict.shape[0], size=X_src_train_batch.shape[0], replace=False)
            # bi_dict_batch = bi_dict[bi_dict_idx,:]
            bi_dict_batch = bi_dict[dict_mini_batches[i],:]

            # update embedding transformation
            # y = np.zeros([dict_mini_batches[i].shape[0]*2,2])
            # y[:dict_mini_batches[i].shape[0],1] = 1
            # y[dict_mini_batches[i].shape[0]:,0] = 1
            # neg_bi_dict_batch = dict_neg_sample(bi_dict_batch)
            # bi_dict_comb = np.concatenate((bi_dict_batch, neg_bi_dict_batch))
            # loss = word_discriminator.train_on_batch(bi_dict_comb, y)
            # end2end_word_loss = loss[0]

            # update embedding transformation
            make_trainable(model.embedding_transform, True)
            loss = word_discriminator.train_on_batch(bi_dict_batch[:,0], np.squeeze(model.embedding_weight[np.squeeze(bi_dict_batch[:,1]),:]))
            end2end_word_loss = loss[0]
            src_losses['word'].append(end2end_word_loss)

        if plot:
            plot_loss(src_losses, plot[0])

        # at the end of each epoch, should fix feature extractor and update task classifier
        make_trainable(model.trans_feature_extractor, False)
        model.trans_end2end_model.fit(X_src_train, y_src_train, batch_size=batch_size, nb_epoch=l)

        # validation
        if X_tgt_val is not None:
            tgt_loss = model.end2end_model.evaluate(X_tgt_val, y_tgt_val, batch_size=batch_size)
            tgt_losses["topic"].append(tgt_loss[0])
            print(("\n\ntopic val loss: " + str(tgt_loss[0]) + ", accuracy:" + str(tgt_loss[1])))
            if plot:
                plot_loss(tgt_losses, plot[1])
            if tgt_loss[0] < val_loss:
                val_no_imprv_count = 0
                val_loss = tgt_loss[0]
                model.end2end_model.save_weights(weights_file_name)
                model_json = model.end2end_model.to_json()
                with open(json_file_name, "w") as json_file:
                    json_file.write(model_json)
            else:
                if model.opt.lr.get_value() > 1e-8:
                    model.opt.lr.set_value(np.float32(model.opt.lr.get_value()*0.5))
                    print(('learning rate changed to ' + str(model.opt.lr.get_value())))
                val_no_imprv_count += 1
                if val_no_imprv_count >= val_patience_count:
                    break # early stop based on validation data
        plt.close('all')
    # get the best model
    # model.end2end_model = load_model('model/tmp_model.h5')
    if X_tgt_val is not None:
        # load json and create model
        json_file = open(json_file_name, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        model.end2end_model = model_from_json(loaded_model_json)
        # load weights into new model
        model.end2end_model.load_weights(weights_file_name)
        os.remove(weights_file_name)
        os.remove(json_file_name)

