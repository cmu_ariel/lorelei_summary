import pandas as pd

from keras.models import Model
from keras.layers import Dense, Activation, Flatten, Input, Dropout, MaxPooling1D, Convolution1D
from keras.layers import LSTM, Lambda, merge
from keras.layers import Embedding, TimeDistributed
from keras.optimizers import SGD, Adam
import numpy as np
# import tensorflow as tf
import re
from keras import backend as K
from keras.engine.topology import Layer
import keras.callbacks
import sys
import os
import argparse

# class Binarize(Layer):
#     def __init__(self, input_dim, **kwargs):
#         self.input_dim = input_dim
#         #self.input_spec = [InputSpec(ndim=3)]
#         super(Binarize, self).__init__(**kwargs)

#     def build(self, input_shape):
#         assert len(input_shape)==3
#         #self.W = self.init((input_shape[-1],1))
#         self.W = self.init((input_shape[-1],))
#         #self.input_spec = [InputSpec(shape=input_shape)]
#         self.trainable_weights = [self.W]
#         super(AttLayer, self).build(input_shape)  # be sure you call this somewhere!

#     def call(self, x, mask=None):
#         eij = K.tanh(K.dot(x, self.W))
        
#         ai = K.exp(eij)
#         weights = ai/K.sum(ai, axis=1).dimshuffle(0,'x')
        
#         weighted_input = x*weights.dimshuffle(0,1,'x')
#         return weighted_input.sum(axis=1)

#     def get_output_shape_for(self, input_shape):
#         return (input_shape[0], input_shape[-1])

# def binarize(x, sz=175):
#     return tf.to_float(tf.one_hot(x, sz, on_value=1, off_value=0, axis=-1))

# def binarize_outshape(in_shape):
#     return (in_shape[0], in_shape[1], 175)

def max_1d(X):
    return K.max(X, axis=1)

# record history of training 
class LossHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.losses = []
        self.accuracies = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))
        self.accuracies.append(logs.get('acc'))

class charCNN:
    def __init__(self, params):
        self.filter_length_list = params['filter_length_list']
        self.nb_filter_list = params['nb_filter_list']
        self.pool_length = params['pool_length']
        self.dropout_rate = params['dropout_rate']
        self.hidden_dim = params['hidden_dim']
        self.lstm_h = params['lstm_h']


        self.maxlen = params['maxlen']
        self.nb_char = params['max_features'] + 1
        self.max_sentences = params['max_sentences']
        self.label_type = params['label_type']
        self.label_dims = params['label_dims']
        self.lr = params['lr']
        self.optimizer = params['optimizer']

        self.feature_extractor = None
        self.end2end_model = None

    def build_model(self, pretrained_embedding=None, temperature=None):
        def char_block(in_layer, nb_filter=[64, 100], filter_length=[3, 3], subsample=[2, 1], pool_length=[2, 2], hidden_dim=128, dropout_rate=0.1):
            block = in_layer
            for i in range(len(nb_filter)):

                block = Convolution1D(nb_filter=nb_filter[i],
                                      filter_length=filter_length[i],
                                      border_mode='valid',
                                      activation='relu',
                                      subsample_length=subsample[i])(block)
                # block = BatchNormalization()(block)
                block = Dropout(dropout_rate)(block)
                if pool_length[i]:
                    block = MaxPooling1D(pool_length=pool_length[i])(block)

            block = Lambda(max_1d, output_shape=(nb_filter[-1],))(block)
            block = Dense(hidden_dim, activation='relu')(block)
            return block

        # document = Input(shape=(self.max_sentences, self.maxlen), dtype='int64')
        in_sentence = Input(shape=(self.maxlen, ), dtype='int64')
        char_embedding = np.identity(self.nb_char)
        char_embedding[0,:] = 0 # zero for the padding
        embedded = Embedding(input_dim=self.nb_char, input_length=self.maxlen, output_dim=self.nb_char, weights=[char_embedding], trainable=False)(in_sentence)
        # embedded = Lambda(binarize, output_shape=binarize_outshape)(in_sentence)

        block2 = char_block(embedded, self.nb_filter_list[0], filter_length=self.filter_length_list[0], subsample=[1, 1, 1], pool_length=self.pool_length[0])
        block3 = char_block(embedded, self.nb_filter_list[1], filter_length=self.filter_length_list[1], subsample=[1, 1, 1], pool_length=self.pool_length[1])

        merged = merge([block2, block3], mode='concat', concat_axis=-1)
        # sent_encode = Dropout(self.dropout_rate)(sent_encode)

        # encoder = Model(input=in_sentence, output=sent_encode)
        # encoded = TimeDistributed(encoder)(document)

        # forwards = LSTM(params['lstm_h'], return_sequences=False, dropout_W=self.dropout_rate, dropout_U=self.dropout_rate,
        #                 consume_less='gpu')(encoded)
        # backwards = LSTM(params['lstm_h'], return_sequences=False, dropout_W=self.dropout_rate, dropout_U=self.dropout_rate,
        #                  consume_less='gpu', go_backwards=True)(encoded)

        # merged = merge([forwards, backwards], mode='concat', concat_axis=-1)
        self.feature_output = merged
        self.feature_extractor = Model(input=in_sentence, output=merged)

        if self.hidden_dim is not None:
            merged = Dropout(self.dropout_rate)(merged)
            merged = Dense(self.hidden_dim, activation='tanh')(merged)


        x = Dropout(self.dropout_rate)(merged)

        # We project onto a single unit output layer, and squash it with a sigmoid:
        logits = Dense(self.label_dims)(x)

        if self.label_type == 'multi-class':
            end2end_output = Activation('softmax')(logits)
        elif self.label_type == 'multi-label':
            end2end_output = Activation('sigmoid')(logits)
        else:
            print('undefined label type {0}'.format(self.label_type))
            sys.exit()

        # define the end-to-end model
        self.end2end_model = Model(input=in_sentence, output=end2end_output)

        if temperature is not None:
            hot_logits = Lambda(lambda x: x / temperature)(logits)
            if self.label_type == 'multi-class':
                end2end_soft_output = Activation('softmax', name='soft_output')(hot_logits)
            elif self.label_type == 'multi-label':
                end2end_soft_output = Activation('sigmoid', name='soft_output')(hot_logits)
            else:
                print('undefined label type {0}'.format(self.label_type))
                sys.exit()
            # self.top_soft_logistic_regression = Model(input=feature_input, output=soft_output)
            self.soft_output = end2end_soft_output
            self.end2end_soft_model = Model(input=in_sentence, output=end2end_soft_output)

        if self.label_type == 'multi-class':
            loss_type = 'categorical_crossentropy'
        elif self.label_type == 'multi-label':
            loss_type = 'binary_crossentropy'

        if self.optimizer == 'SGD':
            self.end2end_opt = SGD(lr=self.lr, decay=1e-6)
        else:
            self.end2end_opt = Adam(lr=self.lr)
            
        self.end2end_model.compile(loss=loss_type,
                      optimizer=self.end2end_opt,
                      metrics=['accuracy', 'categorical_accuracy', 'fmeasure'])
        if temperature is not None:
            self.end2end_soft_opt = Adam(lr=self.lr)
            self.end2end_soft_model.compile(loss=loss_type,
                          optimizer=self.end2end_soft_opt,
                          metrics=['accuracy', 'categorical_accuracy', 'fmeasure'])


def main():

    p = re.compile(r'<.*?>')
    parser = argparse.ArgumentParser(description='Char-level CNN-RNN model')
    parser.add_argument('-checkpoint', required=False, help='load checkpoint from previous training', default=None)
    parser.add_argument('-lr', required=False, type=int, help='learning rate of the model', default=0.01)
    parser.add_argument('-optimizer', required=False, help='optimizer to use for BP', default='Adam')
    args = parser.parse_args()
    checkpoint = args.checkpoint

    # load data
    maxlen = 512
    max_sentences = 15
    data = pd.read_csv("labeledTrainData.tsv", header=0, delimiter="\t", quoting=3)
    txt = ''
    docs = []
    sentences = []
    sentiments = []

    for cont, sentiment in zip(data.review, data.sentiment):
        sentences = re.split(r'(?<!\w\.\w.)(?<![A-Z][a-z]\.)(?<=\.|\?)\s', clean(striphtml(cont)))
        sentences = [sent.lower() for sent in sentences]
        docs.append(sentences)
        sentiments.append(sentiment)

    num_sent = []
    for doc in docs:
        num_sent.append(len(doc))
        for s in doc:
            txt += s

    chars = set(txt)

    print('total chars:', len(chars))
    char_indices = dict((c, i) for i, c in enumerate(chars))
    indices_char = dict((i, c) for i, c in enumerate(chars))

    print('Sample doc{}'.format(docs[1200]))

    maxlen = 512
    max_sentences = 15

    X = np.ones((len(docs), max_sentences, maxlen), dtype=np.int64) * -1
    y = np.array(sentiments)

    for i, doc in enumerate(docs):
        for j, sentence in enumerate(doc):
            if j < max_sentences:
                for t, char in enumerate(sentence[-maxlen:]):
                    X[i, j, (maxlen-1-t)] = char_indices[char]

    print('Sample X:{}'.format(X[1200, 2]))
    print('y:{}'.format(y[1200]))

    ids = np.arange(len(X))
    np.random.shuffle(ids)

    # shuffle
    X = X[ids]
    y = y[ids]

    X_train = X[:20000]
    X_test = X[20000:]

    y_train = y[:20000]
    y_test = y[20000:]

    # model hyper parameter
    params = dict()
    params['filter_length_list'] = [[5, 3, 3], [7, 3, 3]]
    params['nb_filter_list'] = [[100, 200, 200], [200, 300, 300]]
    params['pool_length'] = [[2, 2, 2], [2, 2, 2]]
    params['maxlen'] = maxlen
    params['max_sentences'] = max_sentences
    params['dropout_rate'] = 0.2
    params['label_type'] = 'multi-label'
    params['label_dims'] = 1
    params['lr'] = 0.001
    params['hidden_dim'] = 64


    model = charCNN_CNN(params)
    model.build_model()

    if checkpoint:
        model.end2end_model.load_weights(checkpoint)

    file_name = os.path.basename(sys.argv[0]).split('.')[0]
    check_cb = keras.callbacks.ModelCheckpoint('checkpoints/'+file_name+'.{epoch:02d}-{val_loss:.2f}.hdf5', monitor='val_loss',
                                               verbose=0, save_best_only=True, mode='min')
    earlystop_cb = keras.callbacks.EarlyStopping(monitor='val_loss', patience=7, verbose=1, mode='auto')
    history = LossHistory()

    model.end2end_model.fit(X_train, y_train, validation_data=(X_test, y_test), batch_size=10,
              nb_epoch=15, shuffle=True, callbacks=[earlystop_cb, check_cb, history])

if __name__ == '__main__':
    main()