'''
This is a keras implementation of simple Convolutional Neural Networks for Sentence Classification. Modified from keras examples at https://github.com/fchollet/keras/blob/master/examples/imdb_cnn.py
'''

from __future__ import print_function
import numpy as np
np.random.seed(1337)  # for reproducibility

import cPickle
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, LSTM, GRU, Input, Bidirectional, Activation, Reshape
from keras.layers import Embedding, Lambda
from keras.layers import Convolution1D, MaxPooling1D, Convolution2D, AveragePooling1D
from keras.layers import Input, TimeDistributed
from keras.datasets import imdb
from keras import backend as K
from keras.models import Model
from keras.constraints import maxnorm
from keras.optimizers import *
from sklearn.metrics import f1_score, accuracy_score
import sys 
import os
sys.path.append(os.path.abspath("/usr1/home/ruochenx/Theano_Workplace/DNN_text_clas"))
from TestModel import trainTestModel
from TestModel import checkMultiClass, myAccuracy, myF1Score
sys.stderr = open(sys.argv[0]+'_errorlog.txt', 'w')
import argparse

# import tensorflow as tf
# from keras import backend as K
# with K.tf.device('/gpu:3'):

#     gpu_options = tf.GPUOptions(allow_growth =True)
#     K.set_session(K.tf.Session(config=K.tf.ConfigProto(gpu_options=gpu_options)))

class semiSeq:
    def __init__(self, params):
        self.max_features = params['max_features']
        self.batch_size = params['batch_size']
        self.hidden_dim = params['hidden_dim']
        self.bi_direction = params['bi_direction']
        self.nb_epoch = params['nb_epoch']
        self.dropout_rate = params['dropout_rate']
        self.use_pretrained_embedding = params['use_pretrained_embedding']
        self.label_type = params['label_type']
        self.embedding_dims = params['embedding_dims']
        self.label_dims = params['label_dims']
        self.maxlen = params['maxlen']
        self.opt = params['opt']

        self.feature_extractor = None
        self.top_logistic_regression = None
        self.end2end_model = None
        self.embedding_lookup = None
        self.embedding_weight = None

    def build_model(self, pretrained_embedding, fix_embedding = False, temperature=None):

        # we start off with an efficient embedding layer which maps
        # our vocab indices into embedding_dims dimensions
        word_input = Input(shape=(self.maxlen, ), dtype='int32', name='word_input')
        self.word_input = word_input
        # add embedding
        if self.use_pretrained_embedding and not pretrained_embedding is None:
        #    pretrained_embedding = np.random.rand(max_features, embedding_dims)
            if fix_embedding:
                self.embedding_lookup = Embedding(output_dim=self.embedding_dims, input_dim=self.max_features, input_length=self.maxlen, weights=[pretrained_embedding], trainable=False, mask_zero=True)
                self.embedding_weight = pretrained_embedding
            else:
                self.embedding_lookup = Embedding(output_dim=self.embedding_dims, input_dim=self.max_features, input_length=self.maxlen, weights=[pretrained_embedding], mask_zero=True)
        else:
            if fix_embedding:
                print('ERROR:Using random embedding as fix!')
                sys.exit(-1)
            self.embedding_lookup = Embedding(output_dim=self.embedding_dims, input_dim=self.max_features, input_length=self.maxlen, mask_zero=True)
        embedding_layer = self.embedding_lookup(word_input)

        # we add a (Bi-directional) RNN
        # reshaped_embedding_layer = Reshape((self.maxlen, self.embedding_dims))(embedding_layer)
        if self.bi_direction:
            RNN = Bidirectional(GRU(self.hidden_dim, consume_less='gpu'))
        else:
            RNN = GRU(self.hidden_dim, consume_less='gpu')
        # Weights inside GRU:    
        # self.W = K.concatenate([self.W_z, self.W_r, self.W_h])
        # self.U = K.concatenate([self.U_z, self.U_r, self.U_h])
        # self.b = K.concatenate([self.b_z, self.b_r, self.b_h])

        rnn_layer = RNN(embedding_layer)                                          
        self.feature_output = rnn_layer

        # define feature_extractor part from above
        self.feature_extractor = Model(input=word_input, output=rnn_layer)


        # standard logistic regression part
        # feature_input = Input(shape=(self.feature_extractor.output_shape[1], ), dtype='float32')
        x = Dropout(self.dropout_rate)(rnn_layer)

        # We project onto a single unit output layer, and squash it with a sigmoid:
        logits = Dense(self.label_dims)(x)

        if self.label_type == 'multi-class':
            end2end_output = Activation('softmax')(logits)
        elif self.label_type == 'multi-label':
            end2end_output = Activation('sigmoid')(logits)
        else:
            print('undefined label type {0}'.format(self.label_type))
            sys.exit()

        # self.top_logistic_regression = Model(input=feature_input, output=x)

        # define the end-to-end model
        self.end2end_model = Model(input=word_input, output=end2end_output)

        if temperature is not None:
            hot_logits = Lambda(lambda x: x / temperature)(logits)
            if self.label_type == 'multi-class':
                end2end_soft_output = Activation('softmax', name='soft_output')(hot_logits)
            elif self.label_type == 'multi-label':
                end2end_soft_output = Activation('sigmoid', name='soft_output')(hot_logits)
            else:
                print('undefined label type {0}'.format(self.label_type))
                sys.exit()
            # self.top_soft_logistic_regression = Model(input=feature_input, output=soft_output)
            self.soft_output = end2end_soft_output
            self.end2end_soft_model = Model(input=word_input, output=end2end_soft_output)

        if self.label_type == 'multi-class':
            loss_type = 'categorical_crossentropy'
        elif self.label_type == 'multi-label':
            loss_type = 'binary_crossentropy'

        self.end2end_model.compile(loss=loss_type,
                      optimizer=self.opt,
                      metrics=['accuracy', 'categorical_accuracy', 'fmeasure'])
        if temperature is not None:
            self.end2end_soft_model.compile(loss=loss_type,
                          optimizer=self.opt,
                          metrics=['accuracy', 'categorical_accuracy', 'fmeasure'])

sys.stderr.close()
sys.stderr = sys.__stderr__