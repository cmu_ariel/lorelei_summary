import json

def main():
    params = dict()
    params['hidden_dim'] = 50
    params['nb_filter'] = 100
    params['filter_length_list'] = [3,4,5]
    params['dropout_rate'] = 0.2
    params['use_pretrained_embedding'] = True
    params['l2_constraint'] = 10
    json.dump(params, open("MultiReview/kimCNN.json",'w'))

if __name__ == '__main__':
    main()

