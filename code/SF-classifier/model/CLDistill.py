import random
import math

from sklearn.preprocessing import normalize

from keras.optimizers import *
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Reshape, Merge
from keras.layers import Embedding
from keras.layers import Convolution1D, MaxPooling1D, Convolution2D, AveragePooling1D
from keras.layers import Input
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.datasets import imdb
from keras import backend as K
from keras.models import Model, load_model
from keras.models import model_from_json

from model.my_distillation import *
from model.adversarial import *
from model.DNN_text.kimCNN_text import kimCNN
from model.DNN_text.charCNN_text import charCNN

from utils.data_helper import *
from utils.parl_data_helper import load_parl
from utils.config_model import config_hyper_parameter
from utils.JSON_predictor import json_predictor, typeDict


def my_pad_sequences(X, maxlen):
    if X is not None:
        return sequence.pad_sequences(X, maxlen)
    else:
        return X

class CLD(object):
    def __init__(self, config, logger):
        self.config = config
        self.logger = logger
        self.y_tgt_pred = dict()
        self.alphas = [-1, -0.5, -0.2, 0, 0.2, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5]

    def read_src(self, train_path):
        self.X_src_train, self.y_src_train,\
        self.src_emb, self.src_nb_words,\
        self.src_maxlen, self.catgy_ind,\
        self.src_word_idx_map = read_reliefweb(train_path, self.config['eng_wordembedding_path'], logger=self.logger)
        self.src_idx_word_map = rev_dict(self.src_word_idx_map)
        self.src_idx_word_map[0] = 'UKN'
        return 0

    def read_tgt(self):
        if self.config['tgt_char_model']:
            self.X_tgt_test, self.tgt_emb, \
            self.tgt_nb_words, self.tgt_maxlen, \
            self.tgt_all_docs, self.tgt_word_idx_map, _ = read_ltf_char(self.config['tgt_test_path'])
        else:
            self.X_tgt_test, self.tgt_emb, \
            self.tgt_nb_words, self.tgt_maxlen, \
            self.tgt_all_docs, self.tgt_word_idx_map, _ = read_ltf(self.config['tgt_test_path'], self.config['tgt_wordembedding_path'], maxlen=self.config['maxlen'], seg=self.config['seg'], include_eng=self.config['include_eng'], eng_SN_only=self.config['eng_SN_only'], eng_word_idx=self.src_word_idx_map, eng_nb_words=self.src_nb_words, logger=self.logger)
        self.X_tgt_test_raw = self.X_tgt_test
        self.tgt_idx_word_map = rev_dict(self.tgt_word_idx_map)
        self.tgt_idx_word_map[0] = 'UKN'
        return 0

    def read_parl(self):
        self.X_src_parl, self.X_tgt_parl = load_parl(parl_paths=self.config['parl_path'], src_word_idx=self.src_word_idx_map, tgt_word_idx=self.tgt_word_idx_map, src_maxlen=self.src_maxlen, tgt_maxlen=self.tgt_maxlen, tgt_char_model=self.config['tgt_char_model'])
        return 0

    def write_json(self, thres):
        json_predictor(pred=self.y_tgt_pred, docs=self.tgt_all_docs, X_docs=self.X_tgt_test_raw, idx=self.tgt_idx_word_map, output_file=join(self.config['save_path'], 'out.json'), thres=thres)
        json_predictor(pred=self.y_tgt_pred, docs=self.tgt_all_docs, X_docs=self.X_tgt_test_raw, idx=self.tgt_idx_word_map, output_file=join(self.config['save_path'], 'out.sqrt_thres.json'), thres=np.sqrt(self.sf_type_zero_predict))

    def train(self, name):
        # set parameters:
        params = dict()
        # set data-depandant params
        params['batch_size'] = self.config['batch_size']
        params['nb_epoch'] = self.config['nb_epoch']
        params['label_type'] = self.config['label_type']      # for multi-label we use sigmoid on each output logit
                                                    # for multi-class we just use softmax

        # source training data
        self.src_emb = normalize(self.src_emb, axis=1, norm='l2')
        self.logger.info('Source pretrained embedding size:' + str(self.src_emb.shape))
        self.logger.info('Source max features:' + str(self.src_nb_words))
        self.logger.info('Finish Source Data')

        # target training data
        if self.tgt_emb is not None:
            self.tgt_emb = np.concatenate((self.src_emb, self.tgt_emb), axis=0)
            self.tgt_emb = normalize(self.tgt_emb, axis=1, norm='l2')
            self.logger.info('Target pretrained embedding size:' + str(self.tgt_emb.shape))
        self.logger.info('Target max features:' + str(self.tgt_nb_words))

        # learning rate
        lr = self.config['lr']
        params['label_dims'] = self.y_src_train.shape[1]
        params['lr'] = lr

        ## source and target parameters
        src_params = dict(params)
        tgt_params = dict(params)

        # set feature model-depandant params
        hp = self.config['hyper_params']
        config_hyper_parameter(hp[0], src_params)
        if len(hp) > 1: # use target specific hp
            config_hyper_parameter(hp[1], tgt_params)
        else:
            config_hyper_parameter(hp[0], tgt_params)
        
        src_params['maxlen'] = self.src_maxlen
        tgt_params['maxlen'] = self.tgt_maxlen
        src_params['embedding_dims'] = self.src_emb.shape[1]
        if self.tgt_emb is not None:
            tgt_params['embedding_dims'] = self.tgt_emb.shape[1]
            tgt_params['max_features'] = self.tgt_emb.shape[0]
        else:
            tgt_params['max_features'] = len(self.tgt_word_idx_map)
        src_params['max_features'] = self.src_emb.shape[0]
        
        self.logger.info('pretrained embedding shape:' + str(self.src_emb.shape))

        self.logger.info(str(len(self.X_src_train)) + ' train sequences')
        self.logger.info(str(len(self.X_tgt_test)) + ' test sequences')

        self.logger.info('Pad sequences (samples x time)')
        self.X_src_train = my_pad_sequences(self.X_src_train, maxlen=self.src_maxlen)

        if type(self.X_tgt_test) is list:
            self.X_tgt_test = my_pad_sequences(self.X_tgt_test, maxlen=self.tgt_maxlen)
    
        self.X_tgt_parl = my_pad_sequences(self.X_tgt_parl, maxlen=self.tgt_maxlen)
        self.X_src_parl = my_pad_sequences(self.X_src_parl, maxlen=self.src_maxlen)
        
        self.X_src_train = np.asarray(self.X_src_train, dtype=np.int32)
        self.X_tgt_test = np.asarray(self.X_tgt_test, dtype=np.int32)
        
        self.X_src_parl = np.asarray(self.X_src_parl, dtype=np.int32)
        self.X_tgt_parl = np.asarray(self.X_tgt_parl, dtype=np.int32)

        self.logger.info('X_tgt_test max:' + str(self.X_tgt_test.max()))

        self.logger.info('X_src_train shape:' + str(self.X_src_train.shape))
        self.logger.info('y_src_train shape:' + str(self.y_src_train.shape))
        
        self.logger.info('X_tgt_test shape:' + str(self.X_tgt_test.shape))
        self.logger.info('X_src_parl shape:' + str(self.X_src_parl.shape))
        self.logger.info('X_tgt_parl shape:' + str(self.X_tgt_parl.shape))

        self.logger.info('Building model...')
        src_model = kimCNN(src_params)
        if self.config['tgt_char_model']:
            tgt_model = charCNN(tgt_params)
        else:
            tgt_model = kimCNN(tgt_params)

        # build models
        tgt_model.build_model(pretrained_embedding=self.tgt_emb, temperature=self.config['temp'])

        # train the source model
        if not os.path.isfile(join(self.config['save_path'], 'src_model.' + name + '.json')):
            src_model.build_model(pretrained_embedding=self.src_emb, temperature=self.config['temp'])
            if self.config['adv'] == False:
                # standard training of source model
                file_path = '/tmp/distill_parl_' + get_time_stamp() + '.' + str(os.getpid()) + '.model' 
                checkpoint = ModelCheckpoint(file_path, monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
                early_stopping = EarlyStopping(monitor='val_loss', patience=3, verbose=0, mode='auto')
                callbacks_list = [checkpoint, early_stopping]
                src_model.end2end_model.fit(self.X_src_train, self.y_src_train,
                      batch_size=params['batch_size'],
                      nb_epoch=params['nb_epoch'],
                      validation_split=0.2,
                      callbacks=callbacks_list)
                src_model.end2end_model.load_weights(file_path)
                os.remove(file_path)
            else:
                # adversarial training of source model with parallel text
                adv_hp = dict()
                config_hyper_parameter(self.config['adv_hp'], adv_hp)
                domain_discriminator = get_discriminator(src_model.feature_extractor.output_shape[1], hidden_dim=adv_hp['adv_disc_hidden_dim'], depth=adv_hp['adv_disc_depth'], dropout_rate=adv_hp['dropout_rate'])
                train_for_adversarial_GRL(src_model, domain_discriminator, self.X_src_train[:int(math.floor(self.X_src_train.shape[0]*0.8)),:], self.y_src_train[:int(math.floor(self.X_src_train.shape[0]*0.8))], self.X_src_train[int(math.floor(self.X_src_train.shape[0]*0.8)):,:], self.y_src_train[int(math.floor(self.X_src_train.shape[0]*0.8)):], self.X_src_parl, nb_epoch=adv_hp['nb_epoch'], batch_size=adv_hp['batch_size'], lr=adv_hp['lr'], k=adv_hp['k'], l=adv_hp['l'], hp_lambda=adv_hp['hp_lambda'], plt_frq=adv_hp['plt_frq'], plot=[join(self.config['save_path'], 'distill.src_trn.png'), join(self.config['save_path'],'distill.src_val.png')])

            my_save_model(src_model.end2end_model, join(self.config['save_path'], 'src_model.' + name))
            
        else:
            src_end2end_model = my_load_model(join(self.config['save_path'], 'src_model.' + name))
            src_model.build_model(pretrained_embedding=self.src_emb, temperature=self.config['temp'])
            restore_weights(src_end2end_model, src_model.end2end_soft_model)
            restore_weights(src_end2end_model, src_model.feature_extractor)

        # train the target model with distillation
        simple_distill_parl(src_model, tgt_model, params, self.X_src_parl, self.X_tgt_parl, adv=self.config['adv'], adv_hp=self.config['adv_hp'], X_tgt_unlabeled=self.X_tgt_test, X_tgt_val=None, y_tgt_val=None, X_tgt_test=self.X_tgt_test, src_idx_word=self.src_idx_word_map, tgt_idx_word=self.tgt_idx_word_map, save_path=self.config['save_path'])

        # predict on all zero test data, to determine the threshold
        if name == 'sf_type':
            self.logger.info('Zero input results:')
            X_zero = np.zeros((1, self.X_tgt_test.shape[1]))
            self.sf_type_zero_predict = np.squeeze(tgt_model.end2end_model.predict(X_zero, batch_size=1)[0])
            catgy_zero_predict = dict()
            for catgy in self.catgy_ind:
                if catgy in typeDict:
                    catgy_zero_predict[typeDict[catgy]] = float(self.sf_type_zero_predict[self.catgy_ind[catgy]])
                else:
                    catgy_zero_predict[catgy] = float(self.sf_type_zero_predict[self.catgy_ind[catgy]])

        self.y_tgt_pred[name] = (tgt_model.end2end_model.predict(self.X_tgt_test, batch_size=params['batch_size']), dict(zip(self.catgy_ind.values(), self.catgy_ind.keys())))
