from __future__ import print_function
from timeit import default_timer as timer
import time
import datetime
import numpy as np
from utils.data_helper import *
from sklearn.preprocessing import normalize
# np.random.seed(1337)  # for reproducibility
import random
import matplotlib
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.preprocessing import sequence
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pickle
import argparse
import sys, os
from utils.config_model import config_hyper_parameter
import random
from model.crossling_text_mmatch import kimCNN, charCNN
# sys.path.append('/usr0/home/ruochenx/TensorFlow_Workplace/cross-ling-classification/src/cross_distillation/DNN_text')
# from charCNN_text import charCNN
from model.domain_regularizer import DomainRegularizer
import math
from keras.preprocessing.sequence import pad_sequences
from keras.models import model_from_json
from model.adversarial import restore_weights
from utils.JSON_predictor import json_predictor, typeDict

def my_pad_sequences(X, maxlen):
    if X is not None:
        return pad_sequences(X, maxlen)
    else:
        return X

def my_save_model(model, f):
    # serialize model to JSON
    model_json = model.to_json()
    with open(f + ".json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(f + ".h5")
    print("Saved model into h5 file!")

def my_load_model(f):
    json_file = open(f + '.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(f + ".h5")
    print("Loaded model from disk")
    return loaded_model

def get_time_stamp():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')

def idx2str(l, idx):
    inv_idx = {v: k for k, v in idx.iteritems()}
    s = [inv_idx[e] for e in l if e > 0]
    return ' '.join(s)

def write_dict(f, d):
    for k, v in d.iteritems():
        f.write(str(k) + ':' + str(v) + '\n')

class Simple_WE(object):
    def __init__(self, config, logger):
        self.config = config
        self.logger = logger
        self.y_tgt_pred = dict()
        self.use_tgt_annot = False
        self.alphas = [-3, -2.5, -2, -1.5, -1, -0.5, -0.2, 0, 0.2, 0.5, 1, 1.5]
        self.word_idx_map =None

    def read_src(self, train_paths):
        if self.config['tgt_char_model']:
            self.X_src_train, self.y_src_train,\
            self.src_emb, self.src_nb_words,\
            self.src_maxlen, self.catgy_ind,\
            self.word_idx_map = read_reliefweb_char(train_paths, avoid_all=False, logger=self.logger)
            self.pre_emb = None
        else:
            self.X_src_train, self.y_src_train,\
            self.src_emb, self.src_nb_words,\
            self.src_maxlen, self.catgy_ind,\
            self.word_idx_map = read_eng_corpora(
                train_paths, self.config['eng_wordembedding_path'],
                maxlen=self.config['maxlen'], logger=self.logger)
            self.use_tgt_annot = False
        return 0

    def read_tgt(self):
        if self.config['tgt_char_model']:
            self.X_tgt_test, self.tgt_emb, \
            self.tgt_nb_words, self.tgt_maxlen, \
            self.tgt_all_docs, self.word_idx_map, _ = read_ltf_char(self.config['tgt_test_path'], char_idx=self.word_idx_map, logger=self.logger)
        else:
            self.X_tgt_test, self.tgt_all_docs, = read_ltf(
                self.config['tgt_test_path'], self.config['eng_wordembedding_path'],
                maxlen=self.config['maxlen'], seg=self.config['seg'], share_vocab=True, eng_word_idx=self.word_idx_map,
                logger=self.logger)
            
            # self.tgt_all_docs, self.word_idx_map, _ = read_ltf(self.config['tgt_test_path'], self.config['tgt_wordembedding_path'], logger=self.logger)
        self.X_tgt_test_raw = self.X_tgt_test
        self.idx_word_map = rev_dict(self.word_idx_map)
        self.idx_word_map[0] = 'UNK'

    def read_tgt_json(self, train_paths):
        raise NotImplementedError

    def write_json(self, thres):
        json_predictor(pred=self.y_tgt_pred, docs=self.tgt_all_docs, X_docs=self.X_tgt_test_raw,
                       idx=self.idx_word_map, output_file=join(self.config['save_path'], 'out.json'),
                       thres=thres, use_seg=self.config['seg'])
        for alpha in self.alphas:
            json_predictor(pred=self.y_tgt_pred, docs=self.tgt_all_docs, X_docs=self.X_tgt_test_raw,
                           idx=self.idx_word_map, output_file=join(self.config['save_path'], 'out.thres.$alpha.json'.replace('$alpha',str(alpha))), thres=self.sf_type_thres[alpha], use_seg=self.config['seg'])

    def sigma_thres(self, ys, alpha=3):
        ys = np.concatenate([ys, 2-ys])
        return 1 - alpha*np.std(ys)

    def mean_thres(self, ys, alpha=3):
        return np.mean(ys) + alpha*np.std(ys)

    def find_tp_thres(self, y_model, alpha=3):
        if not hasattr(self, 'sf_type_zero_pred'):
            X_zero = np.zeros((1, self.X_src_train.shape[1]))
            self.sf_type_zero_pred = np.squeeze(self.src_model.end2end_model.predict([X_zero, X_zero], batch_size=1)[0])
        y = self.y_src_train
        sf_type_thres = np.zeros(self.sf_type_zero_pred.shape)
        self.catgy_sigma_thres = dict()
        for catgy in self.catgy_ind:
            y_per_cat_pred = y_model[y[:,self.catgy_ind[catgy]]==1, self.catgy_ind[catgy]]
            if catgy in typeDict:
                self.catgy_sigma_thres[typeDict[catgy]] = self.mean_thres(y_per_cat_pred, alpha)
            else:
                self.catgy_sigma_thres[catgy] = self.mean_thres(y_per_cat_pred, alpha)

            # sf_type_thres[self.catgy_ind[catgy]] = max([math.sqrt(self.sf_type_zero_pred[self.catgy_ind[catgy]]), self.mean_thres(y_per_cat_pred, alpha)])
            sf_type_thres[self.catgy_ind[catgy]] = max([self.sf_type_zero_pred[self.catgy_ind[catgy]], self.mean_thres(y_per_cat_pred, alpha)])

        self.logger.info('alpha:{}, catgy_sigma_thres: {}'.format(alpha, self.catgy_sigma_thres))
        return sf_type_thres

    def train(self, name):
        # set parameters:
        params = dict()
        # set data-depandant params
        params['batch_size'] = self.config['batch_size']
        params['nb_epoch'] = self.config['nb_epoch']
        params['label_type'] = self.config['label_type']      # for multi-label we use sigmoid on each output logit
                                                    # for multi-class we just use softmax

        # source training data
        self.logger.info('Source pretrained embedding size: {}'.format(self.src_emb.shape))
        self.logger.info('Source max features: {}'.format(self.src_nb_words))
        self.logger.info('Finish Source Data')

        if not self.config['tgt_char_model']:
            self.pre_emb = normalize(self.src_emb, axis=1, norm='l2')

        # learning rate
        lr = self.config['lr']
        params['label_dims'] = self.y_src_train.shape[1]
        params['lr'] = lr

        ## source and target parameters
        src_params = dict(params)

        # set feature model-depandant params
        hp = self.config['hyper_params']
        config_hyper_parameter(hp[0], src_params)
        
        src_params['maxlen'] = self.src_maxlen
        if self.src_emb is not None:
            src_params['embedding_dims'] = self.pre_emb.shape[1]
            src_params['max_features'] = self.pre_emb.shape[0]
        else:
            src_params['max_features'] = self.src_nb_words

        self.logger.info('pretrained embedding shape:' + str(self.pre_emb.shape))

        self.logger.info('{} train sequences'.format(len(self.X_src_train)))
        self.logger.info('{} test sequences'.format(len(self.X_tgt_test_raw)))

        self.logger.info('Pad sequences (samples x time)')
        if type(self.X_src_train) is list:
            self.X_src_train = my_pad_sequences(self.X_src_train, maxlen=self.src_maxlen)

        if type(self.X_tgt_test_raw) is list:
            self.X_tgt_test = my_pad_sequences(self.X_tgt_test_raw, maxlen=self.src_maxlen)

        self.X_src_train = np.asarray(self.X_src_train, dtype=np.int32)
        self.X_tgt_test = np.asarray(self.X_tgt_test, dtype=np.int32)

        self.logger.info('X_tgt_test max: {}'.format(self.X_tgt_test.max()))
        self.logger.info('X_src_train shape: {}'.format(self.X_src_train.shape))
        self.logger.info('y_src_train shape: {}'.format(self.y_src_train.shape))


        # build model
        self.logger.info('Building model...')
        if self.config['tgt_char_model']:
            self.logger.info('Use char model')
            self.src_model = charCNN(src_params)
        else:
            self.src_model = kimCNN(src_params)

        # make self.X_tgt_test the same size as self.X_src_train
        X_tgt_test_ori = self.X_tgt_test
        self.X_tgt_test = np.tile(self.X_tgt_test, (int(math.ceil(float(self.X_src_train.shape[0])/self.X_tgt_test.shape[0])), 1))
        self.X_tgt_test = self.X_tgt_test[:self.X_src_train.shape[0],:]

        if self.config['domain_match'] is None or self.config['domain_match'] == 'simple':
            self.src_model.build_model(pretrained_embedding=self.pre_emb)
        elif self.config['domain_match'] == 'mmatch':
            mmatch_penalty = DomainRegularizer(l=self.config['alpha'], name='mmatch')
            self.src_model.build_model(pretrained_embedding=self.pre_emb, domain_regularizer=mmatch_penalty)

        # outputs = src_model.end2end_model.predict([self.X_src_train, self.X_src_train], batch_size=params['batch_size'])
        # print('Train predicts before training:', outputs)

        # outputs = src_model.end2end_model.predict([X_tgt_test_ori, X_tgt_test_ori], batch_size=params['batch_size'])
        # print('Test predicts before training:', outputs)

        # train the source model
        if not os.path.isfile(join(self.config['save_path'], 'src_model.' + name + '.json')):

            dummy_y_tgt_unlabeled=np.zeros((self.X_tgt_test.shape[0], params['label_dims']))
            dummy_y_tgt_unlabeled[0,:] = 1
            file_path = '/tmp/cmd_bwe_' + get_time_stamp() + '.' + str(os.getpid()) + '.model' 
            checkpoint = ModelCheckpoint(file_path, monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
            early_stopping = EarlyStopping(monitor='val_loss', patience=3, verbose=0, mode='auto')
            callbacks_list = [checkpoint, early_stopping]
            self.src_model.end2end_model.fit([self.X_src_train, self.X_tgt_test],
                                             [self.y_src_train, dummy_y_tgt_unlabeled],
                                             batch_size=params['batch_size'],
                                             epochs=params['nb_epoch'],
                                             validation_split=0.2,
                                             callbacks=callbacks_list)
            # src_model.end2end_model.fit(self.X_src_train, self.y_src_train,
                  # batch_size=params['batch_size'],
                  # nb_epoch=params['nb_epoch'],
                  # validation_split=0.2,
                  # callbacks=callbacks_list,
                  # )
            self.src_model.end2end_model.load_weights(file_path)
            my_save_model(self.src_model.end2end_model, join(self.config['save_path'], 'self.src_model.' + name))
        else:
            self.logger.info('Loading previous model')
            self.src_model.end2end_model = my_load_model(join(self.config['save_path'], 'self.src_model.' + name))

        if name == 'sf_type':
            # predict on all zero test data, to determine the threshold
            self.logger.info('Zero input results:')
            X_zero = np.zeros((1, X_tgt_test_ori.shape[1]))
            self.sf_type_zero_pred = np.squeeze(self.src_model.end2end_model.predict([X_zero, X_zero], batch_size=1)[0])
            catgy_zero_pred = dict()
            for catgy in self.catgy_ind:
                if catgy in typeDict:
                    catgy_zero_pred[typeDict[catgy]] = float(self.sf_type_zero_pred[self.catgy_ind[catgy]])
                else:
                    catgy_zero_pred[catgy] = float(self.sf_type_zero_pred[self.catgy_ind[catgy]])
            self.logger.info('Zero input scores for categories')
            self.logger.info(catgy_zero_pred)
            self.logger.info('self.sf_type_zero_pred:' + str(self.sf_type_zero_pred))

            # find all true positive predictions, to determine the threshold
            self.sf_type_thres = dict()
            y_model = self.src_model.end2end_model.predict([self.X_src_train,self.X_src_train])[0]
            for alpha in self.alphas:
                self.sf_type_thres[alpha] = self.find_tp_thres(y_model, alpha)
            self.logger.info('self.sf_type_thres:' + str(self.sf_type_thres))

        self.y_tgt_pred[name] = (self.src_model.end2end_model.predict([X_tgt_test_ori, X_tgt_test_ori], batch_size=params['batch_size'])[0], dict(zip(self.catgy_ind.values(), self.catgy_ind.keys())))
        self.logger.info(name + ' prediction shape:' + str(self.y_tgt_pred[name][0].shape))

