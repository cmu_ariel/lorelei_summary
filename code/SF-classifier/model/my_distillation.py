import sys
import os
import time
import datetime
import math
import numpy as np

from keras.models import Model
from keras.layers import Input
from keras.callbacks import ReduceLROnPlateau
from keras.callbacks import ModelCheckpoint, EarlyStopping

from model.adversarial import *

from utils.parl_data_helper import *
from utils.config_model import config_hyper_parameter

DEBUG = True

def get_time_stamp():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')

def simple_distill_parl(src_model, tgt_model, params, X_src_parl, X_tgt_parl, X_tgt_val=None, y_tgt_val=None, adv=False, adv_hp=None, X_tgt_unlabeled=None, X_tgt_test=None, y_tgt_test=None, X_tgt_trn=None, y_tgt_trn=None, src_idx_word=None, tgt_idx_word=None, save_path=None, debug=DEBUG):
    """ distillation method from paper: Distilling the knowledge in a neural network """
    file_path = os.path.dirname(os.path.realpath(__file__)) + "/../model/" + 'distill_parl_' + get_time_stamp() + '.' + str(os.getpid()) + '.model' 
    soft_pred = src_model.end2end_soft_model.predict(X_src_parl, batch_size=params['batch_size'])
    if src_idx_word is not None and tgt_idx_word is not None and debug:
      print('Parallel target data...')
      pprint_parl_data(X_src_parl, X_tgt_parl, soft_pred, src_idx_word, tgt_idx_word)

    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.9,
                  patience=0, min_lr=0.00001)
    checkpoint = ModelCheckpoint(file_path, monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
    early_stopping = EarlyStopping(monitor='val_loss', patience=2, verbose=0, mode='auto')
    callbacks_list = [checkpoint, early_stopping, reduce_lr]
    # if X_tgt_trn is not None and y_tgt_trn is not None:
    #   X_tgt_parl = np.concatenate((X_tgt_parl, X_tgt_trn), axis=0)
    #   soft_pred = np.concatenate((soft_pred, y_tgt_trn), axis=0)
    if adv == False:
      if X_tgt_val is None or y_tgt_val is None:
        tgt_model.end2end_soft_model.fit(X_tgt_parl, soft_pred,
              batch_size=params['batch_size'],
              nb_epoch=params['nb_epoch'],
              validation_split=0.2,
              callbacks=callbacks_list)
      else:
        tgt_model.end2end_soft_model.fit(X_tgt_parl, soft_pred,
              batch_size=params['batch_size'],
              nb_epoch=params['nb_epoch'],
              validation_data=(X_tgt_val, y_tgt_val),
              callbacks=callbacks_list)
      tgt_model.end2end_soft_model.load_weights(file_path)
      os.remove(file_path)
    else:
      val_metric='loss'
      adv_hp_dict = dict()
      config_hyper_parameter(adv_hp, adv_hp_dict)
      domain_discriminator = get_discriminator(tgt_model.feature_extractor.output_shape[1], hidden_dim=adv_hp_dict['adv_disc_hidden_dim'], depth=adv_hp_dict['adv_disc_depth'], dropout_rate=adv_hp_dict['dropout_rate'])
      if X_tgt_val is None or y_tgt_val is None:
        X_trn = X_tgt_parl[:int(math.floor(X_tgt_parl.shape[0]*0.8)),:]
        y_trn = soft_pred[:int(math.floor(X_tgt_parl.shape[0]*0.8))]
        X_val = X_tgt_parl[int(math.floor(X_tgt_parl.shape[0]*0.8)):,:]
        y_val = soft_pred[int(math.floor(X_tgt_parl.shape[0]*0.8)):]
      else:
        X_trn = X_tgt_parl
        y_trn = soft_pred
        X_val = X_tgt_val
        y_val = y_tgt_val
        val_metric = 'acc'
      train_for_adversarial_GRL(tgt_model, domain_discriminator, X_trn, y_trn, X_val, y_val, X_tgt_unlabeled, nb_epoch=adv_hp_dict['nb_epoch'], batch_size=adv_hp_dict['batch_size'], lr=adv_hp_dict['lr'], k=adv_hp_dict['k'], l=adv_hp_dict['l'], hp_lambda=adv_hp_dict['hp_lambda'], plt_frq=adv_hp_dict['plt_frq'], plot=[os.path.join(save_path, 'BWE.tgt_parl.png'), os.path.join(save_path, 'BWE.tgt_val.png')], use_soft_model=True, val_metric=val_metric)
    if X_tgt_trn is not None and y_tgt_trn is not None and X_tgt_trn.shape[0]>0:
      if X_tgt_val is None or y_tgt_val is None:
        tgt_model.end2end_model.fit(X_tgt_trn, y_tgt_trn,
              batch_size=params['batch_size'],
              nb_epoch=params['nb_epoch'],
              validation_split=0.2,
              callbacks=callbacks_list)
      else:
        tgt_model.end2end_model.fit(X_tgt_trn, y_tgt_trn,
              batch_size=params['batch_size'],
              nb_epoch=params['nb_epoch'],
              validation_data=(X_tgt_val, y_tgt_val),
              callbacks=callbacks_list)


def hint_distill_parl(src_model, tgt_model, params, X_src_parl, X_tgt_parl):
    """ 
    ref:
    FITNETS: HINTS FOR THIN DEEP NETS 
    Cross modal distillation for supervision transfer 
    distillation through the combination of soft logits and intermediate representation
    """
    if src_model.label_type == 'multi-class':
        loss_type = 'categorical_crossentropy'
    elif src_model.label_type == 'multi-label':
        loss_type = 'binary_crossentropy'
    soft_pred = src_model.end2end_soft_model.predict(X_src_parl, batch_size=params['batch_size'])
    med_feature = src_model.feature_extractor.predict(X_src_parl, batch_size=params['batch_size'])
    # define the combined model for distillation
    # word_input = Input(shape=(src_model.maxlen, ), name='word_input', dtype='int32')
    # feature_output = tgt_model.feature_extractor(word_input, name='feature_output')
    # soft_output = tgt_model.end2end_soft_model(word_input, name='soft_output')
    distill_model = Model(input=[tgt_model.word_input], output=[tgt_model.feature_output, tgt_model.soft_output])
    distill_model.compile(optimizer=params['opt'], loss={'feature_output': 'mse', 'soft_output': loss_type},
              loss_weights=[params['alpha'], 1.0])

    file_path = "../model/" + 'distill_parl_' + get_time_stamp() + '.' + str(os.getpid()) + '.model' 
    checkpoint = ModelCheckpoint(file_path, monitor='val_loss', verbose=1, save_best_only=True, mode='auto')
    early_stopping = EarlyStopping(monitor='val_loss', patience=3, verbose=0, mode='auto')
    callbacks_list = [checkpoint, early_stopping]
    distill_model.fit({'word_input': X_tgt_parl},
          {'feature_output': med_feature, 'soft_output': soft_pred},
          batch_size=params['batch_size'],
          nb_epoch=params['nb_epoch'],
          validation_split=0.2,
          callbacks=callbacks_list)
    distill_model.load_weights(file_path)
    os.remove(file_path)

def restore_weights(src_model, tgt_model, verbose=0):
    src_ws = src_model.get_weights()
    tgt_ws = tgt_model.get_weights()
    if verbose > 0:
      print('source model size:')
      for w in src_ws:
        print(w.shape)
        if w.shape[0] < 10 and len(w.shape) == 1:
          print(w)
        print('------')

    tgt_model.set_weights(src_ws[:len(tgt_ws)])
    tgt_ws = tgt_model.get_weights()
    if verbose > 0:
      print('target model size:')
      for w in tgt_ws:
        print(w.shape)
        if w.shape[0] < 10 and len(w.shape) == 1:
          print(w)
        print('------')
    return 0