import sys, os
from timeit import default_timer as timer
import time
import datetime
import random
import math

import numpy as np

# from model.crossling_text_mmatch import kimCNN, charCNN
from model.DNN_text.kimCNN_text import kimCNN
from model.DNN_text.charCNN_text import charCNN
from model.domain_regularizer import DomainRegularizer
from model.adversarial import *
from model.keras_helper import partial_label_fit

from utils.data_helper import *
from utils.parl_data_helper import load_parl
from utils.config_model import config_hyper_parameter
from utils.JSON_predictor import json_predictor, typeDict

from sklearn.preprocessing import normalize
# np.random.seed(1337)  # for reproducibility

from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.preprocessing import sequence
from keras.preprocessing.sequence import pad_sequences
from keras.models import model_from_json

class GRL_BWE(object):
    def __init__(self, config, logger):
        self.config = config
        self.logger = logger
        self.y_tgt_pred = dict()
        self.use_tgt_annot = False
        self.alphas = [-3, -2.5, -2, -1.5, -1, -0.5, -0.2, 0, 0.2, 0.5, 1, 1.5]

    def read_src(self, train_paths):
        self.X_src_train, self.y_src_train,\
        self.src_emb, self.src_nb_words,\
        self.src_maxlen, self.catgy_ind,\
        self.src_word_idx_map = read_eng_corpora(
            train_paths, self.config['eng_wordembedding_path'],
            maxlen=self.config['maxlen'], logger=self.logger)
        self.src_idx_word_map = rev_dict(self.src_word_idx_map)
        self.src_idx_word_map[0] = 'UNK'
        self.use_tgt_annot = False
        return 0

    def read_tgt(self):
        self.X_tgt_test, self.tgt_emb, \
        self.tgt_nb_words, self.tgt_maxlen, \
        self.tgt_all_docs, self.tgt_word_idx_map, self.tgt_all_vocab = read_ltf(
            self.config['tgt_test_path'], self.config['tgt_wordembedding_path'],
            maxlen=self.config['maxlen'], seg=self.config['seg'],
            include_eng=self.config['include_eng'], eng_SN_only=self.config['eng_SN_only'],
            eng_word_idx=self.src_word_idx_map, eng_nb_words=self.src_nb_words, logger=self.logger)
        self.X_tgt_test_raw = self.X_tgt_test
        self.tgt_idx_word_map = rev_dict(self.tgt_word_idx_map)
        self.tgt_idx_word_map[0] = 'UNK'
        self.tgt_idx_word_map[self.src_nb_words] = 'UNK'
        # vocab_file = open(os.path.join(self.config["save_path"], 'tgt_vocab.txt'), 'w')
        # for k in self.tgt_all_vocab:
        #     vocab_file.write(k.encode('utf-8 ') + '\n')
        return 0

    def read_parl(self):
        self.X_src_parl, self.X_tgt_parl = load_parl(parl_paths=self.config['parl_path'], src_word_idx=self.src_word_idx_map, tgt_word_idx=self.tgt_word_idx_map,
                                                     src_maxlen=self.src_maxlen, tgt_maxlen=self.tgt_maxlen, tgt_char_model=False)
        self.X_src_parl = my_pad_sequences(self.X_src_parl, maxlen=self.src_maxlen)
        self.X_tgt_parl = my_pad_sequences(self.X_tgt_parl, maxlen=self.tgt_maxlen)
        self.X_src_parl = np.asarray(self.X_src_parl, dtype=np.int32)
        self.X_tgt_parl = np.asarray(self.X_tgt_parl, dtype=np.int32)
        return 0

    def read_tgt_json(self, train_paths):
        self.logger.info('Reading target annotation....')
        self.logger.info('Num of categories: {}'.format(len(self.catgy_ind)))
        self.X_tgt_train, self.y_tgt_train = list(), list()
        for train_path in train_paths:
            X_tgt_train, y_tgt_train = read_json_annot(train_path, word_idx_map=self.tgt_word_idx_map, catgy_ind=self.catgy_ind, maxlen=self.config['maxlen'])
            self.X_tgt_train.append(X_tgt_train)
            self.y_tgt_train.append(y_tgt_train)
            print(y_tgt_train.shape)
        # self.X_tgt_train = np.concatenate(self.X_tgt_train) + self.src_nb_words
        self.X_tgt_train = np.concatenate(self.X_tgt_train)
        self.y_tgt_train = np.concatenate(self.y_tgt_train)
        self.use_tgt_annot = True

    def write_json(self, thres):
        json_predictor(pred=self.y_tgt_pred, docs=self.tgt_all_docs, X_docs=self.X_tgt_test_raw,
                       idx=self.tgt_idx_word_map, output_file=join(self.config['save_path'], 'out.json'),
                       thres=thres, use_seg=self.config['seg'])
        for alpha in self.alphas:
            path_output = join(self.config['save_path'],
                               'out.thres.$alpha.json'.replace('$alpha',str(alpha)))
            json_predictor(pred=self.y_tgt_pred, docs=self.tgt_all_docs, X_docs=self.X_tgt_test_raw,
                           idx=self.tgt_idx_word_map,
                           output_file=path_output,
                           thres=self.sf_type_thres[alpha], use_seg=self.config['seg'])

    def sigma_thres(self, ys, alpha=3):
        ys = np.concatenate([ys, 2-ys])
        return 1 - alpha*np.std(ys)

    def mean_thres(self, ys, alpha=3):
        return np.mean(ys) + alpha*np.std(ys)

    def find_tp_thres(self, y_model, alpha=3):
        if not hasattr(self, 'sf_type_zero_pred'):
            X_zero = np.zeros((1, self.X_src_train.shape[1]))
            self.sf_type_zero_pred = np.squeeze(self.src_model.end2end_model.predict([X_zero, X_zero], batch_size=1)[0])
        y = self.y_src_train
        sf_type_thres = np.zeros(self.sf_type_zero_pred.shape)
        self.catgy_sigma_thres = dict()
        for catgy in self.catgy_ind:
            y_per_cat_pred = y_model[y[:,self.catgy_ind[catgy]]==1, self.catgy_ind[catgy]]
            if catgy in typeDict:
                self.catgy_sigma_thres[typeDict[catgy]] = self.mean_thres(y_per_cat_pred, alpha)
            else:
                self.catgy_sigma_thres[catgy] = self.mean_thres(y_per_cat_pred, alpha)

            # sf_type_thres[self.catgy_ind[catgy]] = max([math.sqrt(self.sf_type_zero_pred[self.catgy_ind[catgy]]), self.mean_thres(y_per_cat_pred, alpha)])
            sf_type_thres[self.catgy_ind[catgy]] = max([self.sf_type_zero_pred[self.catgy_ind[catgy]], self.mean_thres(y_per_cat_pred, alpha)])

        self.logger.info('alpha:{}, catgy_sigma_thres: {}'.format(alpha, self.catgy_sigma_thres))
        return sf_type_thres

    def train(self, name):
        # set parameters:
        params = dict()
        # set data-depandant params
        params['batch_size'] = self.config['batch_size']
        params['nb_epoch'] = self.config['nb_epoch']
        params['label_type'] = self.config['label_type']      # for multi-label we use sigmoid on each output logit
                                                    # for multi-class we just use softmax
        params['pool'] = self.config['pool']

        # source training data
        # self.src_emb = normalize(self.src_emb, axis=1, norm='l2')
        self.logger.info('Source pretrained embedding size: {}'.format(self.src_emb.shape))
        self.logger.info('Source max features: {}'.format(self.src_nb_words))
        self.logger.info('Finish Source Data')

        # target training data
        # self.tgt_emb = normalize(self.tgt_emb, axis=1, norm='l2')
        self.logger.info('Target pretrained embedding size: {}'.format(self.tgt_emb.shape))
        self.logger.info('Target max features: {}'.format(self.tgt_nb_words))

        self.pre_emb = normalize(np.concatenate((self.src_emb, self.tgt_emb), axis=0), axis=1, norm='l2')

        # learning rate
        lr = self.config['lr']
        params['label_dims'] = self.y_src_train.shape[1]
        params['lr'] = lr

        ## source and target parameters
        src_params = dict(params)

        # set feature model-depandant params
        hp = self.config['hyper_params']
        config_hyper_parameter(hp[0], src_params)

        src_params['maxlen'] = self.src_maxlen
        src_params['embedding_dims'] = self.pre_emb.shape[1]
        src_params['max_features'] = self.pre_emb.shape[0]

        self.logger.info('pretrained embedding shape:' + str(self.pre_emb.shape))

        self.logger.info('{} train sequences'.format(len(self.X_src_train)))
        self.logger.info('{} test sequences'.format(len(self.X_tgt_test_raw)))

        self.logger.info('Pad sequences (samples x time)')
        self.X_src_train = my_pad_sequences(self.X_src_train, maxlen=self.src_maxlen)

        if type(self.X_tgt_test_raw) is list:
            self.X_tgt_test = my_pad_sequences(self.X_tgt_test_raw, maxlen=self.tgt_maxlen)

        self.X_src_train = np.asarray(self.X_src_train, dtype=np.int32)
        self.X_tgt_test = np.asarray(self.X_tgt_test, dtype=np.int32)
        # self.X_tgt_test = self.X_tgt_test + self.src_nb_words

        # make self.X_tgt_test the same size as self.X_src_train
        X_tgt_test_ori = self.X_tgt_test
        self.X_tgt_test = np.tile(self.X_tgt_test, (int(math.ceil(float(self.X_src_train.shape[0])/self.X_tgt_test.shape[0])), 1))
        self.X_tgt_test = self.X_tgt_test[:self.X_src_train.shape[0],:]


        self.logger.info('X_tgt_test max: {}'.format(self.X_tgt_test.max()))
        self.logger.info('X_src_train shape: {}'.format(self.X_src_train.shape))
        self.logger.info('y_src_train shape: {}'.format(self.y_src_train.shape))

        # build model
        self.logger.info('Building model...')
        src_model = kimCNN(src_params)
        src_model.build_model(pretrained_embedding=self.pre_emb)

        # train the source model
        if not os.path.isfile(join(self.config['save_path'], 'src_model.' + name + '.json')):
            self.logger.info("Training model...")
            # adversarial training of source model
            adv_hp = dict()
            config_hyper_parameter(self.config['adv_hp'], adv_hp)
            domain_discriminator = get_discriminator(src_model.feature_extractor.output_shape[1], hidden_dim=adv_hp['adv_disc_hidden_dim'], depth=adv_hp['adv_disc_depth'], dropout_rate=adv_hp['dropout_rate'])

            train_for_adversarial_GRL(src_model, domain_discriminator, self.X_src_train, self.y_src_train, None, None, self.X_tgt_test, use_soft_model=False, nb_epoch=adv_hp['nb_epoch'], batch_size=adv_hp['batch_size'], lr=adv_hp['lr'], k=adv_hp['k'], l=adv_hp['l'], hp_lambda=adv_hp['hp_lambda'], plt_frq=adv_hp['plt_frq'], plot=[join(self.config['save_path'], 'distill.src_trn.png'), join(self.config['save_path'],'distill.src_val.png')], logger=self.logger)

            # further use the parallel data
            if 'parl_path' in self.config:
                self.logger.info('Fine-tunning with parallel data...')
                y_parl_pred = src_model.end2end_model.predict([self.X_src_parl, self.X_src_parl], batch_size=params['batch_size'])[0]
                y_model = src_model.end2end_model.predict([self.X_src_train,self.X_src_train])[0]
                high_conf_idx, y_parl_pred = select_conf_idx(y_parl_pred, self.catgy_ind, self.find_tp_thres(y_model, 0), self.X_src_parl, self.X_tgt_parl, self.src_idx_word_map, self.tgt_idx_word_map, self.logger)
                src_model.end2end_opt.lr.set_value(0.0001)
                src_model.end2end_model.fit([self.X_tgt_parl[high_conf_idx,:], self.X_tgt_parl[high_conf_idx,:]], [y_parl_pred[high_conf_idx,:], y_parl_pred[high_conf_idx,:]],
                  batch_size=params['batch_size'],
                  nb_epoch=params['nb_epoch'],
                  validation_split=0.0)

            # further use the target annotation data
            if self.use_tgt_annot:
                self.logger.info('Training with target annotation...')
                # src_model.end2end_opt.lr.set_value(0.0001)
                # src_model.setup_partial_loss()  # setting up a loss for partial fit
                # nb_epoch = params.get('nb_epoch_partial_fit', params['nb_epoch'])
                # src_model.end2end_model = partial_label_fit(
                #     src_model.end2end_model, self.X_tgt_train, self.y_tgt_train,
                #     self.catgy_ind, batch_size=params['batch_size'], nb_epoch=nb_epoch)
                # self.logger.info("Finish training on source language")

                src_model.end2end_opt.lr.set_value(0.001)
                src_model.end2end_model.fit(self.X_tgt_train, self.y_tgt_train,
                      batch_size=params['batch_size'],
                      nb_epoch=params['nb_epoch'],
                      validation_split=0.0)

            # save the model
            my_save_model(src_model.end2end_model, join(self.config['save_path'], 'src_model.' + name))
            self.logger.info("Saved model at {}".format(join(self.config['save_path'], 'src_model.' + name)))
        else:
            self.logger.info('Loading model from disk')
            src_end2end_model = my_load_model(join(self.config['save_path'], 'src_model.' + name))

        if name == 'sf_type':
            # predict on all zero test data, to determine the threshold
            self.logger.info('Zero input results:')
            X_zero = np.zeros((1, X_tgt_test_ori.shape[1]))
            self.sf_type_zero_pred = np.squeeze(src_model.end2end_model.predict(X_zero, batch_size=1))

            catgy_zero_predict = dict()
            for catgy in self.catgy_ind:
                if catgy in typeDict:
                    catgy_zero_predict[typeDict[catgy]] = float(self.sf_type_zero_pred[self.catgy_ind[catgy]])
                else:
                    catgy_zero_predict[catgy] = float(self.sf_type_zero_pred[self.catgy_ind[catgy]])
            self.logger.info('Zero input scores for categories')
            self.logger.info(catgy_zero_predict)
            self.logger.info('self.sf_type_zero_predict:' + str(self.sf_type_zero_pred))

            # find all true positive predictions, to determine the threshold
            self.sf_type_thres = dict()
            y_model = src_model.end2end_model.predict(self.X_src_train)
            for alpha in self.alphas:
                self.sf_type_thres[alpha] = self.find_tp_thres(y_model, alpha)
            self.logger.info('self.sf_type_thres:' + str(self.sf_type_thres))

        self.y_tgt_pred[name] = (
            src_model.end2end_model.predict(X_tgt_test_ori,
                                            batch_size=params['batch_size']),
            dict(zip(self.catgy_ind.values(), self.catgy_ind.keys())))
        self.logger.info(name + ' prediction shape:' + str(self.y_tgt_pred[name][0].shape))
