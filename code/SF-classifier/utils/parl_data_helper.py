""" A data helper to process parallele data from LOREHLT """
from os import listdir
from os.path import join
import sys
import xml.etree.ElementTree as ET
import pickle
from tqdm import tqdm
import numpy as np
import random
from keras.preprocessing.sequence import pad_sequences
import json

def pprint_parl_data(Xs, Xt, ys, idx_s, idx_t):
    for x_s, x_t, y in zip(list(Xs), list(Xt), list(ys)):
        print(y,  ' '.join([idx_s[i] for i in list(x_s)]).encode('utf-8'), ' ||| ', ' '.join([idx_t[i] for i in list(x_t)]).encode('utf-8'))

def add_zero_to_mat(W):
    if W is None:
        return None
    z = np.zeros((1, W.shape[1]), dtype='float32')
    W_new = np.append(z, W, axis=0)
    return W_new

def add_one_to_X(X):
    if X is None:
        return None
    X_new = list()
    for x in X:
        x_new = [t + 1 for t in x]
        X_new.append(x_new)
    return X_new

def load_segments_from_XML(f):
    seg_tok_dict = dict()
    try:
        tree = ET.parse(f)
    except ET.ParseError:
        print('Oops!  That was wrong XML format!')
        return(-1)
    root = tree.getroot()
    doc = root.find('DOC')
    text = doc.find('TEXT')
    for seg in text.findall('SEG'):
        segId = seg.get('id')
        token_list = list()
        for token in seg.findall('TOKEN'):
            token_list.append(token.text)
        seg_tok_dict[segId] = token_list

    return seg_tok_dict

def toks2idx(toks, idx, maxlen):
    o = list()
    for t in toks[:maxlen]:
        if t.lower() in idx:
            o.append(idx[t.lower()])
        else:
            # print t.encode('utf-8')
            o.append(0)
    return o

def char2idx(toks, idx, maxlen, seg_char=' '):
    o = list()
    # s = toks
    s = ' '.join(toks)
    # s = s.split(' ')
    for t in s[-maxlen:]:
        if t.lower() in idx:
            o.append(idx[t.lower()])
        else:
            # print t.encode('utf-8')
            o.append(0)
    return o

def load_parl_raw(raw_path, src_word_idx, tgt_word_idx, src_maxlen, tgt_maxlen, reverse=False, tgt_char_model=False, seg_char=' '):
    X_src, X_tgt = [], []
    # find name for incident language
    files = listdir(raw_path)
    src_name = 'eng'
    files.remove(src_name)
    files.remove('sentence_alignment')
    tgt_name = files[0] 
    # print tgt_name

    for fIn in listdir(join(raw_path, 'sentence_alignment')):
        try:
            tree = ET.parse(join(raw_path, 'sentence_alignment', fIn))
        except ET.ParseError:
            print('Oops!  That was wrong XML format!')
            continue
        aligns = tree.getroot()
        if reverse:
            src_doc_id = aligns.get('translation_id') + '.ltf.xml'
            tgt_doc_id = aligns.get('source_id') + '.ltf.xml'
        else:
            src_doc_id = aligns.get('source_id') + '.ltf.xml'
            tgt_doc_id = aligns.get('translation_id') + '.ltf.xml'
        # print fIn, src_doc_id, tgt_doc_id
        src_segs = load_segments_from_XML(join(raw_path, src_name, 'ltf', src_doc_id))
        tgt_segs = load_segments_from_XML(join(raw_path, tgt_name, 'ltf', tgt_doc_id))
        for align in aligns.findall('alignment'):
            wrong_flag = False
            if reverse:
                src_seg_ids = align.find('translation').get('segments').split()
                tgt_seg_ids = align.find('source').get('segments').split()
            else:
                src_seg_ids = align.find('source').get('segments').split()
                tgt_seg_ids = align.find('translation').get('segments').split()
            src_toks = []
            tgt_toks = []
            for src_seg_id in src_seg_ids:
                if not src_seg_id in src_segs:
                    wrong_flag = True
                    break
                src_toks += src_segs[src_seg_id]
            for tgt_seg_id in tgt_seg_ids:
                if not tgt_seg_id in tgt_segs:
                    wrong_flag = True
                    break
                tgt_toks += tgt_segs[tgt_seg_id]
            if wrong_flag:
                break
            # print '------------'
            # print ' '.join(src_toks).encode('utf-8')
            # print ' '.join(tgt_toks).encode('utf-8')
            X_src.append(toks2idx(src_toks, src_word_idx, src_maxlen))
            if tgt_char_model:
                X_tgt.append(char2idx(tgt_toks, tgt_word_idx, tgt_maxlen, seg_char=seg_char))
            else:
                X_tgt.append(toks2idx(tgt_toks, tgt_word_idx, tgt_maxlen))
            print(seg_char.join(src_toks).encode('utf-8'), seg_char.join(tgt_toks).encode('utf-8'))
            src_toks = [src_word_idx[i] for i in toks2idx(src_toks, src_word_idx, src_maxlen)]
            if tgt_char_model:
                tgt_chars = [tgt_word_idx[i] for i in char2idx(tgt_toks, tgt_word_idx, tgt_maxlen, seg_char=seg_char)]
                tgt_s = seg_char.join(tgt_chars).encode('utf-8')
            else:
                tgt_toks = [tgt_word_idx[i] for i in toks2idx(tgt_toks, tgt_word_idx, tgt_maxlen)]
                tgt_s = seg_char.join(tgt_toks).encode('utf-8')
            print(' '.join(src_toks).encode('utf-8'), tgt_s)
    if tgt_char_model:
        X_tgt = pad_sequences(X_tgt, maxlen=tgt_maxlen)
    return X_src, X_tgt


def rev_word_idx(idx):
    rev_idx = {v:k for k ,v in idx.items()}
    rev_idx[0] = 'UNK'
    return rev_idx

def pprint_data(Xs, ys, idx):
    for x,y in zip(list(Xs), list(ys)):
        print(y, ' '.join([idx[i] for i in list(x)]).encode('utf-8'))

def read_align_sents(p):
    res = list()
    for l in open(p):
        ss = l.strip().split('|||')
        res.append((ss[0].split(), ss[1].split()))
    return res

def load_parl(parl_paths, src_word_idx, tgt_word_idx, src_maxlen, tgt_maxlen, reverse=False, tgt_char_model=False, seg_char= ' '):
    X_src, X_tgt = [], []
    src_idx_word = {v:k for k ,v in src_word_idx.items()}
    src_idx_word[0] = 'UNK'
    tgt_idx_word = {v:k for k ,v in tgt_word_idx.items()}
    tgt_idx_word[0] = 'UNK'
    for parl_path in parl_paths.split(','):
        align_sents = read_align_sents(parl_path)
        random.shuffle(align_sents)
        for tgt_toks, src_toks in tqdm(align_sents):
            X_src.append(toks2idx(src_toks, src_word_idx, src_maxlen))
            if tgt_char_model:
                X_tgt.append(char2idx(tgt_toks, tgt_word_idx, tgt_maxlen, seg_char=seg_char))
            else:
                X_tgt.append(toks2idx(tgt_toks, tgt_word_idx, tgt_maxlen))
            # print(' '.join(src_toks).encode('utf-8'), ' ||| ', ' '.join(tgt_toks).encode('utf-8'))
            src_toks = [src_idx_word[i] for i in toks2idx(src_toks, src_word_idx, src_maxlen)]
            tgt_toks = [tgt_idx_word[i] for i in toks2idx(tgt_toks, tgt_word_idx, tgt_maxlen)]
    if tgt_char_model:
        X_tgt = pad_sequences(X_tgt, maxlen=tgt_maxlen)
        
    return X_src, X_tgt

# def load_parl_bin(bin_paths, src_word_idx, tgt_word_idx, src_maxlen, tgt_maxlen, reverse=False, tgt_char_model=False, seg_char= ' '):
#     X_src, X_tgt = [], []
#     src_idx_word = {v:k for k ,v in src_word_idx.items()}
#     src_idx_word[0] = 'UNK'
#     tgt_idx_word = {v:k for k ,v in tgt_word_idx.items()}
#     tgt_idx_word[0] = 'UNK'
#     for bin_path in bin_paths.split(','):
#         bi_sents = pickle.load(open(bin_path, "rb"))
#         random.shuffle(bi_sents)
#         for src_toks, tgt_toks in tqdm(bi_sents):
#             X_src.append(toks2idx(src_toks, src_word_idx, src_maxlen))
#             if tgt_char_model:
#                 X_tgt.append(char2idx(tgt_toks, tgt_word_idx, tgt_maxlen, seg_char=seg_char))
#             else:
#                 X_tgt.append(toks2idx(tgt_toks, tgt_word_idx, tgt_maxlen))
#             print(' '.join(src_toks).encode('utf-8'), ' ||| ', ' '.join(tgt_toks).encode('utf-8'))
#             src_toks = [src_idx_word[i] for i in toks2idx(src_toks, src_word_idx, src_maxlen)]
#             tgt_toks = [tgt_idx_word[i] for i in toks2idx(tgt_toks, tgt_word_idx, tgt_maxlen)]
#             print(' '.join(src_toks).encode('utf-8'), ' ||| ', ' '.join(tgt_toks).encode('utf-8'))
#     if tgt_char_model:
#         X_tgt = pad_sequences(X_tgt, maxlen=tgt_maxlen)
        
#     return X_src, X_tgt

def load_tgt_bin(path, word_idx, label_idx, maxlen):
    X, Y = [], []
    label_list = pickle.load(open(path, "rb"))
    for toks, label in label_list:
        X.append(toks2idx(toks, word_idx, maxlen))
        y = [0] * len(label_idx)
        y[label_idx[label]] = 1
        Y.append(y)
    return (np.asarray(pad_sequences(X, maxlen=maxlen), dtype=np.int32), np.asarray(Y, dtype=np.int32))

def sample_data(X, y, size):
    idx = np.random.permutation(np.arange(X.shape[0]))
    return X[idx[:size],:], y[idx[:size]]
