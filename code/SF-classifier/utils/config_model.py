import json

def config_hyper_parameter(p, params):
    load_params = json.load(open(p))
    for k in load_params:
        params[k] = load_params[k] 
