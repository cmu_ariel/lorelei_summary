import numpy as np
import random
from tqdm import tqdm
import argparse
import pickle
import json
import codecs
from os import listdir
from os.path import join, exists

def sqrt_thres(thres):
    new_thres = dict()
    for t in thres:
        new_thres[t] = max(0.07, math.sqrt(thres[t]))
    return new_thres

DEFAULT_STATUS = {
    'urgency_status' : False,
    'need_status' : "current",
    'resolution_status' : "insufficient",
    'issue_status' : "current",
}

typeDict = {
    'energy':'utils',
    'evacuation':'evac',
    'food':'food',
    'infrastructure':'infra',
    'medical':'med',
    'regimeChange':'regimechange',
    'rescue':'search',
    'shelter':'shelter',
    'terrorism':'terrorism',
    'unrest':'crimeviolence',
    'water':'water',
}

# typeDict = {
#     'utils':'utils',
#     'evac':'evac',
#     'food':'food',
#     'infra':'infra',
#     'med':'med',
#     'regimechange':'regimechange',
#     'search':'search',
#     'shelter':'shelter',
#     'terrorism':'terrorism',
#     'crimeviolence':'crimeviolence',
#     'water':'water',
# }

typeRevDict = {v:k for k,v in typeDict.items()}

statusDict = {
    'urgency_status':'Urgent',
    'need_status':'Need',
    'resolution_status':'Relief',
    'issue_status':'Issue',
}

def read_output(p):
    x = pickle.load( open( p, "rb" ) )
    sf_type_pred, sf_type_dict = x[0], x[1]
    sf_type_dict = {v: k for k, v in sf_type_dict.items()}
    return sf_type_pred, sf_type_dict

def json_predictor(pred, docs, X_docs, idx, output_file, names=None, thres=-1, rm_issue=False, filelist=None, use_seg=True):
    # for field in ['urgency_status', 'resolution_status', 'need_status', 'sf_type']:
    #     if field in pred:
    #         print(field + ' output shape', pred[field][0].shape)

    # print('all test docs number: ', len(docs))

    # iterate all testing documents
    jsonOutput = list()
    for t, datum in enumerate(docs):
        catgys = np.where(pred['sf_type'][0][t,:] > thres)[0]
        if catgys.shape[0] > 0:
            for catgy in np.nditer(catgys):
                segOutputDict = dict()
                segOutputDict['Source'] = 'Ruochen'
                # docId
                if filelist is not None and datum['docId'] not in filelist:
                    continue
                segOutputDict['DocumentID'] = datum['docId']
                if use_seg:
                    # Sent ID
                    segOutputDict['SegmentID'] = datum['segId']
                # TextTokenized
                segOutputDict['Text'] = datum['text']
                # Text that in the model
                segOutputDict['TextInVocab'] = ' '.join([idx[word_idx] for word_idx in X_docs[t]])
                # SF type
                sf_type = pred['sf_type'][1][int(catgy)]
                if sf_type == 'all' or sf_type == 'none':
                    continue
                segOutputDict['Type'] = typeDict.get(sf_type, sf_type)
                if segOutputDict['Type'] in ['regimechange', 'terrorism', 'crimeviolence'] and rm_issue:
                    continue
                # TypeConfidence
                segOutputDict['Confidence'] = float(pred['sf_type'][0][t, catgy])
                # Status
                status = dict()
                if segOutputDict['Type'] in ['regimechange', 'terrorism', 'crimeviolence']:
                    field_list = ['issue_status']
                else:
                    field_list = ['urgency_status', 'resolution_status', 'need_status']
                for field in field_list:
                    if field in pred:
                        catgy = np.argmax(pred[field][0][t,:])
                        val = pred[field][1][catgy]
                        if val in ['True', 'False']:
                            val = val == 'True'
                    else:
                        val = DEFAULT_STATUS[field]
                    status[statusDict[field]] = val

                segOutputDict['Status'] = status
                jsonOutput.append(segOutputDict)

    print('size of json output: %d' % len(jsonOutput))
    with codecs.open(output_file, 'w', 'utf-8') as fOut:
        # json.dump(jsonOutput, fOut, ensure_ascii=False, sort_keys=False, indent=4, separators=(',', ': '))
        fOut.write(json.dumps(jsonOutput, ensure_ascii=False, sort_keys=False, indent=4, separators=(',', ': ')))
