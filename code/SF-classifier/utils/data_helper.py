import numpy as np
import pickle
from collections import defaultdict
import sys, re
import pandas as pd
from os import listdir
from os.path import join, isdir, isfile
import xml.etree.ElementTree as ET
import sys
import random
from tqdm import tqdm
import json
import nltk
import time, datetime
from six.moves import cPickle
from unidecode import unidecode

from gensim.models.keyedvectors import KeyedVectors
from keras.preprocessing.sequence import pad_sequences
from keras.models import model_from_json

from .parl_data_helper import toks2idx
from .JSON_predictor import typeRevDict

english_vocab = set(w.lower() for w in nltk.corpus.words.words())

DEBUG = False

def save_obj(obj, fname):
    f = open(fname, 'wb')
    cPickle.dump(obj, f)
    f.close()

def load_obj(fname):
    f = open(fname, 'rb')
    loaded_obj = cPickle.load(f)
    f.close()
    return loaded_obj

def my_pad_sequences(X, maxlen):
    if X is not None:
        return pad_sequences(X, maxlen)
    else:
        return X

def my_save_model(model, f):
    # serialize model to JSON
    model_json = model.to_json()
    with open(f + ".json", "w", encoding='utf-8') as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(f + ".h5")
    # print("Saved model into h5 file!")

def my_load_model(f):
    json_file = open(f + '.json', 'r', encoding='utf-8')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(f + ".h5")
    # print("Loaded model from disk")
    return loaded_model

def get_time_stamp():
    ts = time.time()
    return datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H:%M:%S')

def idx2str(l, idx):
    inv_idx = {v: k for k, v in idx.items()}
    s = [inv_idx[e] for e in l if e > 0]
    return ' '.join(s)

def write_dict(d, f):
    f_out = open(f, 'w')
    for k, v in d.items():
        f_out.write(str(k) + ':' + str(v) + '\n')

def make_vals_int(d):
    d_new = dict()
    for k,v in d.items():
        d_new[k] = int(v)
    return d_new

def read_dict(f):
    d = dict()
    for l in open(f):
        ss = l.strip().split(':')
        d[ss[0]] = ss[1]
    return d

def read_word_idx_map(f):
    d = dict()
    with open(f) as f_in:
        for l in f_in:
            ss = l.strip().split('\t')
            if len(ss) == 1:
                ss.append(' ')
            d[int(ss[0])] = ss[1]
    return d

def rev_dict(my_map):
    inv_map = {v: k for k, v in my_map.items()}
    return inv_map

def build_reliefweb_char_data(data_folder, avoid_all=False, clean_string=False):

    char_indices = defaultdict(float)
    char_index = 0
    catgy_ind = defaultdict(int)
    test_docs = []

    # find all categories
    docDict = dict()
    catgy_idx = 0
    for topic in listdir(data_folder):
        if topic == 'all' and avoid_all:
            continue
        if isdir(join(data_folder, topic)):
            catgy_ind[topic] = catgy_idx
            catgy_idx += 1

            for docName in listdir(join(data_folder, topic)):
                doc = open(join(data_folder, topic, docName), encoding='utf-8').read()
                docId = docName.replace('.txt', '')
                if clean_string:
                    orig_doc = clean_str(doc)
                else:
                    orig_doc = doc

                chars = set(orig_doc.split('~'))

                if not docId in docDict:
                    for char in chars:
                        char_indices[char] += 1
                    datum  = {"text": orig_doc.split('~'),
                              "num_chars": len(orig_doc.split('~')),
                              "catgy": [catgy_ind[topic]],
                              "Id": docId
                              }
                    docDict[docId] = datum
                else:
                    docDict[docId]['catgy'].append(catgy_ind[topic])

    for datum in list(docDict.values()):
        test_docs.append(datum)

    return test_docs, char_indices, catgy_ind

def build_ltf_char_data(DATA_FOLDER):
    """
    build data for RCV1
    """
    char_indices = defaultdict(float)
    char_index = 0
    catgy_ind = defaultdict(int)
    test_docs = []

    # iterate over files
    for fIn in listdir(DATA_FOLDER):
        # print('processing %s' % fIn)
        try:
            tree = ET.parse(join(DATA_FOLDER, fIn))
        except ET.ParseError:
            print('Oops!  That was wrong XML format!')
            continue
        root = tree.getroot()
        # iterate over documents
        for doc in root.findall('DOC'):
            docId = doc.get('id')
            text = doc.find('TEXT')
            for seg in text.findall('SEG'):
                segId = seg.get('id')
                if seg.find('ORIGINAL_TEXT').text is None:
                    continue
                orig_doc = seg.find('ORIGINAL_TEXT').text
                # orig_doc = token_list
                # print orig_doc

                if len(orig_doc.split('~')) < 3:
                    continue
                for char in orig_doc.split('~'):
                    char_indices[char] += 1
                datum  = {"text": orig_doc.split('~'),
                          "num_chars": len(orig_doc.split('~')),
                          "catgy": [0],
                          "docId": docId,
                          "segId": segId
                          }
                test_docs.append(datum)
    # print('char_indices:', char_indices)
    return test_docs, char_indices, None

def to_parseable(tree):
    t = ET.tostring(tree)
    t = t.lower()
    return ET.fromstring(t)

def build_ltf_data(data_folder, clean_string=False, morph=False, use_seg=True,
        bpe_model_path=None):
    """
    build data for LTF
    """

    if bpe_model_path is not None:
        import sentencepiece as spm
        sp = spm.SentencePieceProcessor()
        sp.Load(bpe_model_path)

    vocab = defaultdict(float)
    test_docs = []

    # iterate over files
    for fIn in tqdm(sorted(listdir(data_folder))):
        if isfile(join(data_folder, fIn)):
            # print('processing %s' % fIn)
            pass
        else:
            continue
        try:
            tree = ET.parse(join(data_folder, fIn))
        except ET.ParseError:
            print('Oops!  That was wrong XML format!')
            continue
        root = tree.getroot()
        # root = to_parseable(root)
        # iterate over documents
        for doc in root.findall('DOC'):
            docId = doc.get('id')
            # print('processing doc: %s' % docId)
            text = doc.find('TEXT')
            if not use_seg:
                token_list = list()
            for seg in text.findall('SEG'):
                segId = seg.get('id')
                # print('processing seg: %s' % segId)
                if use_seg:
                    token_list = list()
                for token in seg.findall('TOKEN'):
                    if token.text is None:
                        continue
                    if morph:
                        try:
                            token_list.append(lemma(token.text))
                        except Exception:
                            token_list.append(token.text)
                            # print('parser error:', token.text, token.text)
                    else:
                        # lower case 06/25/2018 [Naoki]
                        # unidecode the token
                        # token_list.append(unidecode(token.text.lower()))
                        token_list.append(token.text.lower())
                # print("token_list", token_list)
                if use_seg:
                    if len(token_list) == 0:
                        token_list = seg.text.split()
                        if len(token_list) == 0:
                            continue
                    orig_doc = ' '.join(token_list)
                    if bpe_model_path is not None:
                        # if use bpe overwrites tokens with bpe
                        token_list = sp.EncodeAsPieces(orig_doc)
                        orig_doc = ' '.join(token_list)

                    words = set(token_list)
                    for word in words:
                        vocab[word] += 1
                    datum  = {"text": orig_doc,
                              "num_words": len(orig_doc.split()),
                              "catgy": [0],  # placeholder
                              "docId": docId,
                              "segId": segId}
                    test_docs.append(datum)

            if not use_seg:
                if len(token_list) == 0:
                    continue
                orig_doc = ' '.join(token_list)

                if bpe_model_path is not None:
                    # if use bpe overwrites tokens with bpe
                    token_list = sp.EncodeAsPieces(orig_doc)
                    orig_doc = ' '.join(token_list)

                words = set(token_list)
                for word in words:
                    vocab[word] += 1
                datum  = {"text": orig_doc,
                          "num_words": len(orig_doc.split()),
                          "catgy": [0],  # placeholder
                          "docId": docId}
                test_docs.append(datum)

    return test_docs, vocab

def build_reliefweb_data(data_folder, avoid_all=False, clean_string=False):
    """
    build data for RCV1
    """
    vocab = defaultdict(float)
    catgy_ind = defaultdict(int)

    # find all categories
    docDict = dict()
    catgy_idx = 0
    for topic in tqdm(listdir(data_folder)):
        if topic == 'all' and avoid_all:
            continue
        if isdir(join(data_folder,topic)):
            # print(topic)
            catgy_ind[topic] = catgy_idx
            catgy_idx += 1

            for docName in listdir(join(data_folder, topic)):
                # print 'processing %s...' % (join(data_folder, topic, docName))
                doc = open(join(data_folder, topic, docName), encoding='utf-8').read()
                docId = docName.replace('.txt', '')
                if clean_string:
                    orig_doc = clean_str(doc)
                else:
                    orig_doc = doc

                # Lower case 06/25/2018 [Naoki]
                orig_doc = orig_doc.lower()

                words = set(orig_doc.split())

                if not docId in docDict:
                    for word in words:
                        vocab[word] += 1
                    datum  = {"text": orig_doc,
                              "num_words": len(orig_doc.split()),
                              "catgy": [catgy_ind[topic]],
                              "Id": docId}
                    docDict[docId] = datum
                else:
                    docDict[docId]['catgy'].append(catgy_ind[topic])
    return list(docDict.values()), vocab, catgy_ind


def read_labeled_docs(data_folder,
                      vocab=defaultdict(int),
                      catgy_ind=None,
                      clean_string=False, avoid_all=False,
                      debug=False):
    """Read labeled documents.

    <data_folder>/<category>/<document>
    """
    if catgy_ind is None:
        catgy_ind = defaultdict(lambda: len(catgy_ind))
    docs = {}
    for topic in tqdm(sorted(listdir(data_folder))):
        if topic == 'all' and avoid_all:
            continue
        if isdir(join(data_folder,topic)):
            catgy_idx = catgy_ind[topic]  # register topic to an indexer
            for i, docName in enumerate(listdir(join(data_folder, topic))):
                if i > 1000 and debug:
                    break
                if not isfile(join(data_folder, topic, docName)):
                    continue
                doc = open(join(data_folder, topic, docName), encoding='utf-8').read()
                did = docName.replace('.txt', '')  # document ID

                if did in docs:
                    docs[did]['catgy'].append(catgy_idx)
                    continue

                # Clean a document if specified
                orig_doc = clean_str(doc) if clean_string else doc
                # Lower case 06/25/2018 [Naoki]
                orig_doc = orig_doc.lower()

                # Count df
                for word in set(orig_doc.split()):
                    vocab[word] += 1

                item  = {"text": orig_doc,
                         "num_words": len(orig_doc.split()),
                         "catgy": [catgy_idx],
                         "Id": did}
                docs[did] = item
    return list(docs.values()), vocab, catgy_ind

def doc2idx(docs, word_idx_map, maxlen=400, num_clas=2, eng_nb_words=0):
    y = np.zeros((len(docs), num_clas))
    X = []
    truncate_num = 0
    for t, datum in enumerate(docs):
        for k in datum['catgy']:
            y[t, k] = 1
        x = []
        for i, w in enumerate(datum['text'].split()):
            if i >= maxlen:
                truncate_num += 1
                break
            if w in word_idx_map:
                x.append(word_idx_map[w] + eng_nb_words)
            else:
                x.append(0)  # UNK
        X.append(x)
    # print("truncate %d docs" % truncate_num)
    return X, y

def doc2idx_comb(docs, tgt_idx_map, eng_word_idx, eng_nb_words=0, maxlen=400, num_clas=2,
                 eng_SN_only=False, english_vocab=None):
    y = np.zeros((len(docs), num_clas))
    X = []
    truncate_num = 0
    for t, datum in enumerate(docs):
        for k in datum['catgy']:
            y[t,k] = 1
        x = []
        for i, w in enumerate(datum['text'].split()):
            if i >= maxlen:
                truncate_num += 1
                break
            if (w.lower() in english_vocab and w.lower() in eng_word_idx) and (not eng_SN_only or '_SN_' in datum['docId']):
                x.append(eng_word_idx[w.lower()])
            elif w in tgt_idx_map:
                x.append(tgt_idx_map[w] + eng_nb_words)
            else:
                x.append(0)
        X.append(x)
    print("truncate %d docs" % truncate_num)
    return X, y

def get_W(word_vecs, k=300):
    """
    Get word matrix. W[i] is the vector for word indexed by i
    """
    vocab_size = len(word_vecs)
    word_idx_map = dict()
    W = np.zeros(shape=(vocab_size+1, k), dtype='float32')
    W[0] = np.zeros(k, dtype='float32')
    i = 1
    for word in sorted(word_vecs.keys()):
        W[i] = word_vecs[word]
        word_idx_map[word] = i
        i += 1
    return W, word_idx_map

# def load_text_vec(fname, vocab):
#     """
#     Loads dx1 word vecs from Google (Mikolov) word2vec
#     """
#     word_vecs = {}
#     with open(fname, "r") as f:

#         for i, line in enumerate(f):
#             if i == 0:
#                 vocab_size, layer1_size = map(int, line.split())
#                 print "reading word2vec at vocab_size:%d, dimension:%d" % (vocab_size, layer1_size)
#             else:
#                 ss = line.split(' ')
#                 word = ss[0]
#                 if word in vocab:
#                     word_vecs[word] = np.fromstring(' '.join(ss[1:]), dtype='float32', count = layer1_size, sep = ' ')

#     return vocab_size, word_vecs, layer1_size

def gen_class_weight(y):
    # print 'y.shape', y.shape
    y_sum = y.sum(axis=0)
    # print 'y_sum.shape', y_sum.shape
    y_weight = 1. / y_sum
    y_weight[np.isinf(y_weight)] = .0
    # print 'y_weight.shape', y_weight.shape
    y_weight = y_weight / (y_weight.sum())
    # print 'y_weight.shape', y_weight.shape
    res = dict()
    for i in range(y_weight.shape[0]):
        res[i] = float(y_weight[i])
    return res

def file_len(fname):
    return sum(1 for line in open(fname, encoding='utf-8'))

def write_word_idx_map(word_idx_map, f):
    with open(f, 'w') as f_out:
        for k in word_idx_map:
            f_out.write("{}\t{}\n".format(k, word_idx_map[k]))

def load_text_vec(fname, vocab, splitter=' ', ext_num=0, logger=None):
    """
    Loads dx1 word vecs from Google (Mikolov) word2vec
    """
    word_vecs = {}
    ext_count = 0
    vocab_size, layer1_size = None, None
    with open(fname, "r", encoding='utf-8') as f:
        for line in tqdm(f, total=vocab_size):
            ss = line.split(' ')
            if len(ss) <= 3:
                vocab_size, layer1_size = map(int, ss)
                continue
            word = ss[0]
            dims = ' '.join(ss[1:]).strip().split(splitter)
            if layer1_size is None:
                layer1_size = len(ss) - 1
            if word in vocab:
                word_vecs[word] = np.fromstring(' '.join(dims), dtype='float32', count=layer1_size, sep=' ')
            elif ext_count < ext_num: # add this word to vocabulary
                ext_count += 1
                word_vecs[word] = np.fromstring(' '.join(dims), dtype='float32', count=layer1_size, sep=' ')

    if vocab_size is None:
        vocab_size = len(word_vecs)

    logger.info("read word2vec at vocab_size:%d, dimension:%d" % (vocab_size, layer1_size))
    return vocab_size, word_vecs, layer1_size


def add_unknown_words(word_vecs, vocab, min_df=1, k=300):
    """
    For words that occur in at least min_df documents, create a separate word vector.
    0.25 is chosen so the unknown vectors have (approximately) same variance as pre-trained ones
    """
    for word in vocab:
        if word not in word_vecs and vocab[word] >= min_df:
            # word_vecs[word] = np.random.uniform(-0.25,0.25,k)
            word_vecs[word] = np.zeros(k)    # TODO: how about using the mean vector?

def clean_str(string, TREC=False):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Every dataset is lower cased except for TREC
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip() if TREC else string.strip()

def truncate_vocab(vocab, nb_words):
    new_vocab = defaultdict(float)
    i = 0
    for w in sorted(vocab, key=vocab.get, reverse=True):
        new_vocab[w] = vocab[w]
        i+=1
        if i >= nb_words:
            break
    return new_vocab

def truncate_chars(vocab, nb_words, min_df=1):
    new_vocab = defaultdict(float)
    i = 0
    for w in sorted(vocab, key=vocab.get, reverse=True):
        if vocab[w] < min_df:
            break
        new_vocab[w] = vocab[w]
        i+=1
        if i >= nb_words:
            break
    return new_vocab

def read_reliefweb_char(data_folder, char_idx=None, avoid_all=False, maxlen=1400, nb_chars=400):
    print("loading data...", end=' ')
    all_docs, char_indices, catgy_ind = build_reliefweb_char_data(data_folder, avoid_all=avoid_all, clean_string=False)
    all_vocab = char_indices
    max_l = np.max(pd.DataFrame(all_docs)["num_chars"])
    mean_l = np.mean(pd.DataFrame(all_docs)["num_chars"])
    print("before truncate, number of chars: " + str(len(char_indices)))
    char_indices = dict((c, i+1) for i, c in enumerate(char_indices.keys()))
    print('char_indices: ', char_indices)
    if char_idx is None:
        char_indices = truncate_chars(char_indices, nb_chars, 0)
        char_indices = dict((c, i+1) for i, c in enumerate(char_indices.keys()))
    else:
        char_indices = char_idx
    print(('char_indices:', char_indices))
    nb_chars = len(char_indices)
    print("data loaded!")
    print("number of testing documents: " + str(len(all_docs)))
    print("max sentence length in chars: " + str(max_l))
    print("avg sentence length in chars: " + str(mean_l))
    print("number of chars: " + str(nb_chars))
    print((len(all_docs), maxlen))
    X_all = np.zeros((len(all_docs), maxlen), dtype=np.int32)
    y_all = np.zeros((len(all_docs), len(catgy_ind)), dtype=np.int32)
    for i, doc in enumerate(all_docs):
        doc_str = ''
        for t, char in enumerate(doc['text'][-maxlen:]):
            if char in char_indices:
                X_all[i, t] = char_indices[char]
                doc_str += char + ' '
            else:
                print(('unknown char:', char))
                doc_str += 'UNK' + ' '

        for catgy in doc['catgy']:
            y_all[i,catgy] = 1
        if i % 1000 == 0:
            print('sample document', doc_str)

    print("shape of val and test data", X_all.shape)
    return X_all, y_all, None, len(char_indices)+1, maxlen, catgy_ind, char_indices

def read_ltf_char(data_folder, char_idx=None, maxlen=1400, nb_chars=400):
    print("loading data...", end=' ')
    all_docs, char_indices, catgy_ind = build_ltf_char_data(data_folder)
    all_vocab = char_indices
    max_l = np.max(pd.DataFrame(all_docs)["num_chars"])
    mean_l = np.mean(pd.DataFrame(all_docs)["num_chars"])

    if char_idx is None:
        print("before truncate, number of chars: " + str(len(char_indices)))
        char_indices = truncate_chars(char_indices, nb_chars, 0)
        char_indices = dict((c, i+1) for i, c in enumerate(char_indices.keys()))
        nb_chars = len(char_indices)
    else:
        char_indices = char_idx
    print("data loaded!")
    print("number of testing documents: " + str(len(all_docs)))
    print("max sentence length in chars: " + str(max_l))
    print("avg sentence length in chars: " + str(mean_l))
    print("number of chars: " + str(nb_chars))
    print((len(all_docs), maxlen))
    X_all = np.zeros((len(all_docs), maxlen), dtype=np.int32)
    for i, doc in enumerate(all_docs):
        doc_str = ''
        for t, char in enumerate(doc['text'][-maxlen:]):
            if char in char_indices:
                X_all[i, t] = char_indices[char]
                doc_str += char + ' '
            else:
                print(('unknown char:', char))
                doc_str += 'UNK' + ' '
        if i % 1000 == 0:
            print('sample document', doc_str)

    print("shape of val and test data", X_all.shape)
    return X_all, None, None, maxlen, all_docs, char_indices, all_vocab

def combine_word_index(src_idx, tgt_idx, src_nb_words=0):
    new_idx = dict()
    if src_idx is not None:
        for w in src_idx:
            # if w in english_vocab:
            #     new_idx[w] = src_idx[w]
            new_idx[w] = src_idx[w]
    for w in tgt_idx:
        if w not in new_idx:
            new_idx[w] = tgt_idx[w] + src_nb_words
        else:
            new_idx[w+'_IL'] = tgt_idx[w] + src_nb_words
    return new_idx


def read_ltf(data_folder, w2v_file, maxlen=100, nb_words=20000, nb_words_ext=20000,
             seg=True, share_vocab=False, include_eng=False, eng_SN_only=False,
             eng_word_idx=None, eng_nb_words=0, tgt_word_idx=None,
             bpe_model_path=None, logger=None):
    logger.info("loading data from {}".format(data_folder))
    all_docs, vocab = build_ltf_data(data_folder, clean_string=False,
        use_seg=seg, bpe_model_path=bpe_model_path)
    all_vocab = vocab
    logger.info("length of all ltf docs: {}".format(len(all_docs)))
    max_l = np.max(pd.DataFrame(all_docs)["num_words"])
    mean_l = np.mean(pd.DataFrame(all_docs)["num_words"])
    # num_clas = np.max(catgy_ind.values()) + 1
    vocab = truncate_vocab(vocab, nb_words)
    # logger.info vocab
    logger.info("data loaded!")
    logger.info("number of testing documents: " + str(len(all_docs)))
    logger.info("vocab size: " + str(len(vocab)))
    logger.info("max sentence length: " + str(max_l))
    logger.info("mean sentence length: " + str(mean_l))

    if share_vocab:  # 'simple' mode for English
        X_all, _ = doc2idx(all_docs, eng_word_idx, maxlen=maxlen)
        logger.info("dataset created!")
        return X_all, all_docs

    logger.info("loading word2vec vectors...")
    _, w2v, layer1_size = load_text_vec(w2v_file, vocab, ext_num=nb_words_ext, logger=logger)
    logger.info("word2vec loaded!")
    logger.info("num words already in word2vec: " + str(len(w2v)))

    add_unknown_words(w2v, vocab, k=layer1_size)
    W, word_idx_map = get_W(w2v, k=layer1_size)

    if tgt_word_idx is None:

        if include_eng:
            X_all, _ = doc2idx_comb(all_docs, tgt_idx_map=word_idx_map, eng_word_idx=eng_word_idx, eng_nb_words=eng_nb_words, maxlen=maxlen, eng_SN_only=eng_SN_only, english_vocab=english_vocab)
        else:
            X_all, _ = doc2idx(all_docs, word_idx_map, maxlen=maxlen, eng_nb_words=eng_nb_words)

        word_idx_map = combine_word_index(eng_word_idx, word_idx_map, eng_nb_words)
    else:
        word_idx_map = tgt_word_idx
        X_all, _ = doc2idx(all_docs, word_idx_map, maxlen=maxlen, eng_nb_words=0)

    # X_val, y_val = doc2idx(val_docs, word_idx_map, maxlen, num_clas)
    # X_test, y_test = doc2idx(test_docs, word_idx_map, maxlen, num_clas)

#    rand_vecs = {}
#    add_unknown_words(rand_vecs, vocab)
#    W2, _ = get_W(rand_vecs)
    logger.info("X_all length: {}".format(len(X_all)))

    # cPickle.dump([X_all, None, None, X_all, None, W, nb_words+1, maxlen, catgy_ind, all_docs], open("chinese_setE_RidgeReg_dim250.p", "wb"))
    logger.info("dataset created!")
    return X_all, W, W.shape[0], maxlen, all_docs, word_idx_map, all_vocab


def read_reliefweb(data_folder, w2v_file, avoid_all=False, maxlen=100, nb_words=20000, logger=None):
    logger.info("loading data...")
    logger.info("data_folder:" + data_folder)
    all_docs, vocab, catgy_ind = build_reliefweb_data(data_folder, avoid_all=avoid_all, clean_string=False)
    logger.info("Data count:{}".format(len(all_docs)))
    max_l = np.max(pd.DataFrame(all_docs)["num_words"])
    mean_l = np.mean(pd.DataFrame(all_docs)["num_words"])
    num_clas = np.max(list(catgy_ind.values())) + 1

    vocab = truncate_vocab(vocab, nb_words)
    logger.info("data loaded!")
    logger.info("number of testing documents: " + str(len(all_docs)))
    logger.info("vocab size: " + str(len(vocab)))
    logger.info("max sentence length: " + str(max_l))
    logger.info("mean sentence length: " + str(mean_l))
    logger.info("number of classes: " + str(num_clas))

    _, w2v, layer1_size = load_text_vec(w2v_file, vocab, ext_num=nb_words, logger=logger)
    logger.info("word2vec loaded!")
    logger.info("num words already in word2vec: " + str(len(w2v)))

    add_unknown_words(w2v, vocab, k=layer1_size)
    W, word_idx_map = get_W(w2v, k=layer1_size)

    random.shuffle(all_docs)
    X_train, y_train = doc2idx(all_docs, word_idx_map, maxlen, num_clas)

#    rand_vecs = {}
#    add_unknown_words(rand_vecs, vocab)
#    W2, _ = get_W(rand_vecs)

    logger.info("X_train length: {}".format(len(X_train)))

    logger.info("dataset created!")
    return X_train, y_train, W, W.shape[0], maxlen, catgy_ind, word_idx_map


def read_eng_corpora(data_folders, w2v_file, maxlen=100, vocabsize=20000, logger=None, debug=DEBUG):
    """Read English corpora."""
    if type(data_folders) is not list:
        data_folders = [data_folders]
    logger.info("loading data...")

    all_docs = []
    for i, data_folder in enumerate(data_folders):
        logger.info("data_folder: " + data_folder)
        if i == 0:  # the first dataset
            docs, vocab, catgy_ind = read_labeled_docs(data_folder, debug=debug)
        else:
            docs, vocab, catgy_ind = read_labeled_docs(
                data_folder, vocab=vocab, catgy_ind=catgy_ind, clean_string=False, debug=debug)
        all_docs += docs
    logger.info("Data count:{}".format(len(all_docs)))
    max_l = np.max(pd.DataFrame(all_docs)["num_words"])
    mean_l = np.mean(pd.DataFrame(all_docs)["num_words"])
    num_clas = np.max(list(catgy_ind.values())) + 1

    vocab = truncate_vocab(vocab, vocabsize)
    logger.info("data loaded!")
    logger.info("number of testing documents: " + str(len(all_docs)))
    logger.info("vocab size: " + str(len(vocab)))
    logger.info("max sentence length: " + str(max_l))
    logger.info("mean sentence length: " + str(mean_l))
    logger.info("number of classes: " + str(num_clas))

    _, w2v, layer1_size = load_text_vec(w2v_file, vocab, ext_num=vocabsize, logger=logger)
    logger.info("word2vec loaded!")
    logger.info("num words already in word2vec: " + str(len(w2v)))

    add_unknown_words(w2v, vocab, k=layer1_size)
    W, word_idx_map = get_W(w2v, k=layer1_size)

    random.shuffle(all_docs)
    X_train, y_train = doc2idx(all_docs, word_idx_map, maxlen, num_clas)

#    rand_vecs = {}
#    add_unknown_words(rand_vecs, vocab)
#    W2, _ = get_W(rand_vecs)

    logger.info("X_train length: {}".format(len(X_train)))

    logger.info("dataset created!")
    return X_train, y_train, W, W.shape[0], maxlen, catgy_ind, word_idx_map


def read_json_annot(train_path, word_idx_map, catgy_ind, maxlen=100,
        include_neg=True, bpe_model_path=None):

    if bpe_model_path is not None:
        import sentencepiece as spm
        sp = spm.SentencePieceProcessor()
        sp.Load(bpe_model_path)

    json_obj = json.load(open(train_path, encoding='utf-8'))
    annot_json_obj = [sf for sf in json_obj if "TypeList" in sf]
    X, y = dict(), dict()
    for cat in catgy_ind:
        X[cat] = list()
        y[cat] = list()

    for i,annot in enumerate(annot_json_obj):
        if bpe_model_path is not None:
            token_list = sp.EncodeAsPieces(annot['Text'])
        else:
            token_list = annot['Text'].split()
        # True Positive
        for cat in annot["TypeList"]:
            # cat = typeRevDict[cat]
            X[cat].append(toks2idx(token_list, word_idx_map, maxlen))
            y[cat].append(1)

        # to be compatible with older format of NI annotation
        if "NegativeTypeList" in annot and include_neg:

            # ignore if both NegativeTypeList and TypeList is empty
            if len(annot["NegativeTypeList"]) == 0 and len(annot["TypeList"]) == 0:
                continue

            for cat in annot["NegativeTypeList"]:
                # cat = typeRevDict[cat]
                X[cat].append(toks2idx(token_list, word_idx_map, maxlen))
                y[cat].append(0)

    for cat in catgy_ind:
        X[cat] = np.asarray(pad_sequences(X[cat], maxlen=maxlen), dtype=np.int32)
        y[cat] = np.asarray(y[cat], dtype=np.int32)

    return X, y

# def read_json_annot(train_path, word_idx_map, catgy_ind, maxlen=100):
#     print('reading tgt annotation:', train_path)
#     json_obj = json.load(open(train_path))
#     X = []
#     y = np.zeros((len(json_obj), len(catgy_ind)))
#     for i,annot in enumerate(json_obj):
#         X.append(toks2idx(annot['Text'].split(), word_idx_map, maxlen))
#         for sf_type in annot['TypeList']:
#             # y[i, catgy_ind[typeRevDict[sf_type]]] = 1
#             y[i, catgy_ind[sf_type]] = 1
#         if len(annot['TypeList']) == 0:
#             y[i, catgy_ind['none']] = 1
#         # print('annot[TypeList]:', annot['TypeList'])
#         # print('y[i,:]', y[i,:])
#     X = np.asarray(pad_sequences(X, maxlen=maxlen), dtype=np.int32)
#
#     return X, y

def select_conf_idx(y_pred, catgy_idx, thres_conf, X_src_parl, X_tgt_parl, src_idx_word, tgt_idx_word, logger=False):
    conf_idx = []
    print("Selecting confident instances from parallel data...")
    if logger:
        logger.info("Selecting confident instances from parallel data...")
    for catgy in catgy_idx:
        if catgy == 'all':
            continue
        thres = thres_conf[catgy_idx[catgy]]
        # thres = 0.5
        y_pred_slice = y_pred[:,catgy_idx[catgy]]
        idx_slice = np.squeeze(np.where(y_pred_slice>thres)).tolist()
        if isinstance(idx_slice, int):
            idx_slice = [idx_slice]
        # print(catgy, np.sum((y_pred_slice>thres).astype(float)))
        # print("idx_slice:", idx_slice)
        conf_idx += idx_slice
        for idx in idx_slice:
            src_toks = [src_idx_word[i] for i in X_src_parl[idx,:].tolist()]
            tgt_toks = [tgt_idx_word[i] for i in X_tgt_parl[idx,:].tolist()]
            print(y_pred_slice[idx], ' '.join(src_toks), ' ||| ', ' '.join(tgt_toks))
            logger.info('confidence: {}, {} ||| {}'.format(y_pred_slice[idx], ' '.join(src_toks), ' '.join(tgt_toks)))
    y_pred = (y_pred>thres_conf).astype(float)
    y_pred[:,catgy_idx['all']] == 0.0
    return conf_idx, y_pred
