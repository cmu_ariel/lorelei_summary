# -*- coding: utf-8 -*-
"""
06 May 2019
Generate NN config files
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import json
from collections import OrderedDict
import os

lang_code_map = {'Akan':        ('AKA',  None),
                 'Amharic':     ('AMH',  None),
                 'Arabic':      ('ARA',  None),
                 'Bengali':     ('BEN',  None),
                 'Farsi':       ('FAS',  None),
                 'Hausa':       ('HAU',  None),
                 'Hindi':       ('HIN',  None),
                 'Hungarian':   ('HUN',  None),
                 'Indonesian':  ('IND',  None),
                 'Mandarin':    ('CMN',  None),
                 'Russian':     ('RUS',  None),
                 'Somali':      ('SOM',  None),
                 'Spanish':     ('SPA',  None),
                 'Swahili':     ('SWA',  None),
                 'Tamil':       ('TAM',  None),
                 'Tagalog':     ('TGL',  None),
                 'Thai':        ('THA',  None),
                 'Turkish':     ('TUR',  None),
                 'Ukrainian':   ('UKR',  None),
                 'Urdu':        ('URD',  None),
                 'Uzbek':       ('UZB',  None),
                 'Vietnamese':  ('VIE',  None),
                 'Wolof':       ('WOL',  None),
                 'Yoruba':      ('YOR',  None),
                 'Zulu':        ('ZUL',  None),
                 'Uyghur':      ('UIG', 'IL3'),
                 'Tigrinya':    ('TIR', 'IL5'),
                 'Oromo':       ('ORM', 'IL6'),
                 'Kinyarwanda': ('KIN', 'IL9'),
                 'Sinhala':     ('SIN', 'IL10'),
                 'IL3':         ('UIG', 'IL3'),
                 'IL5':         ('TIR', 'IL5'),
                 'IL6':         ('ORM', 'IL6'),
                 'IL6_orig':    ('ORM', 'IL6'),
                 'IL9':         ('KIN', 'IL9'),
                 'IL10':        ('SIN', 'IL10'),
                }

config_base = \
        {
            "eng_train_path": {
                "sf_type":  [
                    "/usr3/data/notani/ariel-sf/sf_corpora/ReliefWeb/LOR17.fix.v2",
                    "/usr3/data/notani/ariel-sf/sf_corpora/ReliefWeb/LOR17-SFType_by_Nidhi_KWD_thres0.1_reduce.fix.v2",
                    "/usr3/data/notani/ariel-sf/sf_corpora/LRLP/sent.v1"
                ]
            },
            "eng_wordembedding_path": None,
            "tgt_test_path": None,
            "tgt_wordembedding_path": None,
            "tgt_char_model": False,
            "batch_size": 128,
            "lr": 1e-4,
            "nb_epoch": 5,
            "label_type": "multi-label",
            "adv_hp":"/usr2/data/ruochenx/LOREHLT18/RealRun/script_PostEval/config/CP_grl/adv.v1.json",
            "alpha": 1,
            "thres": -1,
            "hidden_dim": 100,
            "hyper_params": [
                "/usr2/data/ruochenx/LOREHLT18/RealRun/script_PostEval/config/CNN/kimCNN.v3.hd.json",
                "/usr2/data/ruochenx/LOREHLT18/RealRun/script_PostEval/config/CNN/kimCNN.v3.hd.json"
            ],
            "tgt_train_path": {"sf_type":[]}
        }
key_order = ['eng_train_path',
             'tgt_train_path',
             'tgt_test_path',
             'eng_wordembedding_path',
             'tgt_wordembedding_path',
             'batch_size',
             'lr',
             'nb_epoch',
             'label_type',
             'adv_hp',
             'alpha',
             'thres',
             'hidden_dim',
             'hyper_params',
            ]

def main(args=None):
    parser = ArgumentParser(description='Generate NN config files')
    parser.add_argument('--template_path', default='nn_config/template.json',
                        help='The basic template of NN config')
    parser.add_argument('--outdir', default='nn_config',
                        help='The directory to print the config files')
    parser.add_argument('--datadir', default='/usr2/home/amuis/work/lorelei_summary/data/langpacks',
                        help='The root directory containing the data')
    parser.add_argument('--test_data', default='core_data',
                        help='The test data')
    args = parser.parse_args(args)
    for setting in ['core', 'set1_core', 'set12_core', 'set123_core']:
        os.makedirs('{}/{}'.format(args.outdir, setting), exist_ok=True)
        for language, (lang_code, il_code) in lang_code_map.items():
            if il_code is not None:
                # Ignore ILs, they are not in the language packs
                continue
            config = OrderedDict()
            for key in key_order:
                config[key] = config_base[key]
            config['eng_wordembedding_path'] = '{}/{}/word_embeddings/{}/xlingual.eng.emb.txt'.format(
                                                    args.datadir, lang_code, setting)
            config['tgt_wordembedding_path'] = '{}/{}/word_embeddings/{}/xlingual.{}.emb.txt'.format(
                                                    args.datadir, lang_code, setting, lang_code.lower())
            config['tgt_test_path'] = '{}/{}/{}/IL/ltf'.format(args.datadir, lang_code, args.test_data)
            config['tgt_train_path']['sf_type'] = ['{}/{}/setE/IL/sf_anno/gold_sf_IL_as_NI.json'.format(
                                                    args.datadir, lang_code)]

            outpath = '{}/{}/{}_nn_config.json'.format(args.outdir, setting, lang_code)
            with open(outpath, 'w') as outfile:
                json.dump(config, outfile, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    main()

