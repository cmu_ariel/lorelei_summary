# 03 Jun 2019
# By: Aldrian Obaja Muis
# To check the raw texts, whether they contain malformed words, like "floodingcyclone"

basedir=/usr3/data/notani/ariel-sf/sf_corpora/ReliefWeb/LOR17.fix.v2
for dir in $(ls -1 ${basedir}); do
    for file in $(ls -1 ${basedir}/${dir}); do
        cat ${basedir}/${dir}/${file};
    done
done \
    | grep "offood" \
    | less
