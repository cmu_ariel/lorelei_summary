# -*- coding: utf-8 -*-
from __future__ import division, print_function
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
import argparse
from collections import OrderedDict, defaultdict
import glob, os, codecs, json
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.WARNING)
from nltk.stem import WordNetLemmatizer
wnl = WordNetLemmatizer()
from nltk.tokenize import wordpunct_tokenize, sent_tokenize
import tornado.ioloop
from tornado.web import RequestHandler, Application
from tornado.web import HTTPError
from lor_utils import SituationFrame as SF, Version, Document
from lor_utils import SF_TYPES, read_ltf_files
import os
import regex as re
from tqdm import tqdm
from unidecode import unidecode
import unicodedata

TONES_COMBINING_CATEGORIES = set([214, 216, 228, 230, 232, 234])

def remove_accents(input_str, char_class_removed='none'):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    if char_class_removed == 'tones':
        nfkd_form = u"".join([c for c in nfkd_form if unicodedata.combining(c) not in TONES_COMBINING_CATEGORIES])
    elif char_class_removed == 'combining':
        nfkd_form = u"".join([c for c in nfkd_form if not unicodedata.combining(c)])
    else:
        pass
    return unicodedata.normalize('NFKC', nfkd_form)

def span_tokenize(text):
    tokens = wordpunct_tokenize(text)
    spans = {}
    end = 0
    for token in tokens:
        start = text.find(token, end)
        end = start + len(token)
        spans[(start, end)] = token
    return spans

class KW_READ_STRATEGY(object):
    """When reading English keywords, there could be multiple occurrences of the same word, with possibly different
    confidence scores. This read strategy determines whether we take the max confidence score or simply the last one
    """
    TAKE_MAX = 'take_max'
    TAKE_LAST = 'take_last'

class KW_SF_Classifier(object):
    def __init__(self, lang, kw_dict=None, kw_dict_path=None, bi_dict=None, bi_dict_path=None,
                 top_k=2, cut_off_sim=0.8, max_ngram=3,
                 ni_dict=None, ni_dict_path=None,
                 ignore_list=None, ignore_list_path=None,
                 include_list=None, include_list_path=None,
                 kw_read_strategy=KW_READ_STRATEGY.TAKE_MAX,
                 remove_char_class='none', do_print=True):
        if do_print:
            print('Reading keywords from {} (or given)'.format(kw_dict_path))
        self.kw_read_strategy = kw_read_strategy
        self.eng_keywords = KW_SF_Classifier.get_keywords(kw_dict, kw_dict_path, kw_read_strategy=kw_read_strategy)
        if do_print:
            print('Reading dictionaries from {} and {} (or given)'.format(bi_dict_path, ni_dict_path))
        self.bi_dict = KW_SF_Classifier.get_dict(the_dict=bi_dict, the_dict_path=bi_dict_path)
        self.ni_dict = KW_SF_Classifier.get_dict(the_dict=ni_dict, the_dict_path=ni_dict_path)
        self.top_k = top_k
        self.cut_off_sim = cut_off_sim
        self.max_ngram = max_ngram
        self.remove_char_class = remove_char_class
        if do_print:
            print('Reading ignore list from {} (or given)'.format(ignore_list_path))
        self.ignore_list = set(KW_SF_Classifier.get_list(ignore_list, ignore_list_path,
                                                         remove_char_class=self.remove_char_class))
        self.include_list = set(KW_SF_Classifier.get_list(include_list, include_list_path, sep='\t',
                                                          remove_char_class=self.remove_char_class))
        if do_print:
            print('Translating keywords')
        self.il_keywords, self.not_translated = KW_SF_Classifier.translate_keywords(self.eng_keywords,
                                                                                    self.bi_dict,
                                                                                    self.ni_dict,
                                                                                    remove_char_class=self.remove_char_class)

    @classmethod
    def from_config(cls, config=None, config_path=None):
        if config is None and config_path is None:
            raise ValueError('Either config or config_path must be specified')
        if config is not None:
            return KW_SF_Classifier(**config)
        with open(config_path, 'r') as infile:
            config = json.load(infile)
        return KW_SF_Classifier(**config)

    def print_keywords(self, path):
        if os.path.isdir(path):
            sf_types = {}
            for keyword, sf_conf in self.il_keywords.items():
                for sf_type, (eng_keyword, confidence) in sf_conf.items():
                    if sf_type not in sf_types:
                        sf_types[sf_type] = []
                    sf_types[sf_type].append((keyword, confidence, eng_keyword))
            for sf_type, keywords in sf_types.items():
                with open('{}/{}.txt'.format(path, sf_type), 'w') as outfile:
                    for (keyword, confidence, eng_keyword) in keywords:
                        outfile.write('{}\t{}\t{}\t{}\n'.format(keyword, sf_type, confidence, eng_keyword))
        else:
            with open(path, 'w') as outfile:
                for keyword, sf_conf in self.il_keywords.items():
                    for sf_type, (eng_keyword, confidence) in sf_conf.items():
                        outfile.write('{}\t{}\t{}\t{}\n'.format(keyword, sf_type, confidence, eng_keyword))
                        # outfile.write('{}\t{}\t{}\n'.format(keyword, sf_type, confidence))

    def get_frames_ordered(self, tokens):
        frames = dict()   # key - SF type and value= ([keyword], confidence, [span], [eng_keyword])
        killed = False
        # Remove ignored IL keywords
        il_keywords_to_sf_type = {k:v for k,v in self.il_keywords.items() if k not in self.ignore_list}
        # Add included IL keywords with their corresponding SF types
        for il_keyword, sf_type in self.include_list:
            if il_keyword in il_keywords_to_sf_type:
                if sf_type not in il_keywords_to_sf_type[il_keyword]:
                    il_keywords_to_sf_type[il_keyword][sf_type] = ('', 1)
        token_list = list(tokens.items())
        debug = False
        for n_toks in range(1, self.max_ngram+1):
            for start_idx in range(len(token_list)-n_toks+1):
                spans, toks = list(zip(*token_list[start_idx:start_idx+n_toks]))
                span = (spans[0][0], spans[-1][1])
                text = ' '.join(toks).lower()
                if text in il_keywords_to_sf_type:
                    if not killed:
                        for sf_type in il_keywords_to_sf_type[text]: # bomb can be mulitple SF types so get score for each
                            eng_keyword, confidence = il_keywords_to_sf_type[text][sf_type]
                            if confidence < self.cut_off_sim:
                                continue
                            if sf_type not in frames:
                                frames[sf_type] = ([text], confidence, [span], [eng_keyword])
                            else:
                                frames[sf_type] = (frames[sf_type][0]+[text], frames[sf_type][1]+confidence, frames[sf_type][2]+[span], frames[sf_type][3]+[eng_keyword])
        sorted_frames =  sorted(frames.items(), key=lambda x:(x[1][1], bool(x[0] in ['crimeviolence', 'terrorism', 'regimechange'])), reverse=True)
        rank = 1
        sorted_frames_updated = []
        for k in sorted_frames:
            sorted_frames_updated.append((k[0], k[1][0], k[1][1], 1.0/rank, k[1][2], k[1][3])) # sf_type, il_keyword, confidence, rank_confidence, span, eng_keyword
            rank += 1
        return sorted_frames_updated[:self.top_k]

    def predict(self, text, doc_id=None, output_object=False):
        """Predict the SF types given the text or Document object

        Args:
            text: The input text. This can be a str or a Document object
            doc_id: The document ID to be included in the SFs predicted from the given text
            output_object: If True, output SituationFrame object instead of dict

        Returns:
            sfs: List of situation frames
        """
        sfs = []
        def frames_to_sfs(frames, doc_id, seg_id, text):
            sfs = []
            for f in frames:
                if f[2]<0:
                    continue
                sf = OrderedDict()
                sf["DocumentID"] = doc_id
                sf["Type"] = f[0]
                sf["Text"] = text
                sf["Justification_ID"] = seg_id
                sf["TypeConfidence"] = f[2]/100
                sf["RankConfidence"] = f[3]
                sf["Source"] = "Nidhi"
                sf['Keyword'] = []
                for keyword, eng_keyword, (start, end) in zip(f[1], f[5], f[4]):
                    sf['Keyword'].append({'start_char': start,
                                          'end_char': end,
                                          'text': keyword,
                                          'source_keyword': eng_keyword,
                                          'weight': f[2]
                                         })
                # if (f[0]=='regimechange' or f[0]=='terrorism'):
                #     SF_copy = OrderedDict()
                #     SF_copy["DocumentID"] = str(docid)
                #     SF_copy["Type"] = "crimeviolence"
                #     #print 'replaced', f[0]
                #     #SF["TypeConfidence"] = f[2]/100
                #     SF_copy["TypeConfidence"] = f[2]/100
                #     SF_copy["RankConfidence"] = f[3]
                #     SF_copy["Source"] = "Nidhi"
                #     SF_copy["Keyword"] = f[1]
                #     #SF_copy["Gold"] =  SF["Gold"]
                #     SF_copy["Text"] = text
                #     #SF_copy["Drop"]= SF["Drop"]
                #     #final.append(SF_copy)
                sfs.append(sf)
            return sfs
        if isinstance(text, Document):
            doc_id = text.doc_id
            for sentence in text.segments:
                seg_id = sentence.seg_id
                tokens = {}
                for token in sentence.tokens:
                    tokens[(token.span.start, token.span.end)] = remove_accents(token.text, self.remove_char_class)
                frames = self.get_frames_ordered(tokens)
                cur_sfs = frames_to_sfs(frames, doc_id, seg_id, ' '.join(tok.text for tok in sentence.tokens) + ' ')
                if output_object:
                    sfs.extend(SF.from_dict(sf) for sf in cur_sfs)
                else:
                    sfs.extend(cur_sfs)
        else:
            sentences = sent_tokenize(text)
            for seg_num, sentence in enumerate(sentences):
                tokens = span_tokenize(remove_accents(sentence, self.remove_char_class))
                frames = self.get_frames_ordered(tokens)
                cur_sfs = frames_to_sfs(frames, doc_id, 'segment-{}'.format(seg_num), sentence)
                if output_object:
                    sfs.extend(SF.from_dict(sf) for sf in cur_sfs)
                else:
                    sfs.extend(cur_sfs)
        return sfs

    @staticmethod
    def get_keywords(kw_dict=None, kw_dict_path=None, kw_read_strategy='take_last'):
        if kw_dict is None and kw_dict_path is None:
            raise ValueError(('Either kw_dict or kw_dict_path should be specified. '
                              'kw_dict_path can be a directory containing files with SF types as '
                              'their names, each with list of "keyword;confidence". '
                              'It can also be a single file with list of '
                              '"keyword<tab>SF type<tab>confidence"\n'
                              'kw_dict should be a mapping between SF type (evac, med, etc.) '
                              'to list of English keywords'))
        if kw_dict is not None:
            # We are being provided the SFType->list_of_keywords directly, simply check and return
            KW_SF_Classifier.check_eng_keywords(kw_dict)
            return kw_dict
        # Here we load the keyword mapping from the path, either a directory or a file
        if os.path.isdir(kw_dict_path):
            kw_dict = {}
            for sf_type_file in os.listdir(kw_dict_path):
                sf_type_path = '{}/{}'.format(kw_dict_path, sf_type_file)
                sf_type = os.path.splitext(sf_type_file)[0]
                kw_list_per_type = {}
                with open(sf_type_path, 'r') as infile:
                    for line in infile:
                        try:
                            keyword, confidence = line.strip().split(';')
                            confidence = float(confidence)
                            if keyword in kw_list_per_type and kw_read_strategy == KW_READ_STRATEGY.TAKE_MAX:
                                kw_list_per_type[keyword] = max(kw_list_per_type[keyword], confidence)
                            else:
                                kw_list_per_type[keyword] = confidence
                        except:
                            continue
                kw_dict[sf_type] = kw_list_per_type
        else:
            kw_dict = {}
            with open(kw_dict_path, 'r') as infile:
                for line in infile:
                    try:
                        keyword, sf_type, confidence = line.strip().split('\t')
                        confidence = float(confidence)
                        if sf_type not in kw_dict:
                            kw_dict[sf_type] = {}
                        if keyword in kw_dict[sf_type] and kw_read_strategy == KW_READ_STRATEGY.TAKE_MAX:
                            kw_dict[sf_type][keyword] = max(kw_dict[sf_type][keyword], confidence)
                        else:
                            kw_dict[sf_type][keyword] = confidence
                    except:
                        continue
        KW_SF_Classifier.check_eng_keywords(kw_dict)
        return kw_dict

    @staticmethod
    def check_eng_keywords(kw_dict):
        expected = set(SF_TYPES)
        found = set(kw_dict.keys())
        missing = expected.difference(found)
        extra = found.difference(expected)
        if missing or extra:
            raise AssertionError('Extra ({}) or missing ({}) SF types'.format(extra, missing))
    
    @staticmethod
    def get_dict(the_dict=None, the_dict_path=None):
        """Read dictionary in the form of ENG<tab>IL

        Each English word might have multiple IL translations.
        
        Returns:
            dict: English words to list of IL words
        """
        if the_dict is None and the_dict_path is None:
            raise ValueError('Either the_dict or the_dict_path should be specified')
        if the_dict is not None:
            return the_dict
        the_dict = {}
        with open(the_dict_path, 'r') as infile:
            for line in infile:
                eng, il = line.strip('\n').split('\t')
                eng = eng.strip().lower()
                il = il.strip().lower()
                if eng not in the_dict:
                    the_dict[eng] = []
                the_dict[eng].append(il)
        return the_dict

    @staticmethod
    def get_list(the_list=None, the_list_path=None, sep=None, remove_char_class='none'):
        """Reads a file for a list of words/phrases, one per line.
        
        If there are multiple fields, separate them by sep.
        The results will be returned as a list of tuple
        """
        if the_list is None and the_list_path is None:
            return []
        if the_list is not None:
            return the_list
        the_list = []
        with open(the_list_path, 'r') as infile:
            for line in infile:
                if sep:
                    the_list.append(tuple(remove_accents(line.strip(), remove_char_class).split(sep)))
                else:
                    the_list.append(remove_accents(line.strip(), remove_char_class))
        return the_list

    @staticmethod
    def translate_keywords(eng_keywords, bi_dict, ni_dict=None, remove_char_class='none'):
        if ni_dict is not None:
            dict_list = [ni_dict, bi_dict]
        else:
            dict_list = [bi_dict]
        il_keywords = {}
        not_translated = []
        for sf_type in eng_keywords.keys():
            for keyword, confidence in eng_keywords[sf_type].items():
                lowcase = keyword.lower()
                lemmatized = lowcase
                lemmatized = ' '.join(wnl.lemmatize(t) for t in lowcase.split(' '))
                translated = False
                for dictionary in dict_list:
                    for token in [keyword, lowcase, lemmatized]:
                        if token in dictionary:
                            for translation in dictionary[token]:
                                translation = remove_accents(translation, remove_char_class)
                                if translation not in il_keywords:
                                    il_keywords[translation] = dict()
                                if sf_type in il_keywords[translation]:
                                    max_conf = max(il_keywords[translation][sf_type][1], confidence)
                                else:
                                    max_conf = confidence
                                il_keywords[translation][sf_type] = (keyword, max_conf)
                                translated = True
                            if translated:
                                break
                    if translated:
                        break
                if not translated:
                    not_translated.append((keyword, confidence))
        return il_keywords, not_translated


class MainHandler(RequestHandler):
    def initialize(self, kw_sf_classifier):
        self.kw_sf_classifier = kw_sf_classifier

    def get(self, path, *args, **kwargs):
        if path == 'get_sfs':
            pretty = self.get_argument('pretty', default=False) in [True, 'true', 'True', '1', 1]
            text = self.get_argument('text', default=None)
            if text is None:
                path = self.get_argument('path', default=None)
                if path is None:
                    raise HTTPError('No "text" or "path" argument specified')
                with open(path, 'r') as infile:
                    text = infile.read()
            doc_id = hash(text)
            sfs = [sf.to_dict(version=Version.vMacedonianEx) for sf in kw_sf_classifier.predict(text, doc_id, output_object=True)]
            if pretty:
                result = json.dumps(sfs, ensure_ascii=False, indent=4)
                result = re.sub('(?<=\n[ ]*) ', '&nbsp;', result)
                result = re.sub('\n', '<br>', result)
                self.write(result)
            else:
                self.write(json.dumps(sfs, ensure_ascii=False))
        else:
            self.write(json.dumps('unrecognized command: "{}"'.format(path)))
    
    post = get

if __name__ == '__main__':
    parser = argparse.ArgumentParser("ARIEL SF Keyword System")
    # parser.add_argument('--keywords', help='keywords dir', required=True)
    parser.add_argument('--ltf_dir', help='The input LTF directory')
    parser.add_argument('--out_json', help='The output JSON file')
    # parser.add_argument("--data", help="input data in conll format")
    # parser.add_argument("--out", help="output json file")
    parser.add_argument('--top_k', help="How many of top k to take from KWD predictions")
    parser.add_argument('--cut_off_sim', type=float, default=None, help="Threshold for similarity")
    parser.add_argument('--ignore_list_path', help="File which has keywords to ignore")
    parser.add_argument('--run_as_server', action='store_true', help='To run the SF classifier as server')
    parser.add_argument('--config_path', help='The path to config file', required=True)
    parser.add_argument('--port', type=int, default=8100, help='The port to run the server on')
    parser.add_argument('--kw_dict_path', help='The path to the list of keywords in English')
    parser.add_argument('--bi_dict_path', help='The path to the bilingual dictionary, tab-separated En-IL')
    parser.add_argument('--ni_dict_path', help='The path to additional dictionary in the same format')
    args = parser.parse_args()
    with open(args.config_path, 'r') as infile:
        config = json.load(infile)
    for key, value in vars(args):
        if value is not None:
            config[key] = value
    print(config)
    # config = {
    #         'lang': 'il9',
    #         'kw_dict_path': 'eng_extended_cleaned',
    #         'bi_dict_path': '/usr3/data/shared/LoReHLT18/shared/IL9/Lexicons/il9_mt_NI_lexicon.tsv',
    #         'ni_dict_path': '/usr3/data/shared/LoReHLT18/shared/IL9/Lexicons/il9_mt_NI_lexicon.tsv',
    #         'top_k': 2,
    #         'cut_off_sim': 0.8,
    #         'ignore_list_path': 'ignore_list/ignore_KW_v2_kin.txt'
    #         }
    kw_sf_classifier = KW_SF_Classifier(**config)
    if args.run_as_server:
        app = Application([
                (r'/api/(.*)', MainHandler, {'kw_sf_classifier': kw_sf_classifier})
                ])
        port = args.port
        app.listen(port)
        print('Server running at {}'.format(port))
        tornado.ioloop.IOLoop.current().start()
    else:
        docs = read_ltf_files(args.ltf_dir)
        sfs = []
        missing = 0
        for doc in tqdm(docs, smoothing=0.1):
            cur_sfs = kw_sf_classifier.predict(doc, output_object=False)
            if len(cur_sfs) == 0:
                missing += 1
            sfs.extend(cur_sfs)
        print('Missing {}/{}'.format(missing, len(docs)))
        with open(args.out_json, 'w') as outfile:
            json.dump(sorted(sfs, key=lambda x:(x['DocumentID'], x['Text'], x['Type'], x['Keyword'])), outfile, ensure_ascii=False, indent=4)
