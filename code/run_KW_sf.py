# -*- coding: utf-8 -*-
"""
10 Apr 2019
Run KW SF classifier on English Core Data
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from lor_utils import read_ltf_files, Version, SF_TYPES
from argparse import ArgumentParser
import pandas as pd
from sf_kw import KW_SF_Classifier as KW
import json

def main(args=None):
    parser = ArgumentParser(description='Run KW SF classifier on English Core Data')
    parser.add_argument('--dict_paths', nargs='+', required=True,
                        help='The paths to the dictionaries')
    parser.add_argument('--eng_kw_dir_in', default='eng_extended_cleaned',
                        help='The directory containing the keyword list in English')
    parser.add_argument('--ignore_list_path',
                        help='The path to the keyword list (in IL) to be ignored')
    parser.add_argument('--include_list_path',
                        help='The path to the keyword list (in IL) to be included')
    parser.add_argument('--lang', default='',
                        help='The language name of this dictionary, for printing')
    parser.add_argument('--ltf_dir',
                        help='The LTF input directory')
    parser.add_argument('--out_json',
                        help='The path to print the JSON output')
    parser.add_argument('--remove_accents', choices=['none', 'combining', 'tones'], default='none',
                        help='The types of accents to be removed (none, all combining characters, or only tones)')
    args = parser.parse_args(args)
    en_il_dict = {}
    il_en_dict = {}
    num_entries = 0
    for path in args.dict_paths:
        lexicon = pd.read_csv(path, sep='\t', header=None, dtype='object')
        lexicon.columns = ['en_word', 'il_word']
        lexicon = list(zip(*(lexicon['en_word'], lexicon['il_word'])))
        lexicon = [(str(a), str(b)) for a,b in lexicon]
        num_entries += len(lexicon)
        for en_word, il_word in lexicon:
            if en_word not in en_il_dict:
                en_il_dict[en_word] = []
            if il_word not in il_en_dict:
                il_en_dict[il_word] = []
            en_il_dict[en_word].append(il_word)
            il_en_dict[il_word].append(en_word)

    kw = KW(args.lang, kw_dict_path=args.eng_kw_dir_in, bi_dict=en_il_dict, ni_dict=en_il_dict, do_print=False,
            remove_char_class=args.remove_accents, ignore_list_path=args.ignore_list_path,
            include_list_path=args.include_list_path)
    docs = read_ltf_files(args.ltf_dir, progress_bar=False)
    sfs = []
    for doc in docs:
        dot_index = doc.doc_id.find('.')
        if dot_index > 0:
            # Change doc_id from NW_ABC_ENG_0001_20001013.ben to NW_ABC_ENG_0001_20001013" 
            doc.doc_id = doc.doc_id[:dot_index]
        _sfs = kw.predict(doc, output_object=True)
        sfs.extend(_sfs)
    sf_by_types = {}
    for sf in sfs:
        if sf.type not in sf_by_types:
            sf_by_types[sf.type] = []
        sf_by_types[sf.type].append(sf)
    sfs = [sf.to_dict(Version.v2018) for sf in sfs]
    with open(args.out_json, 'w') as outfile:
        json.dump(sfs, outfile, indent=4, ensure_ascii=False)
    print('{}: #docs: {} #SFs: {} {}'.format(args.lang, len(docs), len(sfs),
                    ' '.join(['{}: {}'.format(sf_type, len(sf_by_types.get(sf_type, []))) for sf_type in SF_TYPES])))

if __name__ == '__main__':
    main()

