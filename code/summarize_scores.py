# -*- coding: utf-8 -*-
"""
11 Apr 2019
To summarize SF scores into tables
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import os

def main(args=None):
    parser = ArgumentParser(description='To summarize SF scores into tables')
    parser.add_argument('--scoredir', default='scores',
                        help='The score directory')
    parser.add_argument('--duplicate_handler', choices=['first', 'last', 'average', 'max', 'min'],
                        help='How to handle duplicates (e.g., multiple runs of the same languages)')
    args = parser.parse_args(args)
    results = {}
    for filename in sorted(os.listdir(args.scoredir)):
        lang = filename.split('_')[0]
        pr = None
        rc = None
        f1 = None
        tp = 0
        fp = 0
        fn = 0
        with open('{}/{}'.format(args.scoredir, filename), 'r') as infile:
            for line in infile:
                if 'SFType,occurrence_weighted_precision' in line:
                    pr = float(line.strip().split(',')[-1])
                if 'SFType,occurrence_weighted_recall' in line:
                    rc = float(line.strip().split(',')[-1])
                if 'SFType,occurrence_weighted_f' in line:
                    f1 = float(line.strip().split(',')[-1])
                if 'SFType,num_correct' in line:
                    tp += int(line.strip().split(',')[-1])
                if 'SFType,num_spurious' in line:
                    fp += int(line.strip().split(',')[-1])
                if 'SFType,num_deleted' in line:
                    fn += int(line.strip().split(',')[-1])
        if lang not in results:
            results[lang] = []
        results[lang].append((pr, rc, f1, tp, fp, fn))
    for lang in sorted(results.keys()):
        result = results[lang]
        if args.duplicate_handler == 'first':
            results[lang] = result[0]
        elif args.duplicate_handler == 'last':
            results[lang] = result[-1]
        elif args.duplicate_handler == 'average':
            results[lang] = (sum(l)/len(l) for l in zip(*result))
        elif args.duplicate_handler == 'max':
            results[lang] = (max(l) for l in zip(*result))
        elif args.duplicate_handler == 'max':
            results[lang] = (min(l) for l in zip(*result))
        else:
            pass
    for lang in sorted(results.keys()):
        if args.duplicate_handler is None:
            for pr, rc, f1, tp, fp, fn in results[lang]:
                print('{}: Pr: {:.2f} Rc: {:.2f} F1: {:.2f} TP: {} FP: {} FN {}'.format(
                        lang, pr*100, rc*100, f1*100, int(tp), int(fp), int(fn)))
        else:
            pr, rc, f1, tp, fp, fn = results[lang]
            print('{}: Pr: {:.2f} Rc: {:.2f} F1: {:.2f} TP: {} FP: {} FN {}'.format(
                    lang, pr*100, rc*100, f1*100, int(tp), int(fp), int(fn)))


if __name__ == '__main__':
    main()

