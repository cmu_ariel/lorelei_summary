# 10 Apr 2019
# By: Aldrian Obaja Muis
# Run KW SF Classifier

script_dir=$(dirname $(readlink -f $0))
if [ -z "${outdir+x}" ]; then
    outdir="sf"
fi
mkdir -p ${outdir}
outdir_abs=$(readlink -f ${outdir})
log_path=${outdir}/run.log
echo "script_dir: ${script_dir}"
echo "outdir: ${outdir}"
echo "outdir_abs: ${outdir_abs}"
echo "log_path: ${log_path}"
#echo > ${log_path}

#for lang in "AKA" "AMH" "ARA" "BEN" "CMN" "FAS" "HAU" "HIN" "HUN" "IND" "RUS" "SOM" "SPA" "SWA" "TAM" "TGL" "THA" "TUR"\
#            "UKR" "URD" "UZB" "VIE" "WOL" "YOR" "ZUL" "ENG"; do
for lang in "UKR"; do
    lang_code=$(echo ${lang} | tr [A-Z] [a-z])
    if [ "${lang}" == "ENG" ]; then
        dict_path1="../data/eng_eng_dict.tsv"
        ltf_dir="/home/data/English_Core_Data"
    else
        dict_path1="../data/langpacks/${lang}/dictionaries/lexicon_ldc_clean.tab"
        #dict_path2="../data/langpacks/${lang}/dictionaries/lexicon_uw_2017.tab"
        ltf_dir="/home/data/English_Core_Data_translations/${lang_code}"
    fi
    if [ "${lang}" == "ARA" ]; then
        remove_accents_opt="--remove_accents combining"
    elif [ "${lang}" == "YOR" ]; then
        remove_accents_opt="--remove_accents tones"
    else
        remove_accents_opt=""
    fi
    python3 run_KW_sf.py \
        --dict_paths "${dict_path1}" ${dict_path2} \
        --eng_kw_dir_in "../data/eng_extended_cleaned" \
        --ltf_dir "${ltf_dir}" \
        --out_json "${outdir}/${lang}_kw.json" \
        --lang ${lang} \
        ${remove_accents_opt} \
        | tee -a ${log_path}
done
