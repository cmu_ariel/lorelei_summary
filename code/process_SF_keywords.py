# -*- coding: utf-8 -*-
"""
10 Apr 2019
Process SF keywords by taking a dictionary and the SF keywords
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import pandas as pd
from sf_kw import KW_SF_Classifier as KW
from itertools import chain

def main(args=None):
    parser = ArgumentParser(description='Process SF keywords by taking a dictionary and the SF keywords')
    parser.add_argument('--dict_paths', nargs='+', required=True,
                        help='The paths to the dictionaries')
    parser.add_argument('--eng_kw_dir_in', default='eng_extended_cleaned',
                        help='The directory containing the keyword list in English')
    parser.add_argument('--il_kw_dir_out', required=True,
                        help='The directory to output the translated IL keywords')
    parser.add_argument('--lang', default='',
                        help='The language name of this dictionary, for printing')
    args = parser.parse_args(args)
    en_il_dict = {}
    il_en_dict = {}
    num_entries = 0
    for path in args.dict_paths:
        lexicon = pd.read_csv(path, sep='\t', header=None, dtype='object')
        lexicon.columns = ['en_word', 'il_word']
        lexicon = list(zip(*(lexicon['en_word'], lexicon['il_word'])))
        lexicon = [(str(a), str(b)) for a,b in lexicon]
        num_entries += len(lexicon)
        for en_word, il_word in lexicon:
            if en_word not in en_il_dict:
                en_il_dict[en_word] = []
            if il_word not in il_en_dict:
                il_en_dict[il_word] = []
            en_il_dict[en_word].append(il_word)
            il_en_dict[il_word].append(en_word)

    kw = KW(args.lang, kw_dict_path=args.eng_kw_dir_in, bi_dict=en_il_dict, ni_dict=en_il_dict, do_print=False)
    kw.print_keywords(args.il_kw_dir_out)
    il_keywords = kw.il_keywords
    num_en_lemmas = len(en_il_dict)
    num_il_lemmas = len(il_en_dict)
    def translate(words, the_dict):
        result = {}
        for word in words:
            if word in the_dict:
                result[word] = the_dict[word]
        return result
    eng_keywords = set(chain(*kw.eng_keywords.values()))
    eng_keywords_in_il = translate(eng_keywords, en_il_dict)
    eng_to_il_keywords = set(chain(*eng_keywords_in_il.values()))
    eng_to_il_to_eng_keywords = set(chain(*translate(eng_to_il_keywords, il_en_dict).values()))
    eng_backtrans_valid = eng_keywords.intersection(eng_to_il_to_eng_keywords)
    print(('{} num_dict_entries: {} num_en_lemmas: {} num_il_lemmas: {} kw: {} kw_found: {} kw_in_il: {} '
           'kw_il_backtrans: {} kw_il_backtrans_valid: {}').format(
            args.lang, num_entries, num_en_lemmas, num_il_lemmas, len(eng_keywords), len(eng_keywords_in_il),
            len(eng_to_il_keywords), len(eng_to_il_to_eng_keywords), len(eng_backtrans_valid)))

if __name__ == '__main__':
    main()

