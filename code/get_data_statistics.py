# -*- coding: utf-8 -*-
"""
03 Nov 2018
Print various data statistics
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import pandas as pd

def convert_annotated_filelist_to_table(lang, outdir):
    ann_filelist_path = 'data/{}/setE/sf_anno/annotated_filelist_SF.tab'.format(lang)
    data = pd.read_csv(ann_filelist_path, sep='\t', index_col=None)
    user_ids = list(sorted(set(data.user_id)))
    for user_id in user_ids:
        data.loc[:,str(user_id)] = (data.user_id == user_id)
    grouped = data.groupby(['doc_id'], as_index=False).sum()
    unique_docs = len(grouped)
    grouped['count'] = 1
    cols = [str(u) for u in user_ids]+['count']
    grouped = grouped.groupby([str(u) for u in user_ids], as_index=False).sum()[cols]
    grouped = grouped.append([dict(zip(cols, ['ANY']*len(user_ids) + [unique_docs]))], ignore_index=True)
    outpath = '{}/{}_annotated_filelist_SF.csv'.format(outdir, lang)
    grouped.to_csv(outpath)
    return grouped, 

def main(args=None):
    parser = ArgumentParser(description='Print various data statistics')
    parser.add_argument('--lang', choices=['IL5', 'IL6', 'IL9', 'IL10'], default='IL9',
                        help='The language to be processed')
    parser.add_argument('--outdir', default='analysis/data_statistics',
                        help='The output directory')
    args = parser.parse_args(args)
    table = convert_annotated_filelist_to_table(args.lang, args.outdir)

if __name__ == '__main__':
    main()

